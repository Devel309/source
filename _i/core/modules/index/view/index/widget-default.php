<style>
.datepicker{z-index:1151 !important;}
</style>
<div class="header_bar">
    <!--\\\\\\\ header Start \\\\\\-->
    <div class="brand">
      <!--\\\\\\\ brand Start \\\\\\-->
      <div class="logo" style="display:block"><span class="theme_color">YAHUA</span> </div>
      <div class="small_logo" style="display:none"><img src="images/s-logo.png" width="50" height="47" alt="s-logo" /> <img src="images/r-logo.png" width="122" height="20" alt="r-logo" /></div>
    </div>
    <!--\\\\\\\ brand end \\\\\\-->
    <div class="header_top_bar">
      <!--\\\\\\\ header top bar start \\\\\\-->
      <a href="javascript:void(0);" class="menutoggle"> <i class="fa fa-bars"></i> </a>
      <div class="top_left">
        <div class="top_left_menu">
          <ul>
            <li> <a href="javascript:void(0);"><i class="fa fa-repeat"></i></a> </li>
            <li class="dropdown"> <a data-toggle="dropdown" href="javascript:void(0);"> <i class="fa fa-th-large"></i> </a>
      <ul class="drop_down_task dropdown-menu" style="margin-top:39px">
        <div class="top_left_pointer"></div>
        <li> <a href="#"><i class="fa fa-question-circle"></i> Ayuda</a> </li>
        <li> <a href="#"><i class="fa fa-cog"></i> Configuración</a></li>
        <li> <a href="logout.php"><i class="fa fa-power-off"></i> Cerrar Sesión</a> </li>
      </ul>
      </li>
          </ul>
        </div>
      </div>
      <div class="top_right_bar">
        <div class="user_admin dropdown">
          <a href="javascript:void(0);"
          data-toggle="dropdown">
          <?php
          if ($_SESSION['client_id']==='0'){ ?>
            <img src="../img/clts/owner.jpg" />
             <span class="user_adminname">OWNER</span><b class="caret"></b>
          <?php }else{
          $clt = ClientData::getById($_SESSION["client_id"]);
          if($clt->cond==="http"){ ?>
            <img src="<?php echo $clt->profile; ?>" />
          <?php }else{ ?>
            <img height="40px" width="40px" src="../img/clts/<?php echo $clt->profile; ?>" />
          <?php } ?>
            <span class="user_adminname">
            <?php echo $clt->lastname; ?>
            </span><b class="caret"></b>
          <?php } ?>
          </a>
          <ul class="dropdown-menu">
            <div class="top_pointer"></div>
            <li> <a href="#"><i class="fa fa-user"></i> Editar Perfil</a> </li>
            <li> <a href="#"><i class="fa fa-question-circle"></i> Ayuda</a> </li>
            <li> <a href="#"><i class="fa fa-cog"></i> Configuración </a></li>
            <li> <a href="logout.php"><i class="fa fa-power-off"></i> Cerrar Sesión</a> </li>
          </ul>
        </div>        
      </div>
    </div>
    <!--\\\\\\\ header top bar end \\\\\\-->
  </div>
  <!--\\\\\\\ header end \\\\\\-->
  <div class="inner">
    <!--\\\\\\\ inner start \\\\\\-->
    <div class="left_nav">

      <!--\\\\\\\left_nav start \\\\\\-->
      <div class="search_bar"> <i class="fa fa-search"></i>
        <input name="" type="text" class="search" placeholder="Buscar" />
      </div>
      <?php $consultant = ConsultBusinessData::getById($_SESSION['client_id']); ?>
      <div class="left_nav_slidebar">
        <ul>
          <li class="left_nav_active theme_border"><a href="javascript:void(0);"><i class="fa fa-home"></i> Tablero <span class="left_nav_pointer"></span> <span class="plus"><i class="fa fa-plus"></i></span> </a>
            <ul class="opened" style="display:block">
              <li> <a href="./"> <span>&nbsp;</span> <i class="fa fa-circle theme_color"></i> <b class="theme_color">Inicio</b> </a> </li>
              <li>
                <a href="?view=#"><span>&nbsp;</span> <i class="fa fa-circle"></i> <b>Consultoría</b> </a>
              </li>
            </ul>

          </li>
        </ul>
      </div>
    </div>
    <!--\\\\\\\left_nav end \\\\\\-->
    <div class="contentpanel">
      <!--\\\\\\\ contentpanel start\\\\\\-->
      <div class="pull-left breadcrumb_admin clear_both">
        <div class="pull-left page_title theme_color">
          <h1>Tablero</h1>
          <h2 class="active">Panel de Control</h2>
        </div>
        <div class="pull-right">
          <ol class="breadcrumb">
            <li><a href="#">Tablero</a></li>
            <li class="active">Panel de Control</li>
          </ol>
        </div>
      </div>
      <div class="container clear_both padding_fix">
        <?php $consultant = ConsultBusinessData::getById($_SESSION['client_id']);
        $verpay = ConsultBusinessData::VerifyClientId($_SESSION['client_id']); ?>
        <div class="row" ng-init="services()">
          <div class="col-sm-3 col-sm-6" onclick="window.location='#';" ng-repeat="sv in service">
            <div class="information green_info">   
              <div class="information_inner">
              <div class="info green_symbols"><i class="fa fa-laptop icon"></i></div>
                <span><b>CONSULTORIA</b><br>PREMIUM </span>
                <div class="infoprogress_blue">
                  <div class="greenprogress"></div>
                </div>
                <h3 class="bolded">ADMINISTRAR </h3>
              </div>
            </div>
          </div>
          <div class="col-sm-3 col-sm-6">
            <a href="#">
            <div class="information blue_info">
              <div class="information_inner">
              <div class="info blue_symbols"><i class="fa fa-shopping-cart icon"></i></div>
                <span><b>VENTAS</b><br>PREMIUM </span>
                <div class="infoprogress_blue">
                  <div class="blueprogress"></div>
                </div>
                <h3 class="bolded">ADMINISTRAR </h3>
              </div>
            </div>
            </a>
          </div>
          <div class="col-sm-3 col-sm-6">
            <div class="information red_info">
              <div class="information_inner">
              <div class="info red_symbols"><i class="fa fa-bar-chart-o icon"></i></div>
                <span><b>LOGÍSTCA</b><br>PREMIUM </span>
                <div class="infoprogress_red">
                  <div class="redprogress"></div>
                </div>
                <h3 class="bolded">ADMINISTRAR </h3>
              </div>
            </div>
          </div>
          <div class="col-sm-3 col-sm-6">
           <div class="information gray_info">
              <div class="information_inner">
              <div class="info blue_symbols"><i class="fa fa-cutlery icon"></i></div>
                <span><b>DELIVERY</b><br>PREMIUM </span>
                <div class="infoprogress_gray">
                  <div class="grayprogress"></div>
                </div>
                <h3 class="bolded">ADMINISTRAR </h3>
              </div>
            </div>
          </div>
        </div>
         
      </div>
      <!--\\\\\\\ container  end \\\\\\-->
    </div>
    <!--\\\\\\\ content panel end \\\\\\-->
  </div>
  <!--\\\\\\\ inner end\\\\\\-->