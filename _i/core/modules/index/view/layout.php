<!doctype html>
<html>
<head>
  <meta charset="utf-8">
  <meta http-equiv="X-UA-Compatible" content="IE=edge">
  <title>INNOVANDO MAS ...!</title>
  <link href="../_c/css/font-awesome.css" rel="stylesheet" type="text/css" />
  <link href="../_c/css/bootstrap.min.css" rel="stylesheet" type="text/css" />
  <link href="../_c/css/animate.css" rel="stylesheet" type="text/css" />
  <link href="../_c/css/admin.css" rel="stylesheet" type="text/css" />
  <link href="../_c/css/jquerysctipttop.css" rel="stylesheet" type="text/css">
  <link href="../_c/plugins/kalendar/kalendar.css" rel="stylesheet">
  <link rel="stylesheet" href="../c/plugins/scroll/nanoscroller.css">
  <link href="../_c/plugins/morris/morris.css" rel="stylesheet" />
  <script src="../js/angular-1.3.15.min.js"></script>
  <script src="../js/sweetalert-dev.js"></script>
  <link rel="stylesheet" href="../js/sweetalert.css">
  <link rel="stylesheet" type="text/css" href="../_c/plugins/bootstrap-datepicker/css/datepicker.css" />
  <link rel="icon" type="image/png" href="../img/logo1.png" />
</head>
<body class="light_theme fixed_header left_nav_fixed" data-ng-app="consultoria" data-ng-controller="controller">
<?php if(isset($_SESSION["client_id"])!="0" && $_SESSION["client_id"]!=""):?>
<div class="wrapper">
  <!--\\\\\\\ wrapper Start \\\\\\-->
 <?php  View::load("index"); ?> 

 <?php else:
  View::load("login");
 endif; ?>
</div>
<!--\\\\\\\ wrapper end\\\\\\-->   

<script src="../_c/js/jquery-2.1.0.js"></script>
<script src="../_c/js/bootstrap.min.js"></script>
<script src="../_c/js/common-script.js"></script>
<script src="../_c/js/jquery.slimscroll.min.js"></script>
<script src="../_c/js/jquery.sparkline.js"></script>
<script src="../_c/js/sparkline-chart.js"></script>
<script src="../_c/js/graph.js"></script>
<script src="../_c/js/edit-graph.js"></script>
<script src="../_c/plugins/kalendar/kalendar.js" type="text/javascript"></script>
<script src="../_c/plugins/kalendar/edit-kalendar.js" type="text/javascript"></script>

<script src="../_c/plugins/sparkline/jquery.sparkline.js" type="text/javascript"></script>
<script src="../_c/plugins/sparkline/jquery.customSelect.min.js" ></script> 
<script src="../_c/plugins/sparkline/sparkline-chart.js"></script> 
<script src="../_c/plugins/sparkline/easy-pie-chart.js"></script>
<script src="../_c/plugins/morris/morris.min.js" type="text/javascript"></script> 
<script src="../_c/plugins/morris/raphael-min.js" type="text/javascript"></script>  
<script src="../_c/plugins/morris/morris-script.js"></script> 

<script src="../_c/plugins/demo-slider/demo-slider.js"></script>
<script src="../_c/plugins/knob/jquery.knob.min.js"></script> 

<script src="../_c/js/jPushMenu.js"></script> 
<script src="../_c/js/side-chats.js"></script>
<script src="../_c/js/jquery.slimscroll.min.js"></script>
<script src="../_c/plugins/scroll/jquery.nanoscroller.js"></script>
<script type="text/javascript" src="../_c/plugins/input-mask/jquery.inputmask.min.js"></script>
<script type="text/javascript" src="../_c/plugins/bootstrap-datepicker/js/bootstrap-datepicker.js"></script> 
</body >
</html>