<?php
if (isset($_GET['lang'])){
  $language = $_GET['lang'];
  if(!empty($language)){
    require("../lang/".$language.".php");
  }
}else{
  $language = substr($_SERVER['HTTP_ACCEPT_LANGUAGE'],0,2);
  require("../lang/".$language.".php");
}
?>
<div class="pull-left breadcrumb_admin clear_both">
        <div class="pull-left page_title theme_color">
          <h1><?php echo $lang['consult']; ?></h1>
          <h2 class="">Online</h2>
        </div>
        <div class="pull-right">
          <ol class="breadcrumb">
            <li><a href="#"><?php echo $lang['consult']; ?></a></li>
            <li class="active">Online</li>
          </ol>
        </div>
      </div>
      <div class="container clear_both padding_fix">
        <!--\\\\\\\ container  start \\\\\\-->
        <input type="hidden" ng-model="consultoria_id" ng-init="consultoria_id=<?php echo $_GET["_i"]; ?>">
        <div class="row" ng-init="listar()">
          <div class="col-md-10">
            <h2 style="text-align:center"><?php echo $lang['consult']; ?></h2>
            <div class="timeline">
              <div class="timeline-item alt">
                      <div class="timeline-desk">
                          <div class="panel">
                              <div class="panel-body">
                                  <span class="arrow-alt"></span>

                                  <span class="timeline-icon blue">
                                      <i class="fa fa-check-square-o"></i>
                                  </span>
                                  <form method="post" ng-submit="add()">
                                  <div class="input-group">
                                    <input type="text" class="form-control" ng-model="consultar" placeholder="<?php echo $lang['typmsj']; ?>...">
                                    <div class="input-group-btn">
                                      <input type="submit" name="submit" id="submit" class="btn btn-warning" value="+" />
                                    </div>
                                  </div>
                                  </form>
                                </div>
                              </div>
                          </div>
                      </div>
                  </div>
              
              <div class="timeline" ng-repeat="con in consultas">
                
                  <div class="timeline-item alt">
                      <div class="text-right">
                          <div class="time-show first">
                            <a class='btn btn-primary'>{{con.fecha}}</a>
                          </div>
                      </div>
                  </div>

                    <div ng-class="{'timeline-item alt': ad.client_id == '<?php echo $_SESSION['client_id']; ?>', 'timeline-item': ad.client_id !== '<?php echo $_SESSION['client_id']; ?>' }" ng-repeat='ad in auditorias'>
                      <div class="timeline-desk" ng-if="ad.client_id=='<?php echo $_SESSION['client_id']; ?>' && con.id==ad.cdi">
                        <div class="panel">
                          <div class="panel-body">
                            <span class="arrow-alt"></span>

                            <span class="timeline-icon blue" ng-if="ad.client_id == '<?php echo $_SESSION['client_id']; ?>'">
                              <i class="fa fa-user-md"></i>
                            </span>

                            <h1 class="blue"><i class="fa fa-comment-o"></i></h1>
                            <p>{{ad.conver}}</p>
                            <form method="post" ng-submit="del(ad.id)">
                              <input type="submit" class='btn btn-danger btn-xs' value="<?php echo $lang['del']; ?>" />
                            </form>
                          </div>                              
                        </div>
                      </div>
                    

                      <div class="timeline-desk" ng-if="ad.client_id!='<?php echo $_SESSION['client_id']; ?>' && con.id==ad.cdi">
                        <div class="panel">
                            <div class="panel-body">
                                <span class="arrow-alt"></span>

                                <span class="timeline-icon red" ng-if="ad.client_id != '<?php echo $_SESSION['client_id']; ?>'">
                                  <i class="fa fa-user-md"></i>
                                </span>

                                <h1 class="blue"><i class="fa fa-comment-o"></i></h1>
                                <p>{{ad.conver}}</p>
                            </div>                              
                        </div>
                      </div>
                  </div>
              </div>
          </div>
        </div>
      </div>

<script>
var app = angular.module('consultoria', []);
app.controller('controller', function($scope, $http, $interval){

  $scope.consultoria_id='';

  $scope.listar = function(){
    $http({
      method:"POST",
      url:"?action=clt",
      data:{
        'id':$scope.consultoria_id,
      }
    }).success(function(data){
      $scope.consultas = data;
      $scope.convers();
    });
  };

  $scope.convers = function(){
    $http({
      method:"POST",
      url:"?action=uptclt",
      data:{
        'id':$scope.consultoria_id,
      }
    }).success(function(data){
      $scope.auditorias = data;
    });
  };

  $scope.add = function(){
    $http({
      method:"POST",
      url:"?action=clt",
      data:{
        'id':$scope.consultoria_id,
        'consultar':$scope.consultar,
        'action':'agregar'
      }
    }).success(function(data){
        $scope.consultar = "";
        $scope.listar();
    });
  };

  $scope.del = function(id){
    $scope.consultar = "";
    $http({
      method:"POST",
      url:"?action=clt",
      data:{
        'iddel':id,
        'action':'eliminar'
      }
    }).success(function(data){
        $scope.listar();
    });
  };

$interval(function(){
  $scope.listar();
}, 5000);

});

</script>