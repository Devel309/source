<?php
class ClientData {
	public static $tablename = "client";
	public static $vlr="";

	public function ClientData(){
		$this->name = "";
		$this->lastname = "";
		$this->email = "";
		$this->password = "";
		$this->created_at = "NOW()";
	}

	public function add(){
		$sql = "insert into ".self::$tablename." (name,lastname,gender,nameb,phone,employe_id,email,pass,created_at,is_active,profile) ";
		$sql .= "value (\"$this->firstname\",\"$this->lastname\",\"$this->gender\",\"$this->nameb\",\"$this->phone\",\"$this->eid\",\"$this->email\",\"$this->rpsw\",now(),'1',\"$this->photo\")";
		Executor::doit($sql);
	}
	
	public function addbytweet(){
		$sql = "insert into ".self::$tablename." values (default,'$this->name','','','','','1','','',now(),'1','$this->profile')";
		Executor::doit($sql);
	}

	public static function delById($id){
		$sql = "delete from ".self::$tablename." where id=$id";
		Executor::doit($sql);
	}
	public function del(){
		$sql = "delete from ".self::$tablename." where id=$this->id";
		Executor::doit($sql);
	}

	public function update(){
		$sql = "update ".self::$tablename." set name=\"$this->name\",lastname=\"$this->lastname\",email=\"$this->email\",phone=\"$this->phone\",address=\"$this->address\",is_active=$this->is_active,Dni=\"$this->dni\" where id=$this->id";
		Executor::doit($sql);
	}
	
	public function update_passwd(){
		$sql = "update ".self::$tablename." set pass=\"$this->rpsw\" where id=$this->id";
		Executor::doit($sql);
	}
	
	public function update_email(){
		$sql = "update ".self::$tablename." set email=\"$this->email\" where id=$this->id";
		Executor::doit($sql);
	}
	
	public function update_info(){
		$sql = "update ".self::$tablename." set lastname=\"$this->lastname\",name=\"$this->firstname\",gender=\"$this->gender\",phone=\"$this->phone\" where id=$this->id";
		Executor::doit($sql);
	}
	
	public function update_image(){
		$sql = "update ".self::$tablename." set profile=\"$this->image\" where id=$this->id";
		Executor::doit($sql);
	}
	
	public function update_b(){
		$sql = "update ".self::$tablename." set nameb=\"$this->nameb\",employe_id=\"$this->eid\" where id=$this->id";
		Executor::doit($sql);
	}

	public static function Veraddm($uname){
		$sql = "select * from users where username='$uname'";
		$query = Executor::doit($sql);
		return Model::many($query[0],new ConsultBusinessData());
	}

	public function getFullname(){ return $this->name." ".$this->lastname; }

	public static function getById($id){
		$sql = "select *,mid(profile,1,4) pp from ".self::$tablename." where id=$id";
		$query = Executor::doit($sql);
		$found = null;
		$data = new ClientData();
		while($r = $query[0]->fetch_array()){
			$data->id = $r['id'];
			$data->name = $r['name'];
			$data->lastname = $r['lastname'];
			$data->email = $r['email'];
			$data->phone = $r['phone'];
			$data->password = $r['pass'];
			$data->is_active = $r['is_active'];
			$data->profile = $r['profile'];
			$data->gender = $r['gender'];
			$data->nameb = $r['nameb'];
			$data->employe_id = $r['employe_id'];
			$data->cond = $r['pp'];
			$found = $data;
			break;
		}
		return $found;
	}

	public static function getByEmail($mail){
		$sql = "select * from ".self::$tablename." where email=\"$mail\"";
		$query = Executor::doit($sql);
		$array = array();
		$cnt = 0;
		while($r = $query[0]->fetch_array()){
			$array[$cnt] = new ClientData();
			$array[$cnt]->id = $r['id'];
			$array[$cnt]->name = $r['name'];
			$array[$cnt]->lastname = $r['lastname'];
			$array[$cnt]->email = $r['email'];
			$array[$cnt]->password = $r['pass'];
			$array[$cnt]->created_at = $r['created_at'];
			$cnt++;
		}
		return $array;
	}
	
	public static function getByProf($profile){
		$sql = "select * from ".self::$tablename." where profile=\"$profile\"";
		$query = Executor::doit($sql);
		$found = null;
		$data = new ClientData();
		while($r = $query[0]->fetch_array()){
			$data->id = $r['id'];
			$data->name = $r['lastname'];
			$data->profile = $r['profile'];
			$found = $data;
			break;
		}
		return $found;
	}
	
	public static function getAllByClientDni($id){
		$sql = "select * from ".self::$tablename." where Dni=\"$id\"";
		$query = Executor::doit($sql);
		$array = array();
		$cnt = 0;
		while($r = $query[0]->fetch_array()){
			$array[$cnt] = new ClientData();
			$array[$cnt]->id = $r['id'];
			$array[$cnt]->name = $r['name'];
			$array[$cnt]->lastname = $r['lastname'];
			$array[$cnt]->address = $r['address'];
			$array[$cnt]->phone = $r['phone'];
			$array[$cnt]->email = $r['email'];
			$array[$cnt]->password = $r['password'];
			$array[$cnt]->created_at = $r['created_at'];
			$cnt++;
		}
		return $array;
	}
	
	public static function getSearch($dat){
		$sql = "select * from ".self::$tablename." where concat(lastname,' ',name) like '%".$dat."%'";
		$query = Executor::doit($sql);
		$array = array();
		$cnt = 0;
		while($r = $query[0]->fetch_array()){
			$array[$cnt] = new ClientData();
			$array[$cnt]->dni = $r['Dni'];
			$array[$cnt]->id = $r['id'];
			$array[$cnt]->name = $r['name'];
			$array[$cnt]->lastname = $r['lastname'];
			$array[$cnt]->address = $r['address'];
			$array[$cnt]->phone = $r['phone'];
			$array[$cnt]->email = $r['email'];
			$array[$cnt]->password = $r['password'];
			$array[$cnt]->created_at = $r['created_at'];
			$cnt++;
		}
		return $array;
	}
	
	public static function getAll(){
		$sql = "select * from ".self::$tablename." order by lastname;";
		$query = Executor::doit($sql);
		$array = array();
		$cnt = 0;
		while($r = $query[0]->fetch_array()){
			$array[$cnt] = new ClientData();
			$array[$cnt]->dni = $r['Dni'];
			$array[$cnt]->id = $r['id'];
			$array[$cnt]->name = $r['name'];
			$array[$cnt]->lastname = $r['lastname'];
			$array[$cnt]->address = $r['address'];
			$array[$cnt]->phone = $r['phone'];
			$array[$cnt]->email = $r['email'];
			$array[$cnt]->password = $r['password'];
			$array[$cnt]->created_at = $r['created_at'];
			$cnt++;
		}
		return $array;
	}

	public static function getLike($q){
		$sql = "select * from ".self::$tablename." where name like '%$q%'";
		$query = Executor::doit($sql);
		$array = array();
		$cnt = 0;
		while($r = $query[0]->fetch_array()){
			$array[$cnt] = new ClientData();
			$array[$cnt]->id = $r['id'];
			$array[$cnt]->name = $r['name'];
			$array[$cnt]->mail = $r['mail'];
			$array[$cnt]->password = $r['password'];
			$array[$cnt]->created_at = $r['created_at'];
			$cnt++;
		}
		return $array;
	}
	
	public static function getVerify($nm,$lt){
		$sql = "select * from ".self::$tablename." where concat(name,' ',lastname)=concat('$nm',' ','$lt')";
		$query = Executor::doit($sql);
		return Model::many($query[0],new ClientData());
	}
}

?>