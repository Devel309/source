<?php

$form_data = json_decode(file_get_contents("php://input"));

$con = new ConsultBusinessData();

if (empty($form_data->action)) {
	echo json_encode($con->getMyConsults($_SESSION['client_id']));
}else {
	echo json_encode($con->getClients($form_data->id));
}

?>