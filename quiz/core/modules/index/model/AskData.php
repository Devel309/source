<?php
class AskData {
	public static $tablename = "ask";


	public function AskData(){
		$this->created_at = "NOW()";
	}

	public function getStatus(){ return StatusData::getById($this->status_id);}
	public function getClient(){ return ClientData::getById($this->client_id);}

	public function add(){
		$sql = "insert into ".self::$tablename." (code,client_id,created_at,status_id) ";
		$sql .= "value (\"$this->code\",\"$this->client_id\",$this->created_at,$this->status_id)";
		return Executor::doit($sql);
	}

	public static function delById($id){
		$sql = "delete from ".self::$tablename." where id=$id";
		Executor::doit($sql);
	}
	public function del(){
		$sql = "delete from ".self::$tablename." where id=$this->id";
		Executor::doit($sql);
	}

	public function update(){
		$sql = "update ".self::$tablename." set name=\"$this->name\" where id=$this->id";
		Executor::doit($sql);
	}

	public static function getById($id){
		$sql = "select * from ".self::$tablename." where idask=$id";
		$query = Executor::doit($sql);
		return Model::one($query[0],new AskData());
	}

	public static function getAll(){
		$sql = "select * from ".self::$tablename;
		$query = Executor::doit($sql);
		return Model::many($query[0],new AskData());
	}

	public static function getAnswer($id){
		$sql = "select * from answer where idask=$id";
		$query = Executor::doit($sql);
		return Model::many($query[0],new AskData());
	}

}

?>