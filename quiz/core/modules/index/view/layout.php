<!DOCTYPE html>
<html>
	<head>
		<meta http-equiv="Content-type" content="text/html; charset=utf-8" />
		<meta http-equiv="X-UA-Compatible" content="IE=edge,chrome=1"> 
		<meta name="viewport" content="width=device-width, initial-scale=1.0"> 
		<title>Quiz - CINIA</title>
		<meta name="description" content="Página web para medir cuánto conoces" />
		<meta name="keywords" content="" />
		<meta name="author" content="CINIA" />
    <meta content="width=device-width, initial-scale=1, maximum-scale=1, user-scalable=no" name="viewport">
		<link rel="icon" type="image/png" href="../img/logo1.png" />
		<link rel="stylesheet" type="text/css" href="css/normalize.css" />
		<link rel="stylesheet" type="text/css" href="css/demo.css" />
		<link rel="stylesheet" type="text/css" href="css/component.css" />
		<link rel="stylesheet" type="text/css" href="../js/sweetalert.css">
		<script src="../js/sweetalert.min.js"></script>
	</head>
	<body>
		<div class="container">
			<!-- Top Navigation -->
			<header>
				<h1><img src="../img/logo.png"> Cuestionario de CINIA S.A.C.</h1>	
			</header>
			<?php $numPregs=5;
			$preguntas = AskData::getAll();
			$pregsExistentes = count($preguntas);
       		for($r=0;$r<$numPregs;$r++) $vector[$r]=0;
       		for($r=0;$r<$numPregs;$r++){
       			$alea=rand(1,$pregsExistentes);
       			$bandera=true;
       			for($f=0;$f<$r;$f++)
       			if($vector[$f]==$alea){
       				$bandera=false;
       				break;
       			}
       			if(!$bandera){ $r--; continue; }
       			$vector[$r]=$alea;
       		}

       		$num = 0;
       		$multiple = 0;

       		for($r=0;$r<$numPregs;$r++){
                if(count($preguntas)>0){
                $preg = AskData::getById($vector[$r]);
                $num ++; $multiple++;
       		?>
			<section>
				<?php if ($multiple===1) { ?>
					<form id="<?php echo 'form'.$num; ?>" name="<?php echo 'form'.$num; ?>" class="ac-custom ac-radio ac-fill" autocomplete="off">
				<?php }elseif ($multiple===2) { ?>
					<form id="<?php echo 'form'.$num; ?>" name="<?php echo 'form'.$num; ?>" class="ac-custom ac-radio ac-circle" autocomplete="off">
				<?php }elseif ($multiple===3) { ?>
					<form id="<?php echo 'form'.$num; ?>" name="<?php echo 'form'.$num; ?>" class="ac-custom ac-radio ac-swirl" autocomplete="off">
				<?php }
				if ($multiple===3) {
				 	$multiple = 0;
				 } ?>
					<h2><?php echo $num.' '.$preg->ask; ?></h2>
					<ul>
					<?php $answer = AskData::getAnswer($vector[$r]); ?>
						<input type="hidden" id="preg<?php echo ($r+1) ?>" name="preg<?php echo ($r+1) ?>" value="<?php echo $vector[$r]; ?>">
						<?php
                  		foreach($answer as $a): ?>
						<li><input id="<?php echo 'r'.$a->id; ?>" name="<?php echo 'r'.$num; ?>" type="radio" value="<?php echo $a->id; ?>"><label for="<?php echo 'r'.$a->id; ?>"><?php echo $a->answer; ?></label></li>
						<?php endforeach; ?>
					</ul>
				</form>
			</section>
			<?php }
			} ?>
			<section>
				<div style="text-align: center;">
					<button class="btn btn-success" id="result">Ver Resultado</button>
				</div>
			</section>
			<!--section class="related">
				<p>If you enjoyed these effects you might also like:</p>
				<p><a href="http://tympanus.net/Tutorials/AnimatedBorderMenus/">Animated Border Menus</a></p>
				<p><a href="http://tympanus.net/Development/CreativeButtons/">Creative Button Styles</a></p>
			</section-->
		</div><!-- /container -->
		<script src="../js/vendor/jquery-1.11.3.min.js"></script>
		<script src="js/modernizr.custom.js"></script>
		<script src="js/svgcheckbx.js"></script>
<script>

var user = "";

window.onload=function() {
    FB.init({
    appId      : '453855664962783',
    cookie     : true,
    xfbml      : true,
    version    : 'v2.5'
	});

	FB.getLoginStatus(function(response) {
	    	statusChangeCallback(response, function() {});
	  	});
};

var statusChangeCallback = function(response, callback) {
  		console.log(response);
   		
    	if (response.status === 'connected') {
      		getFacebookData();
    	} else {
     		callback(false);
    	}
  	}

	var checkLoginState = function(callback) {
    	FB.getLoginStatus(function(response) {
      		callback(response);
    	});
  	}

  	var getFacebookData =  function() {
  		FB.api('/me', function(response) {
  			user = ""+response.name;
	  	});
  	}

  	var getDataRegister = function() {
  		checkLoginState(function(data) {
  			if (data.status !== 'connected') {
  				FB.login(function(response) {
  					if (response.status === 'connected')
  						getFacebookData();
  				}, {scope: scopes});
  			}
  		})
  	}
                 

function postStory(){
	FB.ui({
    	method: 'feed',
        link: 'http://cinia.webcindario.com/quiz/',
        caption: 'Test a mean',
        description:'Past Time',
        picture:'http://cinia.webcindario.com/img/logo1.png',
        }, function(response){
        if(response && !response.error_message){
        	$('#lb_msj').text('link posteado'+response.post_id);
        }
        else{
        	$('#lb_msj').text('error ...');
    	}
	});
}

$(document).ready(function(e) {

	//getDataRegister();

	var vlr1;
	var vlr2;
	var vlr3;
	var vlr4;
	var vlr5;
	
	$('#form1 input').on('change', function() {
		vlr1 = $('input[name=r1]:checked', '#form1').val();
	});
	$('#form2 input').on('change', function() {
		vlr2 = $('input[name=r2]:checked', '#form2').val();
	});
	$('#form3 input').on('change', function() {
		vlr3 = $('input[name=r3]:checked', '#form3').val();
	});
	$('#form4 input').on('change', function() {
		vlr4 = $('input[name=r4]:checked', '#form4').val();
	});
	$('#form5 input').on('change', function() {
		vlr5 = $('input[name=r5]:checked', '#form5').val();
	});
                  
  $("#result").click(function(e) {
    e.preventDefault();
                          
    var b=$("#preg1").val();
    var b1=$("#preg2").val();
    var b2=$("#preg3").val();
    var b3=$("#preg4").val();
    var b4=$("#preg5").val();
    
	$.post("login.php",{a1:b,a2:b1,a3:b2,a4:b3,a5:b4,c1:vlr1,c2:vlr2,c3:vlr3,c4:vlr4,c5:vlr5,u:user},function(data){
		
		swal({
  			title: "Resultado",
  			text: ""+data,
  			type: "info",
  			showCancelButton: true,
  			confirmButtonColor: "#1B4F72",
  			confirmButtonText: "Publicar Facebook",
  			cancelButtonText: "Ok",
  			closeOnConfirm: true,
  			closeOnCancel: false
		},
		function(isConfirm){
  			if (isConfirm) {
  				postStory();
  			} else {
  				$("#form1").submit();
				$("#form2").submit();
				$("#form3").submit();
				$("#form4").submit();
				$("#form5").submit();
  			}
  		});
    });
  });
});

(function(d, s, id){
     var js, fjs = d.getElementsByTagName(s)[0];
     if (d.getElementById(id)) {return;}
     js = d.createElement(s); js.id = id;
     js.src = "https://connect.facebook.net/en_US/sdk.js";
     fjs.parentNode.insertBefore(js, fjs);
}(document, 'script', 'facebook-jssdk'));
</script>
	</body>
</html>