<!DOCTYPE html>
<html lang="en">


<!-- Mirrored from ambiance.vagebond.nl/html/template/menu/dinner/ by HTTrack Website Copier/3.x [XR&CO'2014], Sat, 31 Aug 2019 16:28:06 GMT -->
<!-- Added by HTTrack --><meta http-equiv="content-type" content="text/html;charset=utf-8" /><!-- /Added by HTTrack -->
<head>
    <meta charset="UTF-8">
    <title>COMIDA - YAHUA v1.0</title>

    <!-- mobile setup -->
    <meta name="viewport" content="width=device-width, initial-scale=1.0, maximum-scale=1.0, user-scalable=0" />
    <meta name="apple-mobile-web-app-title" content="yahua" />

    <!-- stylesheet -->
    <link rel="stylesheet" href="../../assets/css/style.css">

    <!-- Page description -->
    <meta name="description" content="">

    <!-- Facebook -->
    <meta property="og:title" content="dinner" />
    <meta property="og:type" content="article" />
    <meta property="og:url" content="index.html" />
    <meta property="og:description" content="" />

    <!-- Twitter -->
    <meta name="twitter:title" content="dinner" />
    <meta name="twitter:description" content="" />
    <!-- Favicons -->
    <link rel="apple-touch-icon" sizes="72x72" href="../../assets/img/favicons/apple-touch-icon-72x72.png">
    <link rel="apple-touch-icon" sizes="120x120" href="../../assets/img/favicons/apple-touch-icon-120x120.png">
    <link rel="apple-touch-icon" sizes="152x152" href="../../assets/img/favicons/apple-touch-icon-152x152.png">
    <link rel="icon" type="image/png" href="../../../img/logo1.png" sizes="96x96">

</head>

<body>

    <!-- Top shadow -->
    <div class="shadow"></div>
    <!-- end top shadow -->

    <!-- The splash screen -->
    <div id="splash">
        <div class="loader">
            <img class="splash-logo" src="../../../img/logfood.png" />
            <div class="line"></div>
        </div>
    </div>
    <!-- End of splash screen -->

    <div id="wrapper">
        <!-- main content -->
        <main>
            <!-- The header for content -->
            <header class="detail">
                <a href="../" class="back" data-transition="slide-from-top">
                    <h1>back</h1>
                </a>
                <section>
                    <h3 class="badge">Menu</h3>
                    <h1>¿Qué está cocinando?</h1>
                </section>
            </header>
            <!-- end header -->
            <div class="content-wrap">
                <div class="content">
                    <i class="icon bg icon-Food"></i>
                    <header class="with-subnav">
                        <ul>
                            <?php
                            $mbd = new PDO('mysql:host=localhost;dbname=foods', 'root', '1234');
                            $class = "";
                            $sql = "select tf.id,tf.description,tf.ruta from type_food tf inner join food f on f.typefood_id=tf.id and restaurant_id='".$_GET['_n']."' group by tf.id";
                            foreach($mbd->query($sql) as $fila) {
                                if($fila['description']==="CENA"){
                                    $class = "active";
                                }else{
                                    $class = "";
                                }
                            ?>
                                <li class="btn <?php echo $class; ?>">
                                    <a href="../<?php echo $fila['ruta']; ?>/?_n=<?php echo $_GET['_n']; ?>" data-transition="slide-left">
                                        <h4><?php echo $fila['description']; ?></h4>
                                    </a>
                                </li>
                            <?php }
                                $mbd = null;
                            ?>
                        </ul>
                    </header>

                    <?php
                        $mbd = new PDO('mysql:host=localhost;dbname=foods', 'root', '1234');
                        foreach($mbd->query("select tf.id,tf.description,tf.ruta from type_food tf inner join food f on f.typefood_id=tf.id and restaurant_id='".$_GET['_n']."' and tf.description='CENA' group by tf.id") as $fila) {
                    ?>
                    <!-- menu content -->
                    <header>
                        <h3 class="badge">• <?php echo $fila['description']; ?> •</h3>
                    </header>

                    <ul class="price-list">                        
                        <?php foreach($mbd->query("select * from food where restaurant_id='".$_GET['_n']."' and typefood_id='".$fila['id']."'") as $food) { ?>
                        <li>
                            <div class="top">
                                <img style="width: 100%; height: 300px;" src="../../../_fs/images/food/<?php echo $food['imagen']; ?>">
                                <h3 class="title"><?php echo $food['name']; ?></h3>
                                <h4 class="price badge"><?php echo 'S/. '.$food['price']; ?></h4>
                            </div>
                            <p class="description"><?php echo $food['ingredients']; ?></p>
                        </li>
                        <?php }
                            ?>
                    </ul>
                    <?php }
                        $mbd = null;
                    ?>

                    <aside class="notice">
                        <i class="icon bg inverted icon-Info"></i>
                        <h3>Chef&#039;s</h3>
                        <p>Nuestros mejores aperitivos para el gusto de nuestros clientes.</p>
                    </aside>

                    <!-- end menu content -->
                </div>
            </div>
            <div data-remodal-id="modal">
                <i class="icon bg icon-CommentwithLines"></i>
                <button data-remodal-action="close" class="remodal-close"></button>
                <h1></h1>
                <p></p>
                <div class="signature center">
                    <h6>YAHUA</h6>
                    <h5>Mejorando tu vida</h5>
                </div>
            </div>
        </main>
        <!-- end of main content -->
    </div>

    <!-- The slideshow -->
    <ul id="slideshow" data-speed="6000">
        <li>
            <img src="../../assets/img/slideshow/new/1.jpg" alt="slideshow image" />
        </li>
        <li>
            <img src="../../assets/img/slideshow/new/2.jpg" alt="slideshow image" />
        </li>
        <li>
            <img src="../../assets/img/slideshow/new/3.jpg" alt="slideshow image" />
        </li>
        <li>
            <img src="../../assets/img/slideshow/new/4.jpg" alt="slideshow image" />
        </li>
        <li>
            <img src="../../assets/img/slideshow/new/5.jpg" alt="slideshow image" />
        </li>
        <li>
            <img src="../../assets/img/slideshow/new/6.jpg" alt="slideshow image" />
        </li>
        <li>
            <img src="../../assets/img/slideshow/new/7.jpg" alt="slideshow image" />
        </li>
        <li>
            <img src="../../assets/img/slideshow/new/8.jpg" alt="slideshow image" />
        </li>
        <li>
            <img src="../../assets/img/slideshow/new/9.jpg" alt="slideshow image" />
        </li>
        <li>
            <img src="../../assets/img/slideshow/new/10.jpg" alt="slideshow image" />
        </li>
        <li>
            <img src="../../assets/img/slideshow/new/11.jpg" alt="slideshow image" />
        </li>
        <li>
            <img src="../../assets/img/slideshow/new/12.jpg" alt="slideshow image" />
        </li>
        <li>
            <img src="../../assets/img/slideshow/new/13.jpg" alt="slideshow image" />
        </li>
    </ul>
    <!-- end of slideshow -->

    <!-- Root element of PhotoSwipe. Must have class pswp. -->
    <div class="pswp" tabindex="-1" role="dialog" aria-hidden="true">

        <!-- Background of PhotoSwipe.
         It's a separate element as animating opacity is faster than rgba(). -->
        <div class="pswp__bg"></div>

        <!-- Slides wrapper with overflow:hidden. -->
        <div class="pswp__scroll-wrap">

            <!-- Container that holds slides.
            PhotoSwipe keeps only 3 of them in the DOM to save memory.
            Don't modify these 3 pswp__item elements, data is added later on. -->
            <div class="pswp__container">
                <div class="pswp__item"></div>
                <div class="pswp__item"></div>
                <div class="pswp__item"></div>
            </div>

            <!-- Default (PhotoSwipeUI_Default) interface on top of sliding area. Can be changed. -->
            <div class="pswp__ui pswp__ui--hidden">

                <div class="pswp__top-bar">

                    <!--  Controls are self-explanatory. Order can be changed. -->

                    <div class="pswp__counter"></div>

                    <button class="pswp__button pswp__button--close" title="Close (Esc)"></button>

                    <button class="pswp__button pswp__button--share" title="Share"></button>

                    <button class="pswp__button pswp__button--fs" title="Toggle fullscreen"></button>

                    <button class="pswp__button pswp__button--zoom" title="Zoom in/out"></button>

                    <!-- Preloader demo http://codepen.io/dimsemenov/pen/yyBWoR -->
                    <!-- element will get class pswp__preloader--active when preloader is running -->
                    <div class="pswp__preloader">
                        <div class="pswp__preloader__icn">
                            <div class="pswp__preloader__cut">
                                <div class="pswp__preloader__donut"></div>
                            </div>
                        </div>
                    </div>
                </div>

                <div class="pswp__share-modal pswp__share-modal--hidden pswp__single-tap">
                    <div class="pswp__share-tooltip"></div>
                </div>

                <button class="pswp__button pswp__button--arrow--left" title="Previous (arrow left)">
            </button>

                <button class="pswp__button pswp__button--arrow--right" title="Next (arrow right)">
            </button>

                <div class="pswp__caption">
                    <div class="pswp__caption__center"></div>
                </div>

            </div>

        </div>

    </div>
    <!-- scripts -->
    <script type="text/javascript" src="../../assets/js/pack.js"></script>
    <!-- end of scripts -->

    <script type="application/javascript">
        $(document).ready(function() {
            var currentUrl = window.location.href;

            $('body').on('pageActivated', function() {
                $('.back', '#wrapper').attr('href', currentUrl);
            });
        });
    </script>
</body>


<!-- Mirrored from ambiance.vagebond.nl/html/template/menu/dinner/ by HTTrack Website Copier/3.x [XR&CO'2014], Sat, 31 Aug 2019 16:28:06 GMT -->
</html>