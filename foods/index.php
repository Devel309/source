<!DOCTYPE html>
<html lang="en">

<meta http-equiv="content-type" content="text/html;charset=utf-8" />
<head>
    <meta charset="UTF-8">
    <title>COMIDA - YAHUA v1.0</title>

    <!-- mobile setup -->
    <meta name="viewport" content="width=device-width, initial-scale=1.0, maximum-scale=1.0, user-scalable=0" />
    <meta name="apple-mobile-web-app-title" content="yahua" />

    <!-- stylesheet -->
    <link rel="stylesheet" href="assets/css/style-sushi.css">

    <!-- Page description -->
    <meta name="description" content="">

    <!-- Facebook -->
    <meta property="og:title" content="Sushi" />
    <meta property="og:type" content="article" />
    <meta property="og:url" content="index.html" />
    <meta property="og:description" content="Here the excerp for this page" />

    <!-- Twitter -->
    <meta name="twitter:title" content="Sushi" />
    <meta name="twitter:description" content="Here the excerp for this page" />
    <!-- Favicons -->
    <link rel="apple-touch-icon" sizes="72x72" href="assets/img/favicons/apple-touch-icon-72x72.png">
    <link rel="apple-touch-icon" sizes="120x120" href="assets/img/favicons/apple-touch-icon-120x120.png">
    <link rel="apple-touch-icon" sizes="152x152" href="assets/img/favicons/apple-touch-icon-152x152.png">
    <link rel="icon" type="image/png" href="../img/logo1.png" sizes="96x96">

</head>

<body>

    <!-- Top shadow -->
    <div class="shadow"></div>
    <!-- end top shadow -->

    <!-- The splash screen -->
    <div id="splash">
        <div class="loader">
            <img class="splash-logo" src="../img/logfood.png" />
            <div class="line"></div>
        </div>
    </div>
    <!-- End of splash screen -->

    <div id="wrapper">
        <!-- main content -->
        <main>
            <header>
                <a href="../" class="logo" target="_new">
                    <h1>COMIDAS</h1>
                    <img src="../img/logfood.png" alt="food" />
                </a>
            </header>
            <!-- The navigation -->
            <nav class="strokes">
                <ul id="navigation">
                    <?php
                    try {
                        $mbd = new PDO('mysql:host=localhost;dbname=foods', 'root', '1234');
                        foreach($mbd->query('select id,name,image,address,phone from restaurant') as $fila) {
                            ?>
                    <li>
                        <a href="menu/?_n=<?php echo $fila['id']; ?>" data-transition="slide-to-top">
                            <section>
                                <h1><?php echo $fila['name']; ?></h1>
                                <img class="badge-rounded" style="width: 200px; height: 200px;" src="../_fs/images/logo/<?php echo $fila['image']; ?>" />
                                <h4 style="color: #fff;">Dirección: <?php echo $fila['address']; ?></h4>
                                <h4 style="color: #fff;">Teléfono: <?php echo $fila['phone']; ?></h4>
                            </section>
                            <footer>
                                <i class="icon icon-Food"></i>
                                <h5 class="serif">Nuestra Cocina</h5>
                                <p>Platos y bebidas</p>
                            </footer>
                        </a>
                    </li>
                    <?php }
                        $mbd = null;
                    } catch (PDOException $e) {
                        print "¡Error!: " . $e->getMessage() . "<br/>";
                        die();
                    }
                    ?>
                </ul>
            </nav>
            <!-- End navigation -->
            <div class="overlay"></div>
            <div data-remodal-id="modal">
                <i class="icon bg icon-CommentwithLines"></i>
                <button data-remodal-action="close" class="remodal-close"></button>
                <h1></h1>
                <p></p>
                <div class="signature center">
                    <h6>YAHUA</h6>
                    <h5>Mejorando tu vida</h5>
                </div>
            </div>
        </main>
        <!-- end of main content -->
    </div>

    <!-- The slideshow -->
    <ul id="slideshow" data-speed="6000">
        <li>
            <img src="assets/img/slideshow/sushi/1.jpg" alt="slideshow image" />
        </li>
        <li>
            <img src="assets/img/slideshow/sushi/2.jpg" alt="slideshow image" />
        </li>
        <li>
            <img src="assets/img/slideshow/sushi/3.jpg" alt="slideshow image" />
        </li>
        <li>
            <img src="assets/img/slideshow/sushi/4.jpg" alt="slideshow image" />
        </li>
        <li>
            <img src="assets/img/slideshow/sushi/5.jpg" alt="slideshow image" />
        </li>
        <li>
            <img src="assets/img/slideshow/sushi/6.jpg" alt="slideshow image" />
        </li>
    </ul>
    <!-- end of slideshow -->

    <!-- Root element of PhotoSwipe. Must have class pswp. -->
    <div class="pswp" tabindex="-1" role="dialog" aria-hidden="true">

        <!-- Background of PhotoSwipe.
         It's a separate element as animating opacity is faster than rgba(). -->
        <div class="pswp__bg"></div>

        <!-- Slides wrapper with overflow:hidden. -->
        <div class="pswp__scroll-wrap">

            <!-- Container that holds slides.
            PhotoSwipe keeps only 3 of them in the DOM to save memory.
            Don't modify these 3 pswp__item elements, data is added later on. -->
            <div class="pswp__container">
                <div class="pswp__item"></div>
                <div class="pswp__item"></div>
                <div class="pswp__item"></div>
            </div>

            <!-- Default (PhotoSwipeUI_Default) interface on top of sliding area. Can be changed. -->
            <div class="pswp__ui pswp__ui--hidden">

                <div class="pswp__top-bar">

                    <!--  Controls are self-explanatory. Order can be changed. -->

                    <div class="pswp__counter"></div>

                    <button class="pswp__button pswp__button--close" title="Close (Esc)"></button>

                    <button class="pswp__button pswp__button--share" title="Share"></button>

                    <button class="pswp__button pswp__button--fs" title="Toggle fullscreen"></button>

                    <button class="pswp__button pswp__button--zoom" title="Zoom in/out"></button>

                    <!-- Preloader demo http://codepen.io/dimsemenov/pen/yyBWoR -->
                    <!-- element will get class pswp__preloader--active when preloader is running -->
                    <div class="pswp__preloader">
                        <div class="pswp__preloader__icn">
                            <div class="pswp__preloader__cut">
                                <div class="pswp__preloader__donut"></div>
                            </div>
                        </div>
                    </div>
                </div>

                <div class="pswp__share-modal pswp__share-modal--hidden pswp__single-tap">
                    <div class="pswp__share-tooltip"></div>
                </div>

                <button class="pswp__button pswp__button--arrow--left" title="Previous (arrow left)">
            </button>

                <button class="pswp__button pswp__button--arrow--right" title="Next (arrow right)">
            </button>

                <div class="pswp__caption">
                    <div class="pswp__caption__center"></div>
                </div>

            </div>

        </div>

    </div>
    <!-- scripts -->
    <script type="text/javascript" src="assets/js/pack.js"></script>
    <!-- end of scripts -->

    <script type="application/javascript">
        $(document).ready(function() {
            var currentUrl = window.location.href;

            $('body').on('pageActivated', function() {
                $('.back', '#wrapper').attr('href', currentUrl);
            });
        });
    </script>
</body>

</html>