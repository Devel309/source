<!DOCTYPE html>
<html lang="en">
  <head>
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <meta name="description" content="">
    <meta name="author" content="">
    <title>Voz del Peruano</title>

    <!-- Bootstrap core CSS -->
    <link href="res/bootstrap3/css/bootstrap.css" rel="stylesheet">

    <!-- Add custom CSS here -->
    <link href="../res/lib/sb-admin.css" rel="stylesheet">
    <link rel="stylesheet" href="../res/lib/font-awesome/css/font-awesome.min.css">
    <script src="js/jquery-1.10.2.js"></script>
  </head>

  <body style="background: url('img/back.jpg'); background-size:cover; background-repeat: no-repeat;">

    <div id="wrapper">

      <!-- Sidebar -->
      <nav class="navbar navbar-inverse navbar-fixed-top" style="background:rgba(152,0,2,0.5)">
        <!-- Brand and toggle get grouped for better mobile display -->
        <center><label style="color:#FFFFFF; font-size:26px;">HAZ ESCUCHAR TU OPINION</label></center>
        <!-- Collect the nav links, forms, and other content for toggling -->
        <div class="collapse navbar-collapse navbar-ex1-collapse">
<?php 
$u=null;
if(isset($_SESSION["admin_id"]) && $_SESSION["admin_id"]!=""):
  $u = UserData::getById($_SESSION["admin_id"]);
 endif;?>

        </div><!-- /.navbar-collapse -->
      </nav>

      <div id="page-wrapper">

<?php 
  // puedo cargar otras funciones iniciales
  // dentro de la funcion donde cargo la vista actual
  // como por ejemplo cargar el corte actual
  Action::execute("login",array());
/*if(isset($_SESSION["admin_id"])){
  View::load("index");
}else{
  Action::execute("login",array());
}*/

?>
      </div><!-- /#page-wrapper -->

    </div><!-- /#wrapper -->

    <!-- JavaScript -->

<script type="text/javascript" src="js/jquery.min.js"></script>
<script src="res/bootstrap3/js/bootstrap.min.js"></script>
<script type="text/javascript" src="js/control.js"></script>
  </body>
</html>
