<?php
class CandData {
	public static $tablename = "cands";

	public  function createForm(){
		$form = new lbForm();
	    $form->addField("fullname",array('type' => new lbInputText(array("label"=>"Nombre")),"validate"=>new lbValidator(array())));
	    $form->addField("email",array('type' => new lbInputText(array()),"validate"=>new lbValidator(array())));
	    $form->addField("password",array('type' => new lbInputPassword(array()),"validate"=>new lbValidator(array())));
	    return $form;

	}

	public function CandData(){
		$this->name = "";
		$this->lastname = "";
		$this->email = "";
		$this->password = "";
		$this->created_at = "NOW()";
	}

	public function add(){
		$sql = "insert into vts_cands values (default,\"$this->com\",\"$this->cand\",now()) ";
		Executor::doit($sql);
	}
	
	public static function delById($id){
		$sql = "delete from ".self::$tablename." where id=$id";
		Executor::doit($sql);
	}
	
	public function del(){
		$sql = "delete from ".self::$tablename." where id=$this->id";
		Executor::doit($sql);
	}

	public function update(){
		$sql = "update ".self::$tablename." set username=\"$this->username\",fullname=\"$this->fullname\",email=\"$this->email\" where id=$this->id";
		Executor::doit($sql);
	}

	public static function getByImg($id){
		$sql = "select * from cands where img='$id'";
		$query = Executor::doit($sql);
		return Model::one($query[0],new CandData());
	}
	
	public static function getAll(){
		$sql = "select * from ".self::$tablename." u";
		$query = Executor::doit($sql);
		return Model::many($query[0],new CandData());
	}
	
	public static function getLike($q){
		$sql = "select * from ".self::$tablename." where fullname like '%$q%'";
		$query = Executor::doit($sql);
		$array = array();
		$cnt = 0;
		while($r = $query[0]->fetch_array()){
			$array[$cnt] = new CandData();
			$array[$cnt]->id = $r['id'];
			$array[$cnt]->fullname = $r['fullname'];
			$array[$cnt]->email = $r['email'];
			$array[$cnt]->password = $r['password'];
			$array[$cnt]->created_at = $r['created_at'];
			$cnt++;
		}
		return $array;
	}


}

?>