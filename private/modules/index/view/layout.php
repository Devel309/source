<!doctype html>
<html class="no-js" lang="">
<?php
$language = "";
if (isset($_GET['lang'])){
	$language = $_GET['lang'];
	if(!empty($language)){
		require("lang/".$language.".php");
	}
}else{
	$language = substr($_SERVER['HTTP_ACCEPT_LANGUAGE'],0,2);
	require("lang/".$language.".php");
}
?>
    <head>
        <meta charset="utf-8">
        <meta http-equiv="x-ua-compatible" content="ie=edge">
        <title><?php echo $lang['title']; ?></title>
        <meta name="description" content="">
        <meta name="viewport" content="width=device-width, initial-scale=1">
        <!--[if lt IE 9]> <script src="js/html5shiv.js"></script> 
        	<script src="js/respond.min.js"></script> <![endif]-->	
        <!-- Place favicon.ico in the root directory -->
        <link rel="icon" type="image/png" href="img/logo1.png" />
        <link rel="stylesheet" href="css/normalize.css">
        <link rel="stylesheet" href="css/main.css">
        <link rel="stylesheet" href="css/bootstrap.min.css">
        <link rel="stylesheet" href="css/font-awesome.min.css">
        <link rel="stylesheet" href="css/owl.carousel.css">
        <link rel="stylesheet" href="css/style.css">
        <link rel="stylesheet" type="text/css" href="js/sweetalert.css">
    </head>
<body >
		 <!-- start preloader -->
        <div id="loader-wrapper">
            <div class="logo"></div>
            <div id="loader">
            </div>
        </div>
        <!-- end preloader -->
        <!--[if lt IE 8]>
            <p class="browserupgrade">You are using an <strong>outdated</strong> browser. Please <a href="http://browsehappy.com/">upgrade your browser</a> to improve your experience.</p>
        <![endif]-->

<!-- Start Header Section -->
<header class="main_menu_sec navbar navbar-default navbar-fixed-top">
	<div class="container">
		<div class="row">
			<div class="col-lg-3 col-md-3 col-sm-12">
				<div class="lft_hd">
					<a href="./"><img src="img/logtop.png" alt=""/></a>
				</div>
			</div>			
			<div class="col-lg-9 col-md-9 col-sm-12">
				<div class="rgt_hd">					
					<div class="main_menu">
						<nav id="nav_menu">
							<button aria-controls="navbar" aria-expanded="false" data-target="#navbar" data-toggle="collapse" class="navbar-toggle collapsed" type="button">
							<span class="icon-bar"></span>
							<span class="icon-bar"></span>
							<span class="icon-bar"></span>
							</button>	
						<div id="navbar">
							<ul>
								<li><a href="./"><i class="fa fa-home fa-lg" aria-hidden="true"></i></a></li>
								<li><a href="?view=vm&lang=<?php echo $language; ?>"><?php echo $lang['vis'].' - '.$lang['mis']; ?></a></li>
								<li><a href="?view=consult&lang=<?php echo $language; ?>">Consultorias</a></li>
								<li><a href="./">Tiendas</a></li>
								<li><a href="./">Comidas</a></li>
								<li><a href="./">Hospedajes</a></li>
								<li><a href="./">Noticias</a></li>
								<!--li><a class="page-scroll" href="./"><?php echo $lang['home']; ?></a></li-->
								<!--li><a href="#"><?php echo $lang['sites']; ?> <i class="fa fa-angle-down"></i></a>
									<ul>
										<li><a class="page-scroll" href="#counting_sec"><?php echo $lang['client']; ?></a></li>
										<li><a class="page-scroll" href="#tm_sec"><?php echo $lang['team']; ?></a></li>
										<li><a class="page-scroll" href="#tstm_sec"><?php echo $lang['test']; ?></a></li>
										<li><a class="page-scroll" href="#pricing_sec"><?php echo $lang['price']; ?></a></li>
										<li><a class="page-scroll" href="#"><?php echo $lang['ht']; ?></a></li>
									</ul>
								</li--> 						

								<!--li><a href="#"><?php echo $lang['page']; ?></a></li> 
							
								<li><a class="page-scroll" href="#abt_sec"><?php echo $lang['about']; ?> <i class="fa fa-angle-down"></i></a>
                                	<ul>
                                    	<li><a class="page-scroll" href="?view=vm&lang=<?php echo $language; ?>"><?php echo $lang['vis'].' - '.$lang['mis']; ?></a></li>
	                                </ul>
                                </li>
								<li><a class="page-scroll" href="#skill_sec"><?php echo $lang['skill']; ?></a></li>
								<li><a class="page-scroll" href="#pr_sec"><?php echo $lang['serv']; ?><i class="fa fa-angle-down"></i></a>
                                	<ul>
                                		<li><a class="page-scroll" href="evo/?&lang=<?php echo $language; ?>"><?php echo $lang['consult']; ?></a></li>
                                		<li><a class="page-scroll" href="store"><?php echo $lang['sales']; ?></a></li>
									</ul>
                                </li>
								<li><a class="page-scroll" href="#ctn_sec"><?php echo $lang['contact']; ?></a></li-->
                                <li><a href="#"> <!--i class="fa fa-language"></i--><img src="img/idiomas.png" height="24px" width="24px"></a>
                                <ul>
                                	<li><a class="page-scroll" href="?lang=en">English</a></li>
	                                <li><a class="page-scroll" href="?lang=es">Español</a></li>
                                    <li><a class="page-scroll" href="?lang=zh-tw">Chino (Tradicional)</a></li>
                                    <li><a class="page-scroll" href="?lang=ar">Arabe</a></li>
                                    <li><a class="page-scroll" href="?lang=hi">Hindi</a></li>
                                </ul></li>
							</ul>
						</div>		
						</nav>			
                        <!--div id="google_translate_element"></div><script type="text/javascript">
function googleTranslateElementInit() {
  new google.translate.TranslateElement({pageLanguage: 'en', layout: google.translate.TranslateElement.InlineLayout.SIMPLE, multilanguagePage: true}, 'google_translate_element');
}
</script><script type="text/javascript" src="//translate.google.com/translate_a/element.js?cb=googleTranslateElementInit"></script>
					</div-->
						
				</div>
			</div>
            
		</div>	
	</div>	
</header>
<!-- End Header Section -->

<?php  View::load("index"); ?>

<footer id="ft_sec">
	<div class="container">
		<div class="row">
			<div class="col-lg-12">
				<div class="ft">						
					<ul>
						<li><a href="https://web.facebook.com/YAHUA-556011321558248/" target="_new"><i class="fa fa-facebook"></i></a></li>
						<li><a href=""><i class="fa fa-twitter"></i></a></li>
						<li><a href=""><i class="fa fa-dribbble"></i></a></li>
						<li><a href=""><i class="fa fa-rss"></i></a></li>
						<li><a href=""><i class="fa fa-flickr"></i></a></li>
						<li><a href=""><i class="fa fa-pinterest"></i></a></li>
						<li><a href=""><i class="fa fa-linkedin"></i></a></li>
						<li><a href=""><i class="fa fa-skype"></i></a></li>
						<li><a href=""><i class="fa fa-google"></i></a></li>
					</ul>					
				</div>
				<ul class="copy_right">
					<li>&copy; Copyrigth 2019 - <?=date('Y')?> YAHUA </li>
					<li>Todos los derechos reservados</li>
				</ul>					
			</div>	
		</div>
	</div>
</footer>
<!--script type="text/javascript" src="http://code.jquery.com/jquery-1.9.1.min.js"></script-->
<script src="js/vendor/jquery-1.11.3.min.js"></script>
<script src="js/isotope.pkgd.min.js"></script>
<script src="js/bootstrap.min.js"></script>
<!--script src="js/jquery-ui.js"></script-->
<script src="js/appear.js"></script>
<script src="js/jquery.counterup.min.js"></script>
<script src="js/waypoints.min.js"></script>
<script src="js/owl.carousel.min.js"></script>
<script src="js/showHide.js"></script>
<script src="js/jquery.nicescroll.min.js"></script>
<script src="js/jquery.easing.min.js"></script>
<script src="js/scrolling-nav.js"></script>
<script src="js/plugins.js"></script>
<script src="js/sweetalert.min.js"></script>
<!--script src="https://maps.googleapis.com/maps/api/js"></script-->
<script src="js/main.js"></script>

<script type="text/javascript">

$(document).ready(function(){

   $('.show_hide').showHide({			 
		speed: 1000,  // speed you want the toggle to happen	
		easing: '',  // the animation effect you want. Remove this line if you dont want an effect and if you haven't included jQuery UI
		changeText: 1, // if you dont want the button text to change, set this to 0
		showText: 'View',// the button text to show when a div is closed
		hideText: 'Close' // the button text to show when a div is open
					 
	}); 


});

</script>
<script>
    jQuery(document).ready(function( $ ) {
        $('.counter').counterUp({
            delay: 10,
            time: 500
        });
    });
</script>

<script>

  //Hide Overflow of Body on DOM Ready //
$(document).ready(function(){
    $("body").css("overflow", "hidden");
});

// Show Overflow of Body when Everything has Loaded 
$(window).load(function(){
    $("body").css("overflow", "visible");        
    var nice=$('html').niceScroll({
	cursorborder:"5",
	cursorcolor:"#00AFF0",
	cursorwidth:"3px",
	boxzoom:true, 
	autohidemode:true
	});

});
</script>
<script>
function validar_email( email ) 
{
  var regex = /^([a-zA-Z0-9_\.\-])+\@(([a-zA-Z0-9\-])+\.)+([a-zA-Z0-9]{2,4})+$/;
    return regex.test(email) ? true : false;
}
                 
$(document).ready(function(e) {
                  
  $("#enviar").click(function(e) {
    e.preventDefault();
                          
    var b=$("#name").val();
    var b1=$("#email").val();
    var b2=$("#message").val();

    if(validar_email(b1)){
    	$.post("?action=mail",{a1:b,a2:b1,a3:b2},function(data){
                
    	//var va=data.substring(0,1);
    	//var dd=data.substring(1,1000);

    	var rpt = data;

    	if (rpt==1) {
    		$('#contact-form').trigger("reset");
    		sweetAlert("Correcto", "En unos momentos nos contactaremos contigo", "success");
    	}
    	});
    }else{
    	sweetAlert("Incorrecto", "Escriba Correo Electrónico Válido!", "error");
    }
  });
});

</script>
</body>
</html>