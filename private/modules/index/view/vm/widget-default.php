<?php
$language = "";
if (isset($_GET['lang'])){
	$language = $_GET['lang'];
	if(!empty($language)){
		require("lang/".$language.".php");
	}
}else{
	$language = substr($_SERVER['HTTP_ACCEPT_LANGUAGE'],0,2);
	require("lang/".$language.".php");
}
?>

<!-- End Header Section -->

<!-- start blog Section -->
<section id="blg_sec">
	<div class="container">
		<div class="row">
			<div class="title_sec">
				<h1 style="font-size: 38px; margin: 48px;"><?php echo mb_strtoupper($lang['vis'].' - '.$lang['mis']); ?></h1>
				<h2><?php echo $lang['pres1']; ?></h2>
			</div>		
			<div class="col-lg-8 col-md-8 col-sm-8 ">
				<div class="sngl_blg">						
					<!--img src="http://cdn.shopify.com/s/files/1/1039/5466/files/blog-img2.jpg?10828543012475550494" alt=""/ -->
                    <img src="img/vis.jpg" />
					<div class="post_info">
						<div class="post_intro">
							<h2><?php echo $lang['vis']; ?></h2>	
							<i class="fa fa-th-large"></i>									
						</div>					
					</div>
					<div class="post_content">
						<p style="color: #000; font-size: 18px; text-align: justify;"><?php echo $lang['cont_vis']; ?></p>
						<a href="./"><?php echo $lang['return']; ?> <i class="fa fa-long-arrow-right"></i></a>
					</div>
				</div>					
				
				<div class="sngl_blg">						
					<!--img src="http://cdn.shopify.com/s/files/1/1039/5466/files/blog-img3.jpg?16122351990094232768" alt=""/-->
                    <img src="img/mis.jpg"/>
					<div class="post_info">
						<div class="post_intro">
							<h2><?php echo $lang['mis']; ?></h2>	
							<i class="fa fa-th-large"></i>									
						</div>					
					</div>
					<div class="post_content">
						<p style="color: #000; font-size: 18px; text-align: justify;"><?php echo $lang['cont_mis']; ?></p>
						<a href="./"><?php echo $lang['return']; ?> <i class="fa fa-long-arrow-right"></i></a>
					</div>
				</div>
                				
			</div>				
			
			<div class="col-lg-4 col-md-4 col-sm-4">
				<div class="sidebar">
					<div class="widget">
						<h2><?php echo $lang['ftec']; ?></h2>
						<img src="img/tecuno.jpg" width="100%"/>
					</div>				
					<div class="widget">
                    	<h2></h2>
						<img src="img/tecdos.jpeg" width="100%"/>
					</div>				
					<div class="widget">
						<h2></h2>
						<img src="img/tectres.jpg" width="100%"/>
					</div>
					<div class="widget">
						<h2></h2>
						<img src="img/teccuatro.jpg" width="100%"/>
					</div>
                    <div class="post_content">
						<h2></h2>
						<a href="./"><?php echo $lang['return']; ?> <i class="fa fa-long-arrow-right"></i></a>
					</div>
				</div>
			</div>	
		</div>
	</div>
</section>
<!-- start Blog Section -->

<!-- start located map Section -->
<section id="ltd_map_sec">
<div class="container">
	<div class="row">
		<div class="col-lg-12">
			<div class="map">
			<h1><?php echo $lang['map']; ?></h1><a href="#slidingDiv" class="show_hide" rel="#slidingDiv"><i class="fa fa-angle-up"></i></a>
			<div id="slidingDiv">
				<div class="map_area">
					<iframe src="https://www.google.com/maps/embed?pb=!1m18!1m12!1m3!1d983.7223368269922!2d-77.52366907084323!3d-9.518342476932268!2m3!1f0!2f0!3f0!3m2!1i1024!2i768!4f13.1!3m3!1m2!1s0x0%3A0x1ae0c255ae946a9!2sYAHUA!5e0!3m2!1ses-419!2spe!4v1553291951875" width="100%" height="450" frameborder="0" style="border:0" allowfullscreen></iframe>
				</div>
			</div>	
			</div>
		</div>
	</div>
</div>
</section>