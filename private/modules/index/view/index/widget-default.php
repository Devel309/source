<?php
$language = "";
if (isset($_GET['lang'])){
	$language = $_GET['lang'];
	if(!empty($language)){
		require("lang/".$language.".php");
	}
}else{
	$language = substr($_SERVER['HTTP_ACCEPT_LANGUAGE'],0,2);
	require("lang/".$language.".php");
}
?>
<!-- start slider Section -->
<section id="abt_sec">
	<div class="container">
		<!--div class="row">
			<div class="col-lg-12 col-md-12 col-sm-12 col-xs12 ">
				<div class="title_sec">
					<h1><?php echo $lang['about']; ?></h1>
					<h2><?php echo $lang['pres1']; ?></h2>
				</div>			
			</div>		
			<div class="col-lg-12 col-md-12 col-sm-12 col-xs12 ">
				<div class="abt">
					<p><?php echo $lang['section1']; ?></p>
				</div>
			</div>			
		</div-->
		<div class="row">
			<div class="col-lg-12 col-md-12 col-sm-12 col-xs12 ">
				<div class="title_sec">
					<h1 style="color: #FFF; font-size: 38px; margin: 48px;">NOSOTROS</h1>
				</div>			
			</div>		
			<div class="col-lg-4 col-md-4 col-sm-4 col-xs4">
				<div class="abt">
					<p style="color: #FFF;">
						Somos una empresa de tecnología innovadora, contamos equipo de desarrollo altamente calificados, profesionales con más de 10 años de experiencia en desarrollo de diferentes tipos de soluciones tecnológicos.<br>
						Nosotros brindamos soluciones tecnológicas de fácil uso, hacemos que tu negocio se expanda rápidamente en nuestro red de clientes, porque no fabricamos sistemas sino presentamos soluciones a nuestros clientes.
					</p>
				</div>
			</div>
			<div class="col-lg-8 col-md-8 col-sm-8 col-xs8">
				<div class="abt_join">
					Únete a nuestro equipo
				</div>
			</div>		
		</div>
	</div>
</section>
<div class="row">
	<div class="col-md-6">
		<section id="tstm_sec" style="background: #FFFFFF;">
			<div class="row">
			<div class="col-lg-12 col-md-12 col-sm-12 col-xs12 ">
				<div class="title_sec">
					<h1><?php echo $lang['serv']; ?></h1>
					<h2><?php echo $lang['pres3']; ?></h2>
				</div>			
			</div>		
			<div class="col-lg-4 col-md-4 col-sm-4 col-xs-12">
				<div class="service">						
					<i class="fa fa-globe"></i>
					<h2><?php echo $lang['serv1']; ?></h2>
					<div class="service_hoverly">
						<i class="fa fa-globe"></i>
						<h2><?php echo $lang['serv1']; ?></h2>
						<p><?php echo $lang['info1']; ?></p>
					</div>
				</div>
			</div>				
			<div class="col-lg-4 col-md-4 col-sm-4 col-xs-12">
				<div class="service">						
					<i class="fa fa-paper-plane"></i>
					<h2><?php echo $lang['serv2']; ?></h2>
					<div class="service_hoverly">
						<i class="fa fa-paper-plane"></i>
						<h2><?php echo $lang['serv2']; ?></h2>
						<p><?php echo $lang['info2']; ?></p>
					</div>
				</div>
			</div>
            <div class="col-lg-4 col-md-4 col-sm-4 col-xs-12">
				<div class="service">						
					<i class="fa fa-android"></i>
					<h2><?php echo $lang['serv3']; ?></h2>
					<div class="service_hoverly">
						<i class="fa fa-android"></i>
						<h2><?php echo $lang['serv3']; ?></h2>
						<p><?php echo $lang['info3']; ?></p>
					</div>
				</div>
			</div>	
            <div class="col-lg-4 col-md-4 col-sm-4 col-xs-12">
				<div class="service">						
					<i class="fa fa-quote-right"></i>
					<h2><?php echo $lang['serv4']; ?></h2>
					<div class="service_hoverly">
						<i class="fa fa-quote-right"></i>
						<h2><?php echo $lang['serv4']; ?></h2>
						<p><?php echo $lang['info4']; ?></p>
					</div>
				</div>
			</div>			
			<div class="col-lg-4 col-md-4 col-sm-4 col-xs-12">
				<div class="service">						
					<i class="fa fa-cloud"></i>
					<h2><?php echo $lang['serv5']; ?></h2>
					<div class="service_hoverly">
						<i class="fa fa-cloud"></i>
						<h2><?php echo $lang['serv5']; ?></h2>
						<p><?php echo $lang['info5']; ?></p>
					</div>
				</div>
			</div>				
			<div class="col-lg-4 col-md-4 col-sm-4 col-xs-12">
				<div class="service">						
					<i class="fa fa-paint-brush"></i>
					<h2><?php echo $lang['serv6']; ?></h2>
					<div class="service_hoverly">
						<i class="fa fa-paint-brush"></i>
						<h2><?php echo $lang['serv6']; ?></h2>
						<p><?php echo $lang['info6']; ?></p>
					</div>
				</div>
			</div>
		</section>
	</div>
	<div class="col-md-6">
		<section id="slider_sec">
			<div class="slider">
				<div id="carousel-example-generic" class="carousel slide" data-ride="carousel">
				<!-- Indicators -->
				  <ol class="carousel-indicators">
					<li data-target="#carousel-example-generic" data-slide-to="0" class="active"></li>
					<li data-target="#carousel-example-generic" data-slide-to="1"></li>
					<li data-target="#carousel-example-generic" data-slide-to="2"></li>
				  </ol>

				  <!-- Wrapper for slides -->
				  <div class="carousel-inner" role="listbox">
					<div class="item active">
						<div class="wrap_caption">
						  <div class="caption_carousel">
							<h1><?php echo $lang['slider1']; ?></h1>
							<p><?php echo $lang['content1']; ?></p>
						  </div>						
						</div>
					</div>					
					<div class="item">
						<div class="wrap_caption">
						  <div class="caption_carousel">
							<h1><?php echo $lang['slider2']; ?></h1>
							<p><?php echo $lang['content2']; ?></p>
						  </div>						
						</div>
					</div>					
					<div class="item ">
						<div class="wrap_caption">
						  <div class="caption_carousel">
							<h1><?php echo $lang['slider3']; ?></h1>
							<p><?php echo $lang['content3']; ?></p>
						  </div>						
						</div>
					</div>			
				  </div>
				</div>
			</div>
			</div>
		</section>
	</div>
</div>

<section id="counting_sec">
<div class="container">
	<div class="row">	

		<div class="col-lg-3 col-md-3 col-sm-6 col-xs-12">
			<div class="counting_sl">
			<i class="fa fa-user"></i>
			<h2 class="counter">1000</h2>
			<h4><?php echo $lang['clthappy']; ?></h4>	
			</div>
		</div>			
		<div class="col-lg-3 col-md-3 col-sm-6 col-xs-12">
			<div class="counting_sl">
			<i class="fa fa-desktop"></i>
			<h2 class="counter">250</h2>
			<h4><?php echo $lang['prjects']; ?></h4>	
			</div>
		</div>			
		<div class="col-lg-3 col-md-3 col-sm-6 col-xs-12">
			<div class="counting_sl">
			<i class="fa fa-ticket"></i>
			<h2 class="counter">1200</h2>
			<h4><?php echo $lang['aswplaces']; ?></h4>	
			</div>
		</div>			
		<div class="col-lg-3 col-md-3 col-sm-6 col-xs-12">
			<div class="counting_sl">
			<i class="fa fa-clock-o"></i>
			<h2 class="counter">8000</h2>
			<h4><?php echo $lang['devhorus']; ?></h4>	
			</div>
	</div>					
</div>
</section>
<section id="skill_sec">
	<div class="container">
		<div class="row">
			<div class="col-lg-12 col-md-12 col-sm-12 col-xs12 ">
				<!--div class="title_sec">
					<h1><?php echo $lang['skdgm']; ?></h1>
					<h2><?php echo $lang['pres2']; ?></h2>
				</div-->
				<div class="title_sec">
					<h1 style="font-size: 38px; color: #FFF; margin: 48px;">NUESTROS SERVICIOS</h1>
				</div>			
			</div>			
		  <div class="skills progress-bar1">		  
				<ul class="col-md-6 col-sm-12 col-xs-12 wow fadeInLeft">
					<li class="progress">
						<img class="img_skills" src="img/skill/responsivo.png">
						<div class="progress-bar" data-width="100">
							<?php echo $lang['top1']; ?>
						</div>
					</li>
					<li class="progress">
						<img class="img_skills" src="img/skill/diseno_grafico.jpg">
						<div class="progress-bar" data-width="100">
							<?php echo $lang['top2']; ?>
						</div>
					</li>
					<li class="progress">
						<img class="img_skills" src="img/skill/marketing.png">
						<div class="progress-bar" data-width="100">
							Marketing Digital
							<!--?php echo $lang['top3']; ?-->
						</div>
					</li>
					<li class="progress">
						<img class="img_skills" src="img/skill/posicionamiento.jpg">
						<div class="progress-bar" data-width="100">
							Posicionamiento en buscadores (SEO)
						</div>
					</li>
					<li class="progress">
                    	<img class="img_skills" src="img/skill/qlksense.png">
                      	<div class="progress-bar" data-width="100">
							Analitica de Big Data - Qlik Sense
							<!--?php echo $lang['top6']; ?-->
						</div>
					</li>
					<li class="progress">
						<img class="img_skills" src="img/skill/auditoria.jpg">
						<div class="progress-bar" data-width="100">
							Auditorías internas (Calidad ISO, Cobit)
							<!--?php echo $lang['top5']; ?-->
						</div>
					</li>
                    <li class="progress">
                    	<img class="img_skills" src="img/skill/camaras.jpg">
						<div class="progress-bar" data-width="100">
							Cámaras de seguridad
							<!--?php echo $lang['top7']; ?-->
						</div>
					</li>
                      <!--li class="progress">
							<div class="progress-bar" data-width="100">
								  <?php echo $lang['top8']; ?> 100%
							</div>
					  </li-->
				</ul>
				<ul class="col-md-6 col-sm-12 col-xs-12 wow fadeInRight">
					<li class="progress">
						<img class="img_skills" src="img/skill/ventas.png">
					  	<div class="progress-bar" data-width="100">
							<?php echo $lang['top9']; ?>
						</div>
					</li>
					<li class="progress">
						<img class="img_skills" src="img/skill/moviles.png">
					  	<div class="progress-bar" data-width="100">
							<?php echo $lang['top10']; ?>
						</div>
					</li>
					<li class="progress">
					  	<img class="img_skills" src="img/skill/consultoria.jpg">
					  	<div class="progress-bar" data-width="100">
							<?php echo $lang['top11']; ?>
						</div>
					</li>
					<li class="progress">
						<img class="img_skills" src="img/skill/moodle.jpg">
						<div class="progress-bar" data-width="100">
							Plataformas de aprendizaje - MOODLE
							<!--?php echo $lang['top12']; ?-->
						</div>
					</li>
                    <li class="progress">
                    	<img class="img_skills" src="img/skill/CRM.png">
						<div class="progress-bar" data-width="100">
							Gestión de relación con nuestros clientes (CRM)
							<!--?php echo $lang['top13']; ?-->
						</div>
					</li>
                    <li class="progress">
                    	<img class="img_skills" src="img/skill/seguridad.jpg">
						<div class="progress-bar" data-width="100">
							Seguridad informática
							<!--?php echo $lang['top14']; ?-->
						</div>
					</li>
                    <li class="progress">
                    	<img class="img_skills" src="img/skill/telecom.jpg">
						<div class="progress-bar" data-width="100">
							Redes y telecomunicaciones
							<!--?php echo $lang['top15']; ?-->
						</div>
					</li>
                      <!--li class="progress">
							<div class="progress-bar" data-width="100">
								  <?php echo $lang['top16']; ?> 100%
							</div>
					  </li-->
				</ul>
			</div>
		</div>
	</div>
</section>
<section id="tm_sec">
	<div class="container">
		<div class="row">
			<div class="col-lg-12 col-md-12 col-sm-12 col-xs12 ">
				<div class="title_sec">
					<h1 style="font-size: 38px; text-decoration: underline;">ALTA DIRECCIÓN</h1>
					<!--h1><?php echo $lang['team']; ?></h1-->
					<!--h2><?php echo $lang['pres1']; ?></h2-->
				</div>			
			</div>
			<div class="col-lg-4 col-md-4 col-sm-12">
				<div class="lts_pst">						
					<img class="img_rounded" src="img/mr.jpg" alt=""/>
					<h2>Maycol Yauri <span><?php echo $lang['founder']; ?></span></h2>
					<h3><?php echo $lang['manager']; ?></h3>
				</div>
			</div>
			<div class="col-lg-4 col-md-4 col-sm-12">
				<div class="lts_pst">						
					<img class="img_rounded" src="img/df.jpg" alt=""/>
					<h2>Daniel Florentino <span><?php echo $lang['founder_two']; ?></span></h2>
					<h3><?php echo $lang['first_man']; ?></h3>
				</div>
			</div>
			<div class="col-lg-4 col-md-4 col-sm-12">
				<div class="lts_pst">						
					<img class="img_rounded" src="img/if.jpg" alt=""/>
					<h2>Ines Florentino <span><?php echo $lang['sharehr']; ?></span></h2>
					<h3><?php echo $lang['manager_two']; ?></h3>
				</div>
			</div>
		</div>
	</div>
</section>
<section id="tstm_sec">
	<div class="container">
		<div class="row">
			<div class="col-lg-12" style="color: #FFFFFF; text-align: center;">
				<i class="fa fa-comment"></i>
				<h1 style="color: #FFF;">Nuestra Meta</h1>
				<p style="color: #FFFFFF;">Es brindar la mejor solución tecnológica a nivel nacional e internacional, y que formes parte de nuestro grupo emprendedor.</p>
				<p style="color: #FFFFFF;">"El que persevera y no se rinde sobre toda dificultad obtendrá el éxito "</p>
				<!--h3 style="color: #FFFFFF;"><?php echo $lang['suport']; ?></h3>
				<p style="color: #FFFFFF;"><?php echo $lang['supcont']; ?></p>
				<h6 style="color: #FFFFFF;">- <?php echo $lang['say']; ?></h6-->
			</div>	
		</div>
	</div>
</section>
<!--section id="pricing_sec">
	<div class="container">
		<div class="row">
			<div class="col-lg-12 col-md-12 col-sm-12 col-xs12 ">
				<div class="title_sec">
					<h1><?php echo $lang['plan']; ?></h1>
					<h2><?php echo $lang['pres3']; ?></h2>
				</div>			
			</div>		
			<div class="col-lg-4 col-md-4 col-sm-12">
				<div class="sngl_pricing">											
					<h2 class="title_bg_1"><?php echo $lang['basic']; ?></h2>
					<h3><span class="currency">S/</span>10<span class="monuth">/  <?php echo $lang['month']; ?></span></h3>
					<ul>
						<li><?php echo $lang['descb1']; ?></li>
						<li><?php echo $lang['sales']; ?></li>
						<li><?php echo $lang['descb3']; ?> 10 <?php echo $lang['clt']; ?></li>
						<li><?php echo $lang['descb5']; ?></li>
						<li><a href="" class="btn pricing_btn"><?php echo $lang['send']; ?></a></li>
						
					</ul>		
				</div>
			</div>			
			<div class="col-lg-4 col-md-4 col-sm-12">
				<div class="sngl_pricing">											
					<h2 class="title_bg_2"><?php echo $lang['standard']; ?></h2>
					<h3><span class="currency">S/</span>30<span class="monuth">/  <?php echo $lang['month']; ?></span></h3>
					<ul>
						<li><?php echo $lang['descb2']; ?></li>
						<li><?php echo $lang['sales']; ?></li>
						<li><?php echo $lang['descb3']; ?> 1000 <?php echo $lang['clt']; ?></li>
						<li><?php echo $lang['descb6']; ?></li>
						<li><a href="" class="btn pricing_btn"><?php echo $lang['send']; ?></a></li>
					</ul>		
				</div>
			</div>			
			<div class="col-lg-4 col-md-4 col-sm-12">
				<div class="sngl_pricing">											
					<h2 class="title_bg_3"><?php echo $lang['extended']; ?></h2>
					<h3><span class="currency">S/</span>45<span class="monuth">/  <?php echo $lang['month']; ?></span></h3>
					<ul>
						<li><?php echo $lang['descb2']; ?></li>
						<li><?php echo $lang['sales']; ?></li>
						<li><?php echo $lang['descb4'].$lang['clt']; ?></li>
                        <li><?php echo $lang['descb7']; ?></li>
						<li><a href="" class="btn pricing_btn"><?php echo $lang['send']; ?></a></li>
					</ul>		
				</div>
			</div>	
		</div>
	</div>
</section-->
<section id="ctn_sec">
	<div class="container">
		<div class="row">
			<div class="col-lg-12 col-md-12 col-sm-12 col-xs12 ">
				<div class="title_sec">
					<h1><?php echo $lang['infoc']; ?></h1>
					<h2>Escríbenos para mayor información</h2>
					<!--h2><?php echo $lang['pres1']; ?></h2-->
				</div>			
			</div>		
			<div class="col-sm-6"> 
				<div id="cnt_form">
					<form id="contact-form" class="contact" name="contact-form" method="post" action="">
						<div class="form-group">
						<input type="text" id="name" name="name" class="form-control name-field" required="required" placeholder="<?php echo $lang['name']; ?>" autocomplete="off">
						</div>
						<div class="form-group">
							<input type="email" id="email" name="email" class="form-control mail-field" required="required" placeholder="<?php echo $lang['email']; ?>" autocomplete="off">
						</div> 
						<div class="form-group">
							<textarea name="message" id="message" required class="form-control" rows="8" placeholder="<?php echo $lang['message']; ?>"></textarea>
						</div> 
						<div class="form-group">
							<button type="submit" id="enviar" name="enviar" class="btn btn-primary"><?php echo $lang['send']; ?></button>
						</div>
					</form> 
				</div>
				
			</div>
			<div class="col-lg-6 col-md-6 col-sm-6">
				<div class="cnt_info">
					<ul>
						<li><i class="fa fa-facebook"></i><p><a href="https://web.facebook.com/YAHUA-556011321558248/" target="_new">Facebook: YAHUA</a></p></li>
						<li><i class="fa fa-envelope"></i><p><a href="mailto:info@yahuaweb.com">info@yahuaweb.com</a></p></li>
						<li><i class="fa fa-phone"></i><p><a href="tel:+51925459244">+51-925459244</a> / <a href="tel:+51943195101">+51-943195101</a></p></li>
					</ul>
				</div>
			</div>			
		</div>
	</div>
</section>
<section id="ltd_map_sec">
<div class="container">
	<div class="row">
		<div class="col-lg-12">
			<div class="map">						
			<h1><?php echo $lang['map']; ?></h1><a href="#slidingDiv" class="show_hide" rel="#slidingDiv"><i class="fa fa-angle-up"></i></a>
			<div id="slidingDiv">
				<div class="map_area">
					<iframe src="https://www.google.com/maps/embed?pb=!1m18!1m12!1m3!1d983.7223368269922!2d-77.52366907084323!3d-9.518342476932268!2m3!1f0!2f0!3f0!3m2!1i1024!2i768!4f13.1!3m3!1m2!1s0x0%3A0x1ae0c255ae946a9!2sYAHUA!5e0!3m2!1ses-419!2spe!4v1553291951875" width="100%" height="450" frameborder="0" style="border:0" allowfullscreen></iframe>
				</div>
			</div>	
			</div>
		</div>
	</div>
</div>
</section>