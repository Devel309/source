<?php
if (isset($_GET['lang'])){
	$language = $_GET['lang'];
	if(!empty($language)){
		require("lang/".$language.".php");
	}
}else{
	$language = substr($_SERVER['HTTP_ACCEPT_LANGUAGE'],0,2);
	require("lang/".$language.".php");
}
?>

<section id="consults">
	<div class="container">
		<div class="row">
			<div class="title_sec">
				<h1 style="color: #FFF; font-size: 38px; margin: 48px;">Consultoría</h1>
			</div>
			<div class="col-lg-4 col-md-4 col-sm-4 col-xs4">
				<div class="body_sec">
					<p style="color: #FFF;">
						Prestamos servicio de sistema de consultoria para todos los consultores, el costo es de S/. 50.00 mensual incluido el IGV, soporte, disponibilidad de 24 hrs x 7 dias x 365 días anual, el primer mes es de prueba usted tendrá a su disposición de un sistema con todas las funcionalidades.<br>
						El sistema de consultorias es un sistema ERP, nosotros no fabricamos para un problema específico por el contrario solucionamos problemas.<br>
					</p>
					<a style="color: #00C6D7;" href="evo/?&lang=<?php echo $language; ?>">Abrir enlace <i class="fa fa-long-arrow-right"></i></a>
				</div>
			</div>
			<div class="col-lg-8 col-md-8 col-sm-8 col-xs8">
				<video class="play" src="storage/" autoplay type="video/mp4"></video>
			</div>
		</div>
	</div>
</section>