<?php
class ConsultData {
	public static $tablename = "consult";
	public static $vlr="";

	public function ConsultData(){
		$this->name = "";
		$this->lastname = "";
		$this->email = "";
		$this->password = "";
		$this->created_at = "NOW()";
	}

	public function add(){
		$sql = "insert into ".self::$tablename." values (default,\"$this->msm\",now(),\"$this->cid\",'0')";
		Executor::doit($sql);
	}

	public static function delById($id){
		$sql = "delete from ".self::$tablename." where id=$id";
		Executor::doit($sql);
	}
	public function del(){
		$sql = "delete from ".self::$tablename." where id=$this->id";
		Executor::doit($sql);
	}

	public function update(){
		$sql = "update ".self::$tablename." set name=\"$this->name\",lastname=\"$this->lastname\",email=\"$this->email\",phone=\"$this->phone\",address=\"$this->address\",is_active=$this->is_active,Dni=\"$this->dni\" where id=$this->id";
		Executor::doit($sql);
	}

	public static function getById($id){
		$sql = "select * from ".self::$tablename." where id=$id";
		$query = Executor::doit($sql);
		return Model::one($query[0],new ConsultData());
	}
	
	public static function getByClient($id){
		$sql = "select * from ".self::$tablename." where client_id=$id";
		$query = Executor::doit($sql);
		return Model::many($query[0],new ConsultData());
	}
	
	public static function getAll(){
		$sql = "select *,date(created_at) datec from ".self::$tablename." order by created_at";
		$query = Executor::doit($sql);
		return Model::many($query[0],new ConsultData());
	}
	
	public static function getDates(){
		$sql = "select date(created_at) datec from ".self::$tablename." group by date(created_at)";
		$query = Executor::doit($sql);
		return Model::many($query[0],new ConsultData());
	}
	
	public static function getEmp(){
		$sql = "select * from employe";
		$query = Executor::doit($sql);
		return Model::many($query[0],new ConsultData());
	}
	
	public static function getConvs($dd){
		$sql = "select * from ".self::$tablename." where date(created_at)='$dd'";
		$query = Executor::doit($sql);
		return Model::many($query[0],new ConsultData());
	}
}

?>