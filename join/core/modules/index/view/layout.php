<!DOCTYPE html>
<html>
<head>
<title>FOODS - YAHUA</title>
<meta name="viewport" content="width=device-width, initial-scale=1">
<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
<meta name="keywords" content="" />
<script type="application/x-javascript"> addEventListener("load", function() { setTimeout(hideURLbar, 0); }, false); function hideURLbar(){ window.scrollTo(0,1); } </script>
<link rel="icon" type="image/png" href="../img/logo1.png" />
 <!-- Bootstrap Core CSS -->
<link href="../evo/css/bootstrap.min.css" rel='stylesheet' type='text/css' />
<!-- Custom CSS -->
<link href="css/mrys_reg.css" rel='stylesheet' type='text/css' />
<!-- Graph CSS -->
<link href="../evo/css/font-awesome.css" rel="stylesheet"> 
<!-- jQuery -->
<link href='//fonts.googleapis.com/css?family=Roboto:700,500,300,100italic,100,400' rel='stylesheet' type='text/css'>
<!-- lined-icons -->
<link rel="stylesheet" href="../evo/css/icon-font.min.css" type='text/css' />
<!-- //lined-icons -->
<script src="../evo/js/jquery-1.10.2.min.js"></script>
<!--clock init-->
</head>
<body>
<!--/login-->
<div class="error_page">
<!--/login-top-->
  <div class="error-top">

<?php View::load('_i'); ?>

  </div>
</div>
<!--//login-top-->
<!--//login-->
<div class="footer"></div>
<!--js -->
<script src="../evo/js/jquery.nicescroll.js"></script>
<script src="../evo/js/scripts.js"></script>
<!-- Bootstrap Core JavaScript -->
<script src="../evo/js/bootstrap.min.js"></script>
</body>
</html>
