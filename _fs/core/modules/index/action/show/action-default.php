<?php

$form_data = json_decode(file_get_contents("php://input"));

$con = new ShowData();
$res = new ClientData();

if (empty($form_data->action)) {
	echo json_encode($con->getAllRest());
}elseif ($form_data->action==="food") {
	echo json_encode($con->getTypes());
}elseif ($form_data->action==="agregar") {
	$con->restaurant_id = $res->getById($_SESSION['client_id'])->id;
	$con->serie = $form_data->serie;
	$con->name = $form_data->name;
	$con->ingred = $form_data->ingred;
	$con->fileimg = $form_data->fileimg;
	$con->price = $form_data->price;
	$con->add();
}elseif ($form_data->action==="actualizar") {
	$con->id = $form_data->id;
	$con->serie = $form_data->serieupd;
	$con->name = $form_data->nameupd;
	$con->ingred = $form_data->ingredupd;
	$con->price = $form_data->priceupd;
	$con->update();
	if (strlen($form_data->fileimgupd)>0) {
		$con->fileimg = $form_data->fileimgupd;
		$con->updateimg();
	}
}elseif ($form_data->action==="eliminar") {
	$con->id = $form_data->id;
	$con->del();
	unlink('images/food/'.$form_data->image);
}elseif ($form_data->action==="client") {
	$con->restaurant_id = $res->getById($_SESSION['client_id'])->id;
	$con->description = $form_data->description;
	$con->image = $form_data->fileimg;
	$con->updrest();
}

?>