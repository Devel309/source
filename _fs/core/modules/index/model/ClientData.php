<?php
class ClientData {
	public static $tablename = "user";
	public static $vlr="";

	public function ClientData(){
		$this->name = "";
		$this->lastname = "";
		$this->email = "";
		$this->password = "";
		$this->created_at = "NOW()";
	}

	public function add(){
		$sql = "insert into user (username, password) value (\"$this->uname\",\"$this->rpsw\")";
		Executor::doit($sql);
		$sql = "insert into restaurant (name, description, address, image, phone, user_id) select \"$this->restaurant\" name,'' descrip,\"$this->address\" address,'' image,\"$this->phone\" phone,id user from ".self::$tablename." where username=\"$this->uname\"";
		Executor::doit($sql);
	}

	public static function delById($id){
		$sql = "delete from ".self::$tablename." where id=$id";
		Executor::doit($sql);
	}
	public function del(){
		$sql = "delete from ".self::$tablename." where id=$this->id";
		Executor::doit($sql);
	}

	public function update(){
		$sql = "update ".self::$tablename." set name=\"$this->name\",lastname=\"$this->lastname\",email=\"$this->email\",phone=\"$this->phone\",address=\"$this->address\",is_active=$this->is_active,Dni=\"$this->dni\" where id=$this->id";
		Executor::doit($sql);
	}
	
	public function update_passwd(){
		$sql = "update ".self::$tablename." set pass=\"$this->rpsw\" where id=$this->id";
		Executor::doit($sql);
	}
	
	public function update_email(){
		$sql = "update ".self::$tablename." set email=\"$this->email\" where id=$this->id";
		Executor::doit($sql);
	}
	
	public function update_info(){
		$sql = "update ".self::$tablename." set lastname=\"$this->lastname\",name=\"$this->firstname\",gender=\"$this->gender\",phone=\"$this->phone\" where id=$this->id";
		Executor::doit($sql);
	}
	
	public function update_image(){
		$sql = "update ".self::$tablename." set profile=\"$this->image\" where id=$this->id";
		Executor::doit($sql);
	}
	
	public function update_b(){
		$sql = "update ".self::$tablename." set nameb=\"$this->nameb\",employe_id=\"$this->eid\" where id=$this->id";
		Executor::doit($sql);
	}

	public function getFullname(){ return $this->name." ".$this->lastname; }

	public static function getById($id){
		$sql = "select * from restaurant where user_id='$id'";
		$query = Executor::doit($sql);
		return Model::one($query[0],new ClientData());
	}

	public static function getByUser($user){
		$sql = "select * from ".self::$tablename." where username=\"$user\"";
		$query = Executor::doit($sql);
		$array = array();
		$cnt = 0;
		while($r = $query[0]->fetch_array()){
			$array[$cnt] = new ClientData();
			$array[$cnt]->id = $r['id'];
			$array[$cnt]->username = $r['username'];
			$array[$cnt]->password = $r['password'];
			$cnt++;
		}
		return $array;
	}
	
	public static function getByProf($profile){
		$sql = "select * from ".self::$tablename." where profile=\"$profile\"";
		$query = Executor::doit($sql);
		$found = null;
		$data = new ClientData();
		while($r = $query[0]->fetch_array()){
			$data->id = $r['id'];
			$data->name = $r['lastname'];
			$data->profile = $r['profile'];
			$found = $data;
			break;
		}
		return $found;
	}
	
	public static function getAllByClientDni($id){
		$sql = "select * from ".self::$tablename." where Dni=\"$id\"";
		$query = Executor::doit($sql);
		$array = array();
		$cnt = 0;
		while($r = $query[0]->fetch_array()){
			$array[$cnt] = new ClientData();
			$array[$cnt]->id = $r['id'];
			$array[$cnt]->name = $r['name'];
			$array[$cnt]->lastname = $r['lastname'];
			$array[$cnt]->address = $r['address'];
			$array[$cnt]->phone = $r['phone'];
			$array[$cnt]->email = $r['email'];
			$array[$cnt]->password = $r['password'];
			$array[$cnt]->created_at = $r['created_at'];
			$cnt++;
		}
		return $array;
	}
	
	public static function getSearch($dat){
		$sql = "select * from ".self::$tablename." where concat(lastname,' ',name) like '%".$dat."%'";
		$query = Executor::doit($sql);
		$array = array();
		$cnt = 0;
		while($r = $query[0]->fetch_array()){
			$array[$cnt] = new ClientData();
			$array[$cnt]->dni = $r['Dni'];
			$array[$cnt]->id = $r['id'];
			$array[$cnt]->name = $r['name'];
			$array[$cnt]->lastname = $r['lastname'];
			$array[$cnt]->address = $r['address'];
			$array[$cnt]->phone = $r['phone'];
			$array[$cnt]->email = $r['email'];
			$array[$cnt]->password = $r['password'];
			$array[$cnt]->created_at = $r['created_at'];
			$cnt++;
		}
		return $array;
	}
	
	public static function getAll(){
		$sql = "select * from ".self::$tablename." order by lastname;";
		$query = Executor::doit($sql);
		$array = array();
		$cnt = 0;
		while($r = $query[0]->fetch_array()){
			$array[$cnt] = new ClientData();
			$array[$cnt]->dni = $r['Dni'];
			$array[$cnt]->id = $r['id'];
			$array[$cnt]->name = $r['name'];
			$array[$cnt]->lastname = $r['lastname'];
			$array[$cnt]->address = $r['address'];
			$array[$cnt]->phone = $r['phone'];
			$array[$cnt]->email = $r['email'];
			$array[$cnt]->password = $r['password'];
			$array[$cnt]->created_at = $r['created_at'];
			$cnt++;
		}
		return $array;
	}

	public static function getLike($q){
		$sql = "select * from ".self::$tablename." where name like '%$q%'";
		$query = Executor::doit($sql);
		$array = array();
		$cnt = 0;
		while($r = $query[0]->fetch_array()){
			$array[$cnt] = new ClientData();
			$array[$cnt]->id = $r['id'];
			$array[$cnt]->name = $r['name'];
			$array[$cnt]->mail = $r['mail'];
			$array[$cnt]->password = $r['password'];
			$array[$cnt]->created_at = $r['created_at'];
			$cnt++;
		}
		return $array;
	}
	
	public static function getVerify($rest){
		$sql = "select count(*) cant from restaurant where name='$rest'";
		$query = Executor::doit($sql);
		return Model::one($query[0],new ClientData());
	}
}

?>