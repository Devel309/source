<?php
class ShowData {
	public static $tablename = "food";
	public static $vlr="";

	public function ShowData(){
		$this->created_at = "NOW()";
	}

	public function add(){
		$sql = "insert into ".self::$tablename." (name, ingredients, imagen, price, restaurant_id, typefood_id) value (\"$this->name\",\"$this->ingred\",\"$this->fileimg\",\"$this->price\",\"$this->restaurant_id\",\"$this->serie\")";
		Executor::doit($sql);
	}

	public function payment(){
		$sql = "call payment(\"$this->sec\",\"$this->fec\",\"$this->trans\",\"$this->caj\",\"$this->ofic\",\"$this->monto\",\"$this->idcc\",\"$this->fini\",\"$this->ffin\",\"$this->idclt\")";
		Executor::doit($sql);
	}

	public static function delById($id){
		$sql = "delete from ".self::$tablename." where id='$id'";
		Executor::doit($sql);
	}
	public function del(){
		$sql = "delete from ".self::$tablename." where id=\"$this->id\"";
		Executor::doit($sql);
	}

	public function update(){
		$sql = "update ".self::$tablename." set name=\"$this->name\",ingredients=\"$this->ingred\",price=\"$this->price\",typefood_id=\"$this->serie\" where id=$this->id";
		Executor::doit($sql);
	}

	public function updateimg(){
		$sql = "update ".self::$tablename." set imagen=\"$this->fileimg\" where id=$this->id";
		Executor::doit($sql);
	}

	public function updrest(){
		$sql = "update restaurant set description=\"$this->description\",image=\"$this->image\" where id=\"$this->restaurant_id\"";
		Executor::doit($sql);
	}

	public static function getById($id){
		$sql = "select * from ".self::$tablename." where id=$id";
		$query = Executor::doit($sql);
		return Model::one($query[0],new ShowData());
	}
	
	public static function getAllRest(){
		$sql = "select id,name,image from restaurant";
		$query = Executor::doit($sql);
		return Model::many($query[0],new ShowData());
	}
	
	public static function getTypes(){
		$sql = "select * from type_food";
		$query = Executor::doit($sql);
		return Model::many($query[0],new ShowData());
	}

	public static function fechas(){
		$sql = "select concat(year(curdate()),mid(fecini,5,8)) fini,concat(year(curdate()),mid(fecfin,5,8)) ffin from months where curdate() between fecini and fecfin";
		$query = Executor::doit($sql);
		return Model::many($query[0],new ShowData());
	}
}

?>