<div class="pull-left breadcrumb_admin clear_both">
        <div class="pull-left page_title theme_color">
          <h1>Tablero</h1>
          <h2 class="">Comidas</h2>
        </div>
        <div class="pull-right">
          <ol class="breadcrumb">
            <li><a href="#">Tablero</a></li>
            <li class="active">Comidas</li>
          </ol>
        </div>
      </div>
      <div class="container clear_both padding_fix">
        <!--\\\\\\\ container  start \\\\\\-->
        
      <div class="row">
        <div class="col-md-6">
          <div class="block-web" style="background: rgba(53,139,234,0.5);">
            <div class="header">
              <h3 class="content-header">Actualizar información del restaurante</h3>
            </div>
            <div class="porlets-content">
              <form class="form-horizontal row-border" role="form">                 
                <div class="form-group">
                  <label class="col-sm-3 control-label">Descripción: </label>
                  <div class="col-sm-9">
                    <textarea class="form-control" rows="4" ng-model="description"></textarea>
                  </div>
                </div><!--/form-group--> 
            
                <div class="form-group">
                  <label class="col-sm-3 control-label">Logo / Insignia: </label>
                  <div class="col-sm-9">
                    <input id="fileimg" type="file" file-input="fileimg">
                  </div>
                </div><!--form-group-->
                
                <!--/form-group-->
                <div class="bottom">
                  <button type="submit" class="btn btn-primary" ng-click="subir()">Guardar</button>
                  <button type="reset" class="btn btn-default">Cancelar</button>
                </div>
              </form>
            </div><!--/porlets-content--> 
          </div><!--/block-web--> 
        </div><!--/col-md-6-->

      </div>

<script>
var app = angular.module('comidas', []);
app.directive("fileInput", function($parse){
  return{  
    link: function($scope, element, attrs){  
      element.on("change", function(event){  
        var files = event.target.files;  
        //console.log(files[0].name);  
        $parse(attrs.fileInput).assign($scope, element[0].files);  
        $scope.$apply();
      });
    }
  }
});
app.controller('controller', function($scope, $http, $location){

$scope.description = '';

  $scope.subir = function(){
    if($scope.description.length > 0){
      if ($('#fileimg').get(0).files.length > 0) {
        var formdata = new FormData();
        angular.forEach($scope.fileimg, function (file) {
          formdata.append("file", file);
        });

        $http.post(
          '?action=filert', formdata,
        {  
          transformRequest: angular.client_id,  
          headers: {'Content-Type': undefined,'Process-Data': false}  
        }).success(function(data){
          var v = data.substring(0,1);
          if (v=='3') {
            $scope.save(data.substring(1,data.length));
          }else{
            if (v=='2') {
              alert('El archivo ya existe!!!');
            }else{
              alert('Formato no válido, archivo posiblemente dañado!!!');
            }
          }
        });
      }else{
        alert('Elija el logo de su restaurante');
      }
    }else{
      alert('Escriba breve descripción de su restaurante');
    }
  };

  $scope.save = function(file){
    $http({
      method:"POST",
      url:"?action=food",
      data:{
        'description':$scope.description,
        'fileimg':file,
        'action':'client'
      }
    }).success(function(data){
      $scope.description = "";
      $('#fileimg').val('');
      window.location="./?view=food";
    });
  };

});

</script>

<script language="JavaScript">
function numeros(e){
    key = e.keyCode || e.which;
    tecla = String.fromCharCode(key).toLowerCase();
    letras = "0123456789";
    especiales = [];
 
    tecla_especial = false
    for(var i in especiales){
 if(key == especiales[i]){
     tecla_especial = true;
     break;
        } 
    }
 
    if(letras.indexOf(tecla)==-1 && !tecla_especial)
        return false;
}
</script>