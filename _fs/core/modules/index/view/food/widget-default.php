<?php
if (isset($_GET['lang'])){
  $language = $_GET['lang'];
  if(!empty($language)){
    require("../lang/".$language.".php");
  }
}else{
  $language = substr($_SERVER['HTTP_ACCEPT_LANGUAGE'],0,2);
  require("../lang/".$language.".php");
}
?>
<div class="pull-left breadcrumb_admin clear_both">
        <div class="pull-left page_title theme_color">
          <h1>Tablero</h1>
          <h2 class="">Comidas</h2>
        </div>
        <div class="pull-right">
          <ol class="breadcrumb">
            <li><a href="#">Tablero</a></li>
            <li class="active">Comidas</li>
          </ol>
        </div>
      </div>
      <div class="container clear_both padding_fix">
        <!--\\\\\\\ container  start \\\\\\-->
        
      <div class="row">
        <div class="col-md-6" ng-show="addnew">
          <div class="block-web" style="background: rgba(53,139,234,0.5);">
            <div class="header">
              <h3 class="content-header">Agregar nueva comida</h3>
            </div>
            <div class="porlets-content">
              <form class="form-horizontal row-border" role="form">
                <div class="form-group">
                  <label class="col-sm-3 control-label">Serie: </label>
                  <div class="col-sm-9">
                    <select class="form-control" ng-model="serie">
                      <option ng-repeat="s in series" value="{{s.id}}">{{s.description}}</option>
                    </select>
                  </div>
                </div><!--/form-group-->

                <div class="form-group">
                  <label class="col-sm-3 control-label">Nombre: </label>
                  <div class="col-sm-9">
                    <input type="text" class="form-control input-lg" placeholder="Escriba nombre de la comida" ng-model="name">
                  </div>
                </div><!--/form-group-->
                 
                <div class="form-group">
                  <label class="col-sm-3 control-label">Descripción: </label>
                  <div class="col-sm-9">
                    <textarea class="form-control" rows="4" ng-model="ingred"></textarea>
                  </div>
                </div><!--/form-group--> 
            
                <div class="form-group">
                  <label class="col-sm-3 control-label">Imagen: </label>
                  <div class="col-sm-9">
                    <input id="fileimg" type="file" file-input="fileimg">
                  </div>
                </div><!--form-group-->

                <div class="form-group">
                  <label class="col-sm-3 control-label">Precio: </label>
                  <div class="col-sm-9">
                    <input type="text" class="form-control input-lg" placeholder="Ingrese el precio" ng-model="price" onkeypress="return filterFloat(event,this);">
                  </div>
                </div><!--/form-group-->
                
                <!--/form-group-->
                <div class="bottom">
                  <button type="submit" class="btn btn-primary" ng-click="subir()">Guardar</button>
                  <button type="reset" class="btn btn-default">Cancelar</button>
                </div>
              </form>
            </div><!--/porlets-content--> 
          </div><!--/block-web--> 
        </div><!--/col-md-6--> 

        <div class="col-md-6" ng-show="actual">
          <div class="block-web alert-danger" style="background: rgba(246,165,4,0.28);">
            <div class="header">
              <h3 class="content-header">Editar comida</h3>
            </div>
            <div class="porlets-content">
              <form class="form-horizontal row-border" role="form">
                <div class="form-group">
                  <label class="col-sm-3 control-label">Serie: </label>
                  <div class="col-sm-9">
                    <select class="form-control" ng-model="serieupd">
                      <option ng-repeat="s in series" value="{{s.id}}">{{s.description}}</option>
                    </select>
                  </div>
                </div><!--/form-group-->

                <div class="form-group">
                  <label class="col-sm-3 control-label">Nombre: </label>
                  <div class="col-sm-9">
                    <input type="text" class="form-control input-lg" placeholder="Escriba nombre de la comida" ng-model="nameupd">
                  </div>
                </div><!--/form-group-->
                 
                <div class="form-group">
                  <label class="col-sm-3 control-label">Ingredientes: </label>
                  <div class="col-sm-9">
                    <textarea class="form-control" rows="4" ng-model="ingredupd"></textarea>
                  </div>
                </div><!--/form-group--> 

                <div class="form-group">
                  <label class="col-sm-3 control-label"></label>
                  <div class="col-sm-9">
                    <label class="checkbox-inline">
                      <input type="checkbox" ng-model="change" ng-change="tochange()">
                       Cambiar imagen </label>
                  </div>
                </div><!--/form-group--> 

                <div class="form-group" ng-show="active">
                  <label class="col-sm-3 control-label">Imagen: </label>
                  <div class="col-sm-9">
                    <input id="fileimgupd" type="file" file-input="fileimgupd">
                  </div>
                </div><!--form-group-->

                <div class="form-group">
                  <label class="col-sm-3 control-label">Precio: </label>
                  <div class="col-sm-9">
                    <input type="text" class="form-control input-lg" placeholder="Ingrese el precio" ng-model="priceupd" onkeypress="return filterFloat(event,this);">
                  </div>
                </div><!--/form-group-->
                
                <!--/form-group-->
                <div class="bottom">
                  <button type="submit" class="btn btn-primary" ng-click="cambiarimg()">Guardar</button>
                  <button type="reset" class="btn btn-default" ng-click="cancel()">Cancelar</button>
                </div>
              </form>
            </div><!--/porlets-content--> 
          </div><!--/block-web--> 
        </div><!--/col-md-6--> 
      </div>

      <div class="row">
        <div class="col-md-12">
          <div class="block-web">
            <div class="header">              
              <h3 class="content-header">Lista de comidas registradas</h3>
            </div>
         <div class="porlets-content">
          <div class="adv-table editable-table ">
            <div class="clearfix">
              <!--div class="btn-group pull-right">
                <button class="btn dropdown-toggle" data-toggle="dropdown">Tools <i class="fa fa-angle-down"></i>
                </button>
                <ul class="dropdown-menu pull-right">
                  <li><a href="#">Print</a></li>
                  <li><a href="#">Save as PDF</a></li>
                  <li><a href="#">Export to Excel</a></li>
                </ul>
              </div-->
              Buscar: <input type="text" ng-model="search">
            </div>
            <div class="margin-top-10"></div>
            <div class="table-responsive">
              <table class="table table-striped table-hover table-bordered" id="editable-sample">
                <thead>
                  <tr>
                    <th>Comida</th>
                    <th>Descripción</th>
                    <th>Precio</th>
                    <th>Tipo Comida</th>
                    <th>Acción</th>
                  </tr>
                </thead>
                <tbody>
                  <tr ng-repeat="c in comidas | filter : search">
                    <td>{{c.name}}</td>
                    <td>{{c.ingredients}}</td>
                    <td>{{c.price}}</td>
                    <td class="center">{{c.typefood}}</td>
                    <td>
                      <div class="btn-group">
                        <button class="btn btn-primary dropdown-toggle" data-toggle="dropdown"> Acción <span class="caret"></span> </button>
                        <ul class="dropdown-menu">
                          <li><a class="btn btn-warning" ng-click="edit(c.id)"><i class="fa fa-edit"></i> Editar</a> </li>
                          <li> <a class="btn btn-danger" ng-click="delete(c.id,c.imagen)"><i class="fa fa-times"></i> Eliminar</a> </li>
                        </ul>
                      </div>
                    </td>
                  </tr>
                </tbody>
              </table>
            </div>
            </div>
           </div><!--/porlets-content-->  
          </div><!--/block-web--> 
        </div><!--/col-md-12--> 
      </div><!--/row-->

<script type="application/javascript">
  $(document).ready(function() {
    $('#optone').addClass('theme_color');
  });

var app = angular.module('comidas', []);
app.directive("fileInput", function($parse){
  return{  
    link: function($scope, element, attrs){  
      element.on("change", function(event){  
        var files = event.target.files;  
        //console.log(files[0].name);  
        $parse(attrs.fileInput).assign($scope, element[0].files);  
        $scope.$apply();
      });
    }
  }
});
app.controller('controller', function($scope, $http, $location){

$scope.serie = '';
$scope.name = '';
$scope.ingred = '';
$scope.image = '';
$scope.price = '';
$scope.active = false;
$scope.addnew = true;
$scope.actual = false;
$scope.idupd = '';

$http({
  method:"POST",
  url:"?action=food",
  data:{
    'action':'food'
  }
}).success(function(data){
  $scope.series = data;
});

$http.get('?action=food').success(function(data){
  $scope.comidas = data;
});

  $scope.subir = function(){
   if($scope.serie.length > 0){
    if ($scope.name.length > 0) {
     if ($scope.ingred.length > 0) { 
       if ($('#fileimg').get(0).files.length > 0) {
        if ($scope.price.length > 0) {
          var formdata = new FormData();
          angular.forEach($scope.fileimg, function (file) {
            formdata.append("file", file);
          });

          $http.post(
            '?action=file', formdata,
          {  
            transformRequest: angular.client_id,  
            headers: {'Content-Type': undefined,'Process-Data': false}  
          }).success(function(data){
            var v = data.substring(0,1);
            if (v=='3') {
              $scope.create(data.substring(1,data.length));
            }else{
              if (v=='2') {
                alert('El archivo ya existe!!!');
              }else{
                alert('Formato no válido, archivo posiblemente dañado!!!');
              }
            }
          });
        }else{
          alert('Escriba el precio de la comida');
        }
      }else{
       alert('Selecciona la imagen de la comida');
      }
     }else{
      alert('Escriba los ingredientes de la comida');
     }
    }else{
      alert('Escriba el nombre de la comida');
    }
   }else{
    alert('Elija serie del plato');
   }
  };

  $scope.listar = function(){
    $http.get('?action=food').success(function(data){
      $scope.comidas = data;
    });
  };

  $scope.create = function(file){
    $http({
      method:"POST",
      url:"?action=food",
      data:{
        'serie':$scope.serie,
        'name':$scope.name,
        'ingred':$scope.ingred,
        'fileimg':file,
        'price':$scope.price,
        'action':'agregar'
      }
    }).success(function(data){
      $scope.serie = "";
      $scope.name = "";
      $scope.ingred = "";
      $('#fileimg').val('');
      $scope.price = "";
      $scope.listar();
    });
  };

  $scope.edit = function(id){
    for (var i = $scope.comidas.length - 1; i >= 0; i--) {
      if ($scope.comidas[i].id==id) {
        $scope.idupd = id;
        $scope.serieupd = $scope.comidas[i].typefood_id;
        $scope.nameupd = $scope.comidas[i].name;
        $scope.imgbefore = $scope.comidas[i].imagen;
        $scope.ingredupd = $scope.comidas[i].ingredients;
        $scope.priceupd = $scope.comidas[i].price;
      }
    }
    $scope.addnew = false;
    $scope.actual = true;
  };

  $scope.tochange = function(){
    if($scope.change){
      $scope.active = true;
    }else{
      $scope.active = false;
    }
  };

  $scope.cancel = function(){
    $scope.addnew = true;
    $scope.actual = false;
  };

  $scope.cambiarimg = function(){
   if($scope.serieupd.length > 0){
    if ($scope.nameupd.length > 0) {
     if ($scope.ingredupd.length > 0) { 
       if ($scope.change==true && $('#fileimgupd').get(0).files.length > 0) {
        if ($scope.priceupd.length > 0) {
          var formdata = new FormData();
          angular.forEach($scope.fileimgupd, function (file) {
            formdata.append("file", file);
          });

          $http.post(
            '?action=file&img='+$scope.imgbefore, formdata,
          {  
            transformRequest: angular.client_id,  
            headers: {'Content-Type': undefined,'Process-Data': false}  
          }).success(function(data){
            var v = data.substring(0,1);
            if (v=='3') {
              $scope.update(data.substring(1,data.length));
            }else{
              if (v=='2') {
                alert('El archivo ya existe!!!');
              }else{
                alert('Formato no válido, archivo posiblemente dañado!!!');
              }
            }
          });
        }else{
          alert('Escriba el precio de la comida');
        }
      }else{
        if ($scope.priceupd.length > 0) {
          $scope.update('');
        }else{
          alert('Escriba el precio de la comida');
        }
      }
     }else{
      alert('Escriba los ingredientes de la comida');
     }
    }else{
      alert('Escriba el nombre de la comida');
    }
   }else{
    alert('Elija serie del plato');
   }
  };

  $scope.update = function(file){
    $http({
      method:"POST",
      url:"?action=food",
      data:{
        'id':$scope.idupd,
        'serieupd':$scope.serieupd,
        'nameupd':$scope.nameupd,
        'ingredupd':$scope.ingredupd,
        'fileimgupd':file,
        'priceupd':$scope.priceupd,
        'action':'actualizar'
      }
    }).success(function(data){
      $scope.serieupd = "";
      $scope.nameupd = "";
      $scope.ingredupd = "";
      $('#fileimgupd').val('');
      $scope.priceupd = "";
      $scope.addnew = true;
      $scope.actual = false;
      $scope.listar();
    });
  };

  $scope.delete = function(id,img){
    $http({
      method:"POST",
      url:"?action=food",
      data:{
        'id':id,
        'image':img,
        'action':'eliminar'
      }
    }).success(function(data){
      $scope.listar();
    });
  };

});

</script>

<script language="JavaScript">
function filterFloat(evt,input){
    // Backspace = 8, Enter = 13, ‘0′ = 48, ‘9′ = 57, ‘.’ = 46, ‘-’ = 43
    var key = window.Event ? evt.which : evt.keyCode;    
    var chark = String.fromCharCode(key);
    var tempValue = input.value+chark;
    if(key >= 48 && key <= 57){
        if(filter(tempValue)=== false){
            return false;
        }else{       
            return true;
        }
    }else{
          if(key == 8 || key == 13 || key == 0) {     
              return true;              
          }else if(key == 46){
                if(filter(tempValue)=== false){
                    return false;
                }else{       
                    return true;
                }
          }else{
              return false;
          }
    }
}

function filter(__val__){
    var preg = /^([0-9]+\.?[0-9]{0,2})$/; 
    if(preg.test(__val__) === true){
        return true;
    }else{
       return false;
    }
}
</script>