<?php
$language = "";
if (isset($_GET['lang'])){
	$language = $_GET['lang'];
	if(!empty($language)){
		require("../lang/".$language.".php");
	}
}else{
	$language = substr($_SERVER['HTTP_ACCEPT_LANGUAGE'],0,2);
	require("../lang/".$language.".php");
}
?>
<!doctype html>
<html>
<?php if(isset($_SESSION["client_id"])!=0 && $_SESSION["client_id"]!=""):?>
<head>
  <meta charset="utf-8">
  <meta http-equiv="X-UA-Compatible" content="IE=edge">
  <title>COMIDA - YAHUA v1.0</title>
  <link href="../_c/css/font-awesome.css" rel="stylesheet" type="text/css" />
  <link href="../_c/css/bootstrap.min.css" rel="stylesheet" type="text/css" />
  <link href="../_c/css/animate.css" rel="stylesheet" type="text/css" />
  <link href="../_c/css/admin.css" rel="stylesheet" type="text/css" />
  <link href="../_c/css/jquerysctipttop.css" rel="stylesheet" type="text/css">
  <link href="../_c/plugins/kalendar/kalendar.css" rel="stylesheet">
  <link rel="stylesheet" href="../_c/plugins/scroll/nanoscroller.css">
  <link href="../_c/plugins/morris/morris.css" rel="stylesheet" />
  <script src="../js/angular-1.3.15.min.js"></script>
  <script src="../js/sweetalert-dev.js"></script>
  <link rel="stylesheet" href="../js/sweetalert.css">
  <link rel="stylesheet" type="text/css" href="../_c/plugins/bootstrap-datepicker/css/datepicker.css" />
  <link rel="icon" type="image/png" href="../img/logo1.png" />
  <script src="../_c/js/jquery-2.1.0.js"></script>
</head>
<body class="dark_theme fixed_header left_nav_fixed blue_thm" ng-app="comidas" ng-controller="controller">
<!--body class="fixed_header atm-spmenu-push dark_theme blue_thm" style="background: rgb(38, 42, 51);" ng-app="comidas" ng-controller="controller"-->
<div class="wrapper">
  <!--\\\\\\\ wrapper Start \\\\\\-->
  <div class="header_bar">
    <!--\\\\\\\ header Start \\\\\\-->
    <div class="brand">
      <!--\\\\\\\ brand Start \\\\\\-->
      <div class="logo" style="display:block"><span class="theme_color">YAHUA v1.0
      </span> </div>
      <div class="small_logo" style="display:none"><img src="images/s-logo.png" width="50" height="47" alt="s-logo" /> <img src="images/r-logo.png" width="122" height="20" alt="r-logo" /></div>
    </div>
    <!--\\\\\\\ brand end \\\\\\-->
    <div class="header_top_bar">
      <!--\\\\\\\ header top bar start \\\\\\-->
      <a href="javascript:void(0);" class="menutoggle"> <i class="fa fa-bars"></i> </a>
      <div class="top_left">
        <div class="top_left_menu">
          <ul>
            <li> <a href="javascript:void(0);"><i class="fa fa-repeat"></i></a> </li>
            <li class="dropdown"> <a data-toggle="dropdown" href="javascript:void(0);"> <i class="fa fa-th-large"></i> </a>
      <ul class="drop_down_task dropdown-menu" style="margin-top:39px">
        <div class="top_left_pointer"></div>
        <li> <a href="help.html"><i class="fa fa-question-circle"></i> Ayuda</a> </li>
        <li> <a href="settings.html"><i class="fa fa-cog"></i> Configuración</a></li>
        <li> <a href="logout.php"><i class="fa fa-power-off"></i> Salir</a> </li>
      </ul>
      </li>
          </ul>
        </div>
      </div>

      <div class="top_right_bar">
        <?php $clt = ClientData::getById($_SESSION["client_id"]); ?>
        <div class="user_admin dropdown"> <a href="javascript:void(0);" data-toggle="dropdown"><img src="../img/clts/owner.jpg" /><span class="user_adminname"><?php echo $clt->name; ?></span> <b class="caret"></b> </a>
          <ul class="dropdown-menu">
            <div class="top_pointer"></div>
            <!--li> <a href="profile.html"><i class="fa fa-user"></i> Perfil</a> </li-->
            <li> <a href="help.html"><i class="fa fa-question-circle"></i> Ayuda</a> </li>
            <li> <a href="settings.html"><i class="fa fa-cog"></i> Configuración</a></li>
            <li> <a href="logout.php"><i class="fa fa-power-off"></i> Salir</a> </li>
          </ul>
        </div>
      </div>
    </div>
    
    <!--\\\\\\\ header top bar end \\\\\\-->
  </div>
  <!--\\\\\\\ header end \\\\\\-->
  <div class="inner">
    <!--\\\\\\\ inner start \\\\\\-->
    <div class="left_nav">

      <!--\\\\\\\left_nav start \\\\\\-->
      <div class="search_bar"> <i class="fa fa-search"></i>
        <input name="" type="text" class="search" placeholder="Buscar..." />
      </div>
      <div class="left_nav_slidebar">
        <ul>
          <li class="left_nav_active theme_border"><a href="javascript:void(0);"><i class="fa fa-home"></i> Tablero <span class="left_nav_pointer"></span> <span class="plus"><i class="fa fa-plus"></i></span> </a>
            <ul class="opened" style="display:block">
              <li> <a href="./"> <span>&nbsp;</span> <i class="fa fa-circle theme_color"></i> <b id="home">Inicio</b> </a> </li>
              <?php if (strlen($clt->description) > 0 && strlen($clt->image) > 0) { ?>
                <li> <a href="?view=food"><span>&nbsp;</span> <i class="fa fa-circle theme_color"></i> <b id="optone">Comidas</b> </a>
              </li>
              <?php }else{ ?>
                <li> <a href="?view=act"><span>&nbsp;</span> <i class="fa fa-circle"></i> <b >Comidas</b> </a>
              </li>
              <?php } ?>
            </ul>

          </li>
        </ul>
      </div>
    </div>
    <!--\\\\\\\left_nav end \\\\\\-->
    <div class="contentpanel" style="background: rgb(229, 232, 232);">
      <!--\\\\\\\ contentpanel start\\\\\\-->
      <?php  View::load("index"); ?>
      <!--\\\\\\\ container  end \\\\\\-->
    </div>
    <!--\\\\\\\ content panel end \\\\\\-->
  </div>
  <!--\\\\\\\ inner end\\\\\\-->
</div>
<!--\\\\\\\ wrapper end\\\\\\-->

<script src="../_c/js/bootstrap.min.js"></script>
<script src="../_c/js/jquery.slimscroll.min.js"></script>
<script src="../_c/js/jquery.sparkline.js"></script>
<script src="../_c/js/sparkline-chart.js"></script>
<script src="../_c/js/graph.js"></script>
<script src="../_c/js/edit-graph.js"></script>
<script src="../_c/plugins/kalendar/kalendar.js" type="text/javascript"></script>
<script src="../_c/plugins/kalendar/edit-kalendar.js" type="text/javascript"></script>

<script src="../_c/plugins/sparkline/jquery.sparkline.js" type="text/javascript"></script>
<script src="../_c/plugins/sparkline/jquery.customSelect.min.js" ></script> 

<script src="../_c/plugins/knob/jquery.knob.min.js"></script> 

<script src="../_c/js/jPushMenu.js"></script> 
<script src="../_c/js/side-chats.js"></script>
<script src="../_c/js/jquery.slimscroll.min.js"></script>
<script src="../_c/plugins/scroll/jquery.nanoscroller.js"></script>
<script type="text/javascript"  src="../_c/plugins/input-mask/jquery.inputmask.min.js"></script>
<script type="text/javascript" src="../_c/plugins/bootstrap-datepicker/js/bootstrap-datepicker.js"></script> 
</body >
<?php else:
	core::redir("../join");
 endif; ?>
</html>