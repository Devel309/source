<style>
.datepicker{z-index:1151 !important;}
</style>
<div class="pull-left breadcrumb_admin clear_both">
  <div class="pull-left page_title theme_color">
    <h1>Tablero</h1>
    <h2 class="">Inicio</h2>
  </div>
  <div class="pull-right">
    <ol class="breadcrumb">
      <li><a href="#">Tablero</a></li>
      <li class="active">Inicio</li>
    </ol>
  </div>
</div>
<div class="container clear_both padding_fix">
  <!--div class="row" ng-init="services()"-->
  <div class="row">          
    <?php $clt = ClientData::getById($_SESSION["client_id"]); ?>
    <div class="col-sm-3 col-sm-6">
      <?php if (strlen($clt->description) > 0 && strlen($clt->image) > 0) { ?>
      <a href="?view=food">
      <?php }else{ ?>
      <a href="?view=act">
      <?php } ?>
        <div class="information gray_info">
          <div class="information_inner">
            <div class="info blue_symbols"><i class="fa fa-cutlery icon"></i></div>
            <span><b>RESTAURANTES</b><br>PREMIUM </span>
              <div class="infoprogress_gray">
                <div class="grayprogress"></div>
              </div>
              <!--h3 class="bolded">S/. 50 mensual </h3-->
          </div>
        </div>
      </a>
    </div>
    <div class="col-sm-3 col-sm-6">
      <a href="../" target="_new">
        <div class="information green_info">
          <div class="information_inner">
            <div class="info green_symbols"><i class="fa fa-laptop icon"></i></div>
            <span><b>CONSULTORÍA</b><br>PREMIUM </span>
            <div class="infoprogress_green">
              <div class="greenprogress"></div>
            </div>
            <h3 class="bolded">S/. 100 mensual </h3>
          </div>
        </div>
      </a>
    </div>
    <div class="col-sm-3 col-sm-6">
      <a href="../store" target="_new">
        <div class="information blue_info">
          <div class="information_inner">
            <div class="info blue_symbols"><i class="fa fa-shopping-cart icon"></i></div>
            <span><b>VENTAS</b><br>PREMIUM </span>
            <div class="infoprogress_blue">
              <div class="blueprogress"></div>
            </div>
            <h3 class="bolded">S/. 80 mensual </h3>
          </div>
        </div>
      </a>
    </div>
    <div class="col-sm-3 col-sm-6">
      <div class="information red_info">
        <div class="information_inner">
          <div class="info red_symbols"><i class="fa fa-bar-chart-o icon"></i></div>
          <span><b>LOGÍSTCA</b><br>PREMIUM </span>
          <div class="infoprogress_red">
            <div class="redprogress"></div>
          </div>
          <h3 class="bolded">S/. 120 mensual </h3>
        </div>
      </div>
    </div>
  </div>
</div>

<script type="application/javascript">
  $(document).ready(function() {
    $('#home').addClass('theme_color');
  });
</script>