<!DOCTYPE html>
<html>
<head>
	<title>NOT FOUND</title>
</head>
<style type="text/css">
	body{
		background: url('background.jpg');
		background-size: cover;
	}
	a{
		font-size: 35px;
		color: #fff;
		font-weight: bold;
		text-decoration: none;
		background: #189CED;
		font-family: "Comic Sans MS", cursive, sans-serif;
		width: 600px;
	}
	div{
		text-align: center;
		margin: 1rem;
		padding: 1rem;
  		border: none;
  		display: flex;
  		justify-content: center;
  		align-items: center;
	}
</style>
<body>
<div>
	<a href="#">Visite Nuestra Nueva P&aacute;gina Web</a>
</div>
</body>
</html>