<?php
if (isset($_GET['lang'])){
  $language = $_GET['lang'];
  if(!empty($language)){
    require("../lang/".$language.".php");
  }
}else{
  $language = substr($_SERVER['HTTP_ACCEPT_LANGUAGE'],0,2);
  require("../lang/".$language.".php");
}

/*require 'tweet/autoload.php';
use Abraham\TwitterOAuth\TwitterOAuth;

define('CONSUMER_KEY', 'wOkRWRMRJPV0ZShztliP7pVFZ'); // add your app consumer key between single quotes
define('CONSUMER_SECRET', 'M5UxpATTHOBwLTp7P6pYj69ToXN1bHouWl2FAbi1mE1PAELz8K'); // add your app consumer secret key between single quotes
define('OAUTH_CALLBACK', 'http://127.0.0.1:8084/home/index.php?action=tw'); // your app callback URL
if (!isset($_SESSION['access_token'])) {
  $connection = new TwitterOAuth(CONSUMER_KEY, CONSUMER_SECRET);
  $request_token = $connection->oauth('oauth/request_token', array('oauth_callback' => OAUTH_CALLBACK));
  $_SESSION['oauth_token'] = $request_token['oauth_token'];
  $_SESSION['oauth_token_secret'] = $request_token['oauth_token_secret'];
  $url = $connection->url('oauth/authorize', array('oauth_token' => $request_token['oauth_token']));
  
  /////write next code for login with tweet, in href: <?php echo $url; ?>
}*/
?>
<!DOCTYPE html>
<html>
<head>
<title><?php echo $lang['title']; ?></title>
<meta name="viewport" content="width=device-width, initial-scale=1">
<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
<meta name="keywords" content="" />
<script type="application/x-javascript"> addEventListener("load", function() { setTimeout(hideURLbar, 0); }, false); function hideURLbar(){ window.scrollTo(0,1); } </script>
<link rel="icon" type="image/png" href="../img/logo1.png" />
 <!-- Bootstrap Core CSS -->
<link href="css/bootstrap.min.css" rel='stylesheet' type='text/css' />
<!-- Custom CSS -->
<link href="css/mrys_reg.css" rel='stylesheet' type='text/css' />
<!-- Graph CSS -->
<link href="css/font-awesome.css" rel="stylesheet"> 
<!-- jQuery -->
<link href='//fonts.googleapis.com/css?family=Roboto:700,500,300,100italic,100,400' rel='stylesheet' type='text/css'>
<!-- lined-icons -->
<link rel="stylesheet" href="css/icon-font.min.css" type='text/css' />
<!-- //lined-icons -->
<script src="js/jquery-1.10.2.min.js"></script>
<!--clock init-->
</head>
<body>
<!--/login-->
<div class="error_page">
<!--/login-top-->
  <div class="error-top">

<?php View::load('_i'); ?>

  </div>
</div>
<!--//login-top-->
<!--//login-->
<div class="footer"></div>
<!--js -->
<script src="js/jquery.nicescroll.js"></script>
<script src="js/scripts.js"></script>
<!-- Bootstrap Core JavaScript -->
<script src="js/bootstrap.min.js"></script>
</body>
</html>
