<?php
if (isset($_GET['lang'])){
	$language = $_GET['lang'];
	if(!empty($language)){
		require("../lang/".$language.".php");
	}
}else{
	$language = substr($_SERVER['HTTP_ACCEPT_LANGUAGE'],0,2);
	require("../lang/".$language.".php");
}
?>
<h2 class="inner-tittle page"><?php echo $lang['augment']; ?></h2>
<div class="login">
	<h3 class="inner-tittle t-inner">Register</h3>
	<form class="form-horizontal" method="post" action="../_c/?action=rgtrs" role="form">
		<input style="background: url('../img/reg.png') no-repeat 14px 17px;" type="text" class="text" placeholder="<?php echo $lang['lname']; ?>" name="lname" required >
        <input style="background: url('../img/reg.png') no-repeat 14px 17px;" type="text" class="text" placeholder="<?php echo $lang['fname']; ?>" name="fname" required >
        <select style="background: url('../img/gen.png') no-repeat 14px 17px;" name="gender" class="select"><option><?php echo $lang['gender']; ?></option><option value="1"><?php echo $lang['male']; ?></option><option value="0"><?php echo $lang['female']; ?></option></select>
        <input style="background: url('../img/business.png') no-repeat 14px 17px;" type="text" class="text" placeholder="<?php echo $lang['company']; ?>" name="cname" required >
        <?php $clt = ConsultData::getEmp(); ?>
        <select style="background: url('../img/emp.png') no-repeat 14px 17px;" name="eid" class="select"><option><?php echo $lang['snumber'].' '.$lang['employe']; ?></option><?php if(count($clt)>0): foreach($clt as $e){ ?><option value="<?php echo $e->id; ?>"><?php echo $e->descrip.' '.$lang['employe']; ?></option><?php } endif; ?></select>
        <input style="background: url('../img/phone.png') no-repeat 14px 17px;" type="text" class="text" placeholder="<?php echo $lang['phone']; ?>" name="phone" required >
        <input type="email" class="text" placeholder="<?php echo $lang['email']; ?>" name="email" required >
        <input type="password" placeholder="<?php echo $lang['psw']; ?>" name="psw" required>
		<input type="password" placeholder="<?php echo $lang['rpsw']; ?>" name="rpsw" required >
		<div class="sign-up">
			<input type="reset" value="<?php echo $lang['reset']; ?>">
			<input type="submit" value="<?php echo $lang['register']; ?>">
		</div>
		<div class="clearfix"></div>
		<div class="new">
			<p><label class="checkbox11"><input type="checkbox" name="checkbox"><i> </i><?php echo $lang['agree']; ?></label></p>
			<p class="sign"><?php echo $lang['already']; ?> <a href="./?view=_i"><?php echo $lang['login']; ?></a></p>
			<div class="clearfix"></div>
		</div>
		<a class="read fourth" href="../"><?php echo $lang['return']; ?></a>
	</form>
</div>