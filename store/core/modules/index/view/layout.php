<?php
$language = "";
if (isset($_GET['lang'])){
	$language = $_GET['lang'];
	if(!empty($language)){
		require("lang/".$language.".php");
	}
}else{
	$language = substr($_SERVER['HTTP_ACCEPT_LANGUAGE'],0,2);
	require("lang/".$language.".php");
}
?>
<!DOCTYPE html>
<html lang="en">
<head>
  <meta charset="utf-8">
  <meta http-equiv="X-UA-Compatible" content="IE=edge">
  <meta name="viewport" content="width=device-width, initial-scale=1">

  <title><?php echo $lang['title']; ?></title>
  <link href="https://fonts.googleapis.com/css?family=Hind:400,700" rel="stylesheet">
  <link type="text/css" rel="stylesheet" href="css/bootstrap.min.css" />
  <link type="text/css" rel="stylesheet" href="css/slick.css" />
  <link type="text/css" rel="stylesheet" href="css/slick-theme.css" />
  <link type="text/css" rel="stylesheet" href="css/nouislider.min.css" />
  <link rel="stylesheet" href="css/font-awesome.min.css">
  <link type="text/css" rel="stylesheet" href="css/style.css" />
  <script src="../js/angular-1.3.15.min.js"></script>
  <link rel="icon" type="image/png" href="img/logo1.png" />

</head>
<body data-ng-app="store" data-ng-controller="controller">
<!-- HEADER -->
  <header>
    <!-- header -->
    <div id="header">
      <div class="container">
        <div class="pull-left">
          <!-- Logo -->
          <div class="header-logo">
            <a class="logo" href="#">
              <img src="./img/logtop.png" alt="">
            </a>
          </div>
          <!-- /Logo -->

          <!-- Search -->
          <div class="header-search">
            <form>
              <input class="input search-input" type="text" placeholder="<?php echo $lang['search']; ?>">
              <select class="input search-categories">
                <option value="0"><?php echo $lang['acat']; ?></option>
                <option value="1"><?php echo $lang['subuno']; ?></option>
                <option value="1"><?php echo $lang['subdos']; ?></option>
                <option value="1"><?php echo $lang['subtres']; ?></option>
                <option value="1"><?php echo $lang['subqatro']; ?></option>
                <option value="1"><?php echo $lang['subcin']; ?></option>
                <option value="1"><?php echo $lang['subseis']; ?></option>
                <option value="1"><?php echo $lang['subsite']; ?></option>
                <option value="1"><?php echo $lang['subocto']; ?></option>
              </select>
              <button class="search-btn"><i class="fa fa-search"></i></button>
            </form>
          </div>
          <!-- /Search -->
        </div>
        <div class="pull-right">
          <ul class="header-btns">
            <!-- Account -->
            <li class="header-account dropdown default-dropdown">
              <div class="dropdown-toggle" role="button" data-toggle="dropdown" aria-expanded="true">
                <div class="header-btns-icon">
                  <i class="fa fa-user-o"></i>
                </div>
                <strong class="text-uppercase"><?php if(isset($_SESSION["client_id"])!=0 && $_SESSION["client_id"]!=""): echo $lang['account']; else: echo $lang['login']; endif; ?> <i class="fa fa-caret-down"></i></strong>
              </div>
              <ul class="custom-menu">
                <li><a href="#"><i class="fa fa-user-o"></i> <?php echo $lang['account']; ?></a></li>
                <li><a href="#"><i class="fa fa-heart-o"></i> <?php echo $lang['wishl']; ?></a></li>
                <li><a href="#"><i class="fa fa-exchange"></i> <?php echo $lang['dpric']; ?></a></li>
                <li><a href="#"><i class="fa fa-check"></i> <?php echo $lang['pay']; ?></a></li>
                <li>
                  <?php
                    if (isset($_SESSION["client_id"])!=0 && $_SESSION["client_id"]!="") {
                  ?>
                    <a href="logout.php" role="button"><i class="fa fa-sign-out"></i> <?php echo $lang['logout']; ?></a>
                  <?php } else { ?>
                    <a href="#loginModal" role="button" data-toggle="modal"><i class="fa fa-unlock-alt"></i> <?php echo $lang['login']; ?></a>
                  <?php } ?>
                </li>
                <li></li>
              </ul>
            </li>
            <!-- /Account -->

            <!-- Cart -->
            <li class="header-cart dropdown default-dropdown">
              <a class="dropdown-toggle" data-toggle="dropdown" aria-expanded="true">
                <div class="header-btns-icon">
                  <i class="fa fa-shopping-cart"></i>
                  <span class="qty">3</span>
                </div>
                <strong class="text-uppercase"><?php echo $lang['cart']; ?>:</strong>
                <br>
                <span>S/ 35.20</span>
              </a>
              <div class="custom-menu">
                <div id="shopping-cart">
                  <div class="shopping-cart-list">
                    <div class="product product-widget">
                      <div class="product-thumb">
                        <img src="./img/thumb-product01.jpg" alt="">
                      </div>
                      <div class="product-body">
                        <h3 class="product-price">S/ 32.50 <span class="qty">x3</span></h3>
                        <h2 class="product-name"><a href="#">Product Name Goes Here</a></h2>
                      </div>
                      <button class="cancel-btn"><i class="fa fa-trash"></i></button>
                    </div>
                    <div class="product product-widget">
                      <div class="product-thumb">
                        <img src="./img/thumb-product01.jpg" alt="">
                      </div>
                      <div class="product-body">
                        <h3 class="product-price">S/ 32.50 <span class="qty">x3</span></h3>
                        <h2 class="product-name"><a href="#">Product Name Goes Here</a></h2>
                      </div>
                      <button class="cancel-btn"><i class="fa fa-trash"></i></button>
                    </div>
                    <div class="product product-widget">
                      <div class="product-thumb">
                        <img src="./img/thumb-product01.jpg" alt="">
                      </div>
                      <div class="product-body">
                        <h3 class="product-price">S/ 32.50 <span class="qty">x3</span></h3>
                        <h2 class="product-name"><a href="#">Product Name Goes Here</a></h2>
                      </div>
                      <button class="cancel-btn"><i class="fa fa-trash"></i></button>
                    </div>
                  </div>
                  <div class="shopping-cart-btns">
                    <button class="main-btn"><?php echo $lang['vcart']; ?></button>
                    <button class="primary-btn"><?php echo $lang['pay']; ?> <i class="fa fa-arrow-circle-right"></i></button>
                  </div>
                </div>
              </div>
            </li>
            <!-- /Cart -->

            <!-- Mobile nav toggle-->
            <li class="nav-toggle">
              <button class="nav-toggle-btn main-btn icon-btn"><i class="fa fa-bars"></i></button>
            </li>
            <!-- / Mobile nav toggle -->
          </ul>
        </div>
      </div>
      <!-- header -->
    </div>
    <!-- container -->
  </header>
  <!-- /HEADER -->

  <?php
    View::load("index");
  ?>

  <!-- Modal / Ventana / Overlay en HTML -->
<div id="loginModal" class="modal fade">
    <div class="modal-dialog modal-sm">
        <div class="modal-content">
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal" aria-hidden="true">&times;</button>
                <h4 class="modal-title"><?php echo $lang['login']; ?></h4>
            </div>
            <div class="modal-body">
              <form>
                <div class="form-group">
                  <label for="inputName"><?php echo $lang['user']; ?></label>
                  <input type="text" class="form-control" ng-model="username" autocomplete="off">
                </div>
                <div class="form-group">
                  <label for="inputPassword"><?php echo $lang['pasw']; ?></label>
                  <input type="password" class="form-control" ng-model="password">
                </div>
              </form>
              <a href="#"><?php echo $lang['forgot']; ?></a><br><a href="?view=join"><?php echo $lang['create']; ?></a>
            </div>
            <div class="modal-footer">
              <button type="button" class="btn btn-danger" ng-click="login()"><?php echo $lang['login']; ?></button>
              <button type="button" class="btn btn-default" data-dismiss="modal"><?php echo $lang['close']; ?></button>
            </div>
        </div>
    </div>
</div>

<!-- FOOTER -->
  <footer id="footer" class="section section-grey">
    <!-- container -->
    <div class="container">
      <!-- row -->
      <div class="row">
        <!-- footer widget -->
        <div class="col-md-3 col-sm-6 col-xs-6">
          <div class="footer">
            <!-- footer logo -->
            <div class="footer-logo">
              <a class="logo" href="#">
                <img src="./img/logtop.png" alt="">
              </a>
            </div>
            <!-- /footer logo -->

            <p style="text-align: justify;">Somos una empresa que avanza conforme que la tecnología avanza, más tecnología para tu comodidad.</p>

            <!-- footer social -->
            <ul class="footer-social">
              <li><a href="https://web.facebook.com/YAHUA-556011321558248/" target="_new"><i class="fa fa-facebook"></i></a></li>
              <li><a href="#"><i class="fa fa-twitter"></i></a></li>
              <li><a href="#"><i class="fa fa-instagram"></i></a></li>
              <li><a href="#"><i class="fa fa-google-plus"></i></a></li>
              <li><a href="#"><i class="fa fa-linkedin"></i></a></li>
            </ul>
            <!-- /footer social -->
          </div>
        </div>
        <!-- /footer widget -->

        <!-- footer widget -->
        <div class="col-md-3 col-sm-6 col-xs-6">
          <div class="footer">
            <h3 class="footer-header"><?php echo $lang['account']; ?></h3>
            <ul class="list-links">
              <li><a href="#loginModal" role="button" data-toggle="modal"><?php echo $lang['account']; ?></a></li>
              <li><a href="#"><?php echo $lang['wishl']; ?></a></li>
              <li><a href="#"><?php echo $lang['dpric']; ?></a></li>
              <li><a href="#"><?php echo $lang['pay']; ?></a></li>
              <li>
                <?php
                  if (isset($_SESSION["client_id"])!=0 && $_SESSION["client_id"]!="") {
                ?>
                  <a href="logout.php" role="button"><?php echo $lang['logout']; ?></a>
                <?php } else { ?>
                  <a href="#loginModal" role="button" data-toggle="modal"><?php echo $lang['login']; ?></a>
                <?php } ?>
              </li>
            </ul>
          </div>
        </div>
        <!-- /footer widget -->

        <div class="clearfix visible-sm visible-xs"></div>

        <!-- footer widget -->
        <div class="col-md-3 col-sm-6 col-xs-6">
          <div class="footer">
            <h3 class="footer-header"><?php echo $lang['custserv']; ?></h3>
            <ul class="list-links">
              <li><a href="../"><?php echo $lang['about']; ?></a></li>
              <li><a href="#"><?php echo $lang['ship']; ?></a></li>
              <li><a href="#"><?php echo $lang['guide']; ?></a></li>
              <li><a href="#"><?php echo $lang['faq']; ?></a></li>
            </ul>
          </div>
        </div>
        <!-- /footer widget -->

        <!-- footer subscribe -->
        <div class="col-md-3 col-sm-6 col-xs-6">
          <div class="footer">
            <h3 class="footer-header"><?php echo $lang['email']; ?></h3>
            <p>Permanece al día con los preciazos y últimos productos de moda.</p>
            <form>
              <div class="form-group">
                <input class="input" placeholder="<?php echo $lang['msjemail']; ?>">
              </div>
              <button class="primary-btn"><?php echo $lang['recemail']; ?></button>
            </form>
          </div>
        </div>
        <!-- /footer subscribe -->
      </div>
      <!-- /row -->
      <hr>
      <!-- row -->
      <div class="row">
        <div class="col-md-8 col-md-offset-2 text-center">
          <!-- footer copyright -->
          <div class="footer-copyright">
             &copy; Copyright 2019 - <?=date('Y')?> <a href="../" target="_blank">YAHUA</a> - Todos los derechos reservados
          </div>
          <!-- /footer copyright -->
        </div>
      </div>
      <!-- /row -->
    </div>
    <!-- /container -->
  </footer>
  <!-- /FOOTER -->

  <!-- jQuery Plugins -->
  <script src="js/jquery.min.js"></script>
  <script src="js/bootstrap.min.js"></script>
  <script src="js/slick.min.js"></script>
  <script src="js/nouislider.min.js"></script>
  <script src="js/jquery.zoom.min.js"></script>
  <script src="js/main.js"></script>

  <script>
  var app = angular.module('store', []);

  app.controller('controller', function($scope, $http) {

  $scope.msjname = "";

  $scope.openuno = function(id,name,price){
    $scope.idserv = id;
    $scope.nameserv = name;
    $scope.priceserv = price;
    $("#modaluno").modal("show");
    $scope.inicio();
    //$('.mask').inputmask();
    //$('.datepicker').datepicker();
  };

  $scope.services = function(){
    $http({
      method:"POST",
      url:"?action=service"
    }).success(function(data){
      $scope.service = data;
    });
  };

  $scope.login = function(){
    $http({
      method:"POST",
      url:"?action=login",
      data:{
        action:"start",
        user:$scope.username,
        pasw:$scope.password
      }
    }).success(function(data){
      /*$scope.btnpay = true;
      $scope.btnsave = false;*/
      $scope.username = '';
      $scope.password = '';
      $("#loginModal").modal("hide");
      location.reload();
    });
  };

  $scope.match = function () {
    $scope.ismatch = angular.equals($scope.pasw,$scope.rpasw);
  }

  $scope.phonev = function () {
    $scope.isphone = $scope.phone.length;
  }

  $scope.save = function(){
    if($scope.name.length>0 && $scope.lastname.length>0 && $scope.email.length>0 && $scope.phone.length>0 && $scope.address.length>0 && $scope.pasw.length>0 && $scope.rpasw.length>0){
      $http({
        method:"POST",
        url:"?action=rgtrs",
        data:{
          action:"add",
          name:$scope.name,
          lastname:$scope.lastname,
          email:$scope.email,
          phone:$scope.phone,
          address:$scope.address,
          password:$scope.pasw,
          rpasw:$scope.rpasw
        }
      }).success(function(data){
        $scope.name = '';
        $scope.lastname = '';
        $scope.email = '';
        $scope.phone = '';
        $scope.address = '';
        $scope.pasw = '';
        $scope.rpasw = '';
        window.location.href = "./";
     });
    }
  };

  });
  
</script>
</body >
</html>