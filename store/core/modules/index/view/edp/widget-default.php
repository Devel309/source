<?php
if (isset($_GET['lang'])){
	$language = $_GET['lang'];
	if(!empty($language)){
		require("../lang/".$language.".php");
	}
}else{
	$language = substr($_SERVER['HTTP_ACCEPT_LANGUAGE'],0,2);
	require("../lang/".$language.".php");
}
?>
    <!-- Content Header (Page header) -->
    <section class="content-header">
      <h1>
        <?php echo $lang['nedit']; ?>
        <small><?php echo $lang['form']; ?></small>
      </h1>
      <ol class="breadcrumb">
        <li><a href="#"><i class="fa fa-dashboard"></i> <?php echo $lang['dash']; ?></a></li>
        <li><a href="#"><?php echo $lang['form']; ?></a></li>
      </ol>
    </section>
    <?php $conv = ClientData::getById($_SESSION['client_id']); ?>
    <!-- Main content -->
    <section class="content">
      <div class="row">
        <!-- left column -->
        <div class="col-md-6">
          <!-- general form elements -->
          <div class="box box-primary">
            <div class="box-header with-border">
              <h3 class="box-title"><?php echo $lang['edemail']; ?></h3>
            </div>
            <!-- /.box-header -->
            <!-- form start -->
            <form method="post" action="index.php?action=edp" role="form">
              <div class="box-body">
                <div class="form-group">
                  <label for="exampleInputEmail1"><?php echo $lang['email']; ?></label>
                  <input type="email" name="email" class="form-control" id="email" value="<?php echo $conv->email; ?>" required>
                </div>
                <div class="checkbox">
                  <label>
                    <input type="checkbox" required> <?php echo $lang['confirm']; ?>
                  </label>
                </div>
              </div>
              <!-- /.box-body -->

              <div class="box-footer">
                <button class="btn btn-danger" name="save_email"><?php echo $lang['save']; ?></button>
              </div>
            </form>
          </div>
          <!-- /.box -->

          <!-- Form Element sizes -->
          <div class="box box-success">
            <div class="box-header with-border">
              <h3 class="box-title"><?php echo $lang['edpi']; ?></h3>
            </div>
            <!-- form start -->
            <form method="post" action="index.php?action=edp" role="form">
              <div class="box-body">
                <div class="form-group">
                  <label for="exampleInputEmail1"><?php echo $lang['lname']; ?></label>
                  <input type="text" name="lname" class="form-control" id="exampleInputEmail1" value="<?php echo $conv->lastname; ?>">
                </div>
                <div class="form-group">
                  <label for="exampleInputEmail1"><?php echo $lang['fname']; ?></label>
                  <input type="text" name="fname" class="form-control" id="exampleInputEmail1" value="<?php echo $conv->name; ?>">
                </div>
                <div class="form-group">
                  <label for="exampleInputEmail1"><?php echo $lang['gender']; ?></label>
                  <select class="form-control" name="gender"><option value="1" <?php if($conv->gender==='1'){ echo 'selected'; } ?>><?php echo $lang['male']; ?></option><option value="0" <?php if($conv->gender==='0'){ echo 'selected'; } ?>><?php echo $lang['female']; ?></option></select>
                </div>
                <div class="form-group">
                  <label for="exampleInputEmail1"><?php echo $lang['phone']; ?></label>
                  <input type="text" name="fname" class="form-control" id="exampleInputEmail1" value="<?php echo $conv->phone; ?>">
                </div>
                <div class="checkbox">
                  <label>
                    <input type="checkbox" required> <?php echo $lang['confirm']; ?>
                  </label>
                </div>
              </div>
              <!-- /.box-body -->

              <div class="box-footer">
                <button class="btn btn-danger" name="save_info"><?php echo $lang['save']; ?></button>
              </div>
            </form>
          </div>
          <!-- /.box -->

          
        </div>
        <!--/.col (left) -->
        <!-- right column -->
        <div class="col-md-6">
          <!-- Horizontal Form -->
          <div class="box box-info">
            <div class="box-header with-border">
              <h3 class="box-title"><?php echo $lang['edpsw']; ?></h3>
            </div>
            <!-- /.box-header -->
            <!-- form start -->
            <form method="post" action="index.php?action=edp" role="form">
              <div class="box-body">
                <div class="form-group">
                  <label for="inputPassword3"><?php echo $lang['psw']; ?></label>
                  <input type="password" name="psw" class="form-control" id="inputPassword3" placeholder="Write new password" required>
                </div>
                <div class="form-group">
                  <label for="inputPassword3"><?php echo $lang['rpsw']; ?></label>
                  <input type="password" name="rpsw" class="form-control" id="inputPassword3" placeholder="Write new password" required>
                </div>
                <div class="checkbox">
                  <label>
                    <input type="checkbox" required> <?php echo $lang['confirm']; ?>
                  </label>
                </div>
              </div>
              <!-- /.box-body -->
              <div class="box-footer">
                <button type="reset" class="btn btn-default"><?php echo $lang['cancel']; ?></button>
                <button class="btn btn-danger pull-right" name="save_psw"><?php echo $lang['save']; ?></button>
              </div>
              <!-- /.box-footer -->
            </form>
          </div>
          <!-- /.box -->
          <!-- Horizontal Form -->
          <div class="box box-info">
            <div class="box-header with-border">
              <h3 class="box-title"><?php echo $lang['edimg']; ?></h3>
            </div>
            <!-- /.box-header -->
            <!-- form start -->
            <form enctype="multipart/form-data" method="post" action="index.php?action=edp" role="form">
              <div class="box-body">
                <div class="form-group">
                  <label for="Photo1"><?php echo $lang['photo']; ?></label>
                    <input type="file" name="image" required>
                </div>
                <div class="checkbox">
                  <label>
                    <input type="checkbox" required> <?php echo $lang['confirm']; ?>
                  </label>
                </div>
              </div>
              <!-- /.box-body -->
              <div class="box-footer">
                <button type="reset" class="btn btn-default"><?php echo $lang['cancel']; ?></button>
                <button class="btn btn-danger pull-right" name="save_photo"><?php echo $lang['save']; ?></button>
              </div>
              <!-- /.box-footer -->
            </form>
          </div>
          <!-- /.box -->
          <!-- general form elements disabled -->
          <div class="box box-warning">
            <div class="box-header with-border">
              <h3 class="box-title"><?php echo $lang['edb']; ?></h3>
            </div>
            <!-- /.box-header -->
            <div class="box-body">
              <form method="post" action="index.php?action=edp" role="form">
                <!-- text input -->
                <div class="form-group">
                  <label><?php echo $lang['company']; ?></label>
                  <input type="text" name="cname" class="form-control" value="<?php echo $conv->nameb; ?>" required>
                </div>
                <div class="form-group">
                  <label><?php echo $lang['snumber'].' '.$lang['employe']; ?></label>
                  <select class="form-control" name="eid">
                  <?php $data = ConsultData::getEmp();
				  if(count($data)>0):
				  foreach($data as $d){ ?>
                  <option value="<?php echo (string)$d->id; ?>" <?php if($d->id===$conv->employe_id){ echo 'selected'; } ?>><?php echo $d->descrip.' '.$lang['employe']; ?></option>
                  <?php }
				  endif; ?>
                  </select>
                </div>
                <div class="checkbox">
                  <label>
                    <input type="checkbox" required> <?php echo $lang['confirm']; ?>
                  </label>
                </div>
                <!-- /.box-body -->
              	<div class="box-footer">
	                <button class="btn btn-danger pull-right" name="save_bb"><?php echo $lang['save']; ?></button>
    	        </div>
              </form>
            </div>
            <!-- /.box-body -->
          </div>
          <!-- /.box -->
        </div>
        <!--/.col (right) -->
      </div>
      <!-- /.row -->
    </section>
    <!-- /.content -->
<script>
document.querySelector('button').onclick = function(){
	swal({
		title: "Are you sure?",
		text: "You will not be able to recover this imaginary file!",
		type: "warning",
		showCancelButton: true,
		confirmButtonColor: '#DD6B55',
		confirmButtonText: 'Yes, delete it!',
		cancelButtonText: "No, cancel plx!",
		closeOnConfirm: false,
		closeOnCancel: false
	},
	function(isConfirm){
    if (isConfirm){
		location.href ="index.php?action=edp";
    } else {
      swal("Cancelled", "Your imaginary file is safe :)", "error");
    }
	});
};
</script>