<?php
$language = "";
if (isset($_GET['lang'])){
  $language = $_GET['lang'];
  if(!empty($language)){
    require("lang/".$language.".php");
  }
}else{
  $language = substr($_SERVER['HTTP_ACCEPT_LANGUAGE'],0,2);
  require("lang/".$language.".php");
}
?>
<style>
.datepicker{z-index:1151 !important;}
</style>

  <!-- NAVIGATION -->
  <div id="navigation">
    <!-- container -->
    <div class="container">
      <div id="responsive-nav">
        <!-- category nav -->
        <div class="category-nav">
          <span class="category-header"><?php echo $lang['categ']; ?> <i class="fa fa-list"></i></span>
          <ul class="category-list">
            <li class="dropdown side-dropdown">
              <a class="dropdown-toggle" data-toggle="dropdown" aria-expanded="true"><?php echo $lang['subuno']; ?> <i class="fa fa-angle-right"></i></a>
              <div class="custom-menu">
                <div class="row">
                  <div class="col-md-4">
                    <ul class="list-links">
                      <li>
                        <h3 class="list-links-title"><?php echo $lang['categ']; ?></h3></li>
                      <li><a href="#"><?php echo $lang['catone']; ?></a></li>
                      <li><a href="#"><?php echo $lang['cattwo']; ?></a></li>
                      <li><a href="#"><?php echo $lang['catthree']; ?></a></li>
                      <li><a href="#"><?php echo $lang['catfour']; ?></a></li>
                      <li><a href="#"><?php echo $lang['catfive']; ?></a></li>
                      <li><a href="#"><?php echo $lang['catsix']; ?></a></li>
                    </ul>
                    <hr class="hidden-md hidden-lg">
                  </div>
                  <div class="col-md-4">
                    <ul class="list-links">
                      <li>
                        <h3 class="list-links-title"><?php echo $lang['categ']; ?></h3></li>
                      <li><a href="#"><?php echo $lang['catseven']; ?></a></li>
                      <li><a href="#"><?php echo $lang['cateigth']; ?></a></li>
                      <li><a href="#"><?php echo $lang['catnine']; ?></a></li>
                      <li><a href="#"><?php echo $lang['catteen']; ?></a></li>
                      <li><a href="#"><?php echo $lang['cateleven']; ?></a></li>
                      <li><a href="#"><?php echo $lang['cattwelve']; ?></a></li>
                    </ul>
                    <hr class="hidden-md hidden-lg">
                  </div>
                  <div class="col-md-4">
                    <ul class="list-links">
                      <li>
                        <h3 class="list-links-title">Categories</h3></li>
                      <li><a href="#"><?php echo $lang['catthirteen']; ?></a></li>
                      <li><a href="#"><?php echo $lang['catfourteen']; ?></a></li>
                      <li><a href="#"><?php echo $lang['catfiveteen']; ?></a></li>
                      <li><a href="#"><?php echo $lang['catsixteen']; ?></a></li>
                      <li><a href="#">ABRIGOS</a></li>
                    </ul>
                  </div>
                </div>
                <div class="row hidden-sm hidden-xs">
                  <div class="col-md-12">
                    <hr>
                    <a class="banner banner-1" href="#">
                      <img src="./img/banner05.jpg" alt="">
                      <div class="banner-caption text-center">
                        <h2 class="white-color">NEW COLLECTION</h2>
                        <h3 class="white-color font-weak">HOT DEAL</h3>
                      </div>
                    </a>
                  </div>
                </div>
              </div>
            </li>
            <li class="dropdown side-dropdown">
              <a class="dropdown-toggle" data-toggle="dropdown" aria-expanded="true"><?php echo $lang['subdos']; ?> <i class="fa fa-angle-right"></i></a>
              <div class="custom-menu">
                <div class="row">
                  <div class="col-md-4">
                    <ul class="list-links">
                      <li>
                        <h3 class="list-links-title">PRENDAS</h3></li>
                      <li><a href="#">CAMISAS</a></li>
                      <li><a href="#"><?php echo $lang['cattwo']; ?></a></li>
                      <li><a href="#">CHOMPAS</a></li>
                      <li><a href="#">CASACAS</a></li>
                      <li><a href="#">POLOS</a></li>
                      <li><a href="#">JEANS</a></li>
                    </ul>
                    <hr class="hidden-md hidden-lg">
                  </div>
                  <div class="col-md-4">
                    <ul class="list-links">
                      <li>
                        <h3 class="list-links-title">PRENDAS DE VESTIR</h3></li>
                      <li><a href="#">TERNOS</a></li>
                      <li><a href="#">CORBATAS</a></li>
                      <li><a href="#">SOMBREROS</a></li>
                      <li><a href="#">PANTALONES</a></li>
                      <li><a href="#">ABRIGOS</a></li>
                      <li><a href="#">ZAPATOS</a></li>
                    </ul>
                    <hr class="hidden-md hidden-lg">
                  </div>
                  <div class="col-md-4">
                    <ul class="list-links">
                      <li>
                        <h3 class="list-links-title">DEPORTIVAS</h3></li>
                      <li><a href="#">BUZOS</a></li>
                      <li><a href="#">SUDADERAS</a></li>
                      <li><a href="#">GORROS</a></li>
                      <li><a href="#">ZAPATILLAS</a></li>
                    </ul>
                  </div>
                </div>
                <div class="row hidden-sm hidden-xs">
                  <div class="col-md-12">
                    <hr>
                    <a class="banner banner-1" href="#">
                      <img src="./img/banner05.jpg" alt="">
                      <div class="banner-caption text-center">
                        <h2 class="white-color">NEW COLLECTION</h2>
                        <h3 class="white-color font-weak">HOT DEAL</h3>
                      </div>
                    </a>
                  </div>
                </div>
              </div>
            </li>
            <li class="dropdown side-dropdown"><a class="dropdown-toggle" data-toggle="dropdown" aria-expanded="true"><?php echo $lang['subtres']; ?> <i class="fa fa-angle-right"></i></a>
              <div class="custom-menu">
                <div class="row">
                  <div class="col-md-4">
                    <ul class="list-links">
                      <li>
                        <h3 class="list-links-title">ACCESORIOS</h3></li>
                      <li><a href="#">ANILLOS</a></li>
                      <li><a href="#">GAFAS DE SOL</a></li>
                      <li><a href="#">GEMELOS</a></li>
                      <li><a href="#">ALFILER DE CORBATAS</a></li>
                      <li><a href="#">PAÑUELOS</a></li>
                    </ul>
                    <hr>
                    <ul class="list-links">
                      <li>
                        <h3 class="list-links-title">TECNOLÓGICOS</h3></li>
                      <li><a href="#">AUDIFONOS</a></li>
                      <li><a href="#">PARLANTES INHALAMBRICOS</a></li>
                      <li><a href="#">MEMORIAS USB</a></li>
                      <li><a href="#">CANDADOS ANTIRROBOS</a></li>
                      <li><a href="#">CHROMECAST</a></li>
                      <li><a href="#">EXTENDEDOR DE WIFI</a></li>
                      <li><a href="#">CHROMECAST</a></li>
                    </ul>
                    <hr class="hidden-md hidden-lg">
                  </div>
                  <div class="col-md-4">
                    <ul class="list-links">
                      <li>
                        <h3 class="list-links-title">Categorías</h3></li>
                      <li><a href="#">Women’s Clothing</a></li>
                      <li><a href="#">Men’s Clothing</a></li>
                      <li><a href="#">Phones & Accessories</a></li>
                      <li><a href="#">Jewelry & Watches</a></li>
                      <li><a href="#">Bags & Shoes</a></li>
                    </ul>
                    <hr>
                    <ul class="list-links">
                      <li>
                        <h3 class="list-links-title">Categorías</h3></li>
                      <li><a href="#">Women’s Clothing</a></li>
                      <li><a href="#">Men’s Clothing</a></li>
                      <li><a href="#">Phones & Accessories</a></li>
                      <li><a href="#">Jewelry & Watches</a></li>
                      <li><a href="#">Bags & Shoes</a></li>
                    </ul>
                  </div>
                  <div class="col-md-4 hidden-sm hidden-xs">
                    <a class="banner banner-2" href="#">
                      <img src="./img/banner04.jpg" alt="">
                      <div class="banner-caption">
                        <h3 class="white-color">NEW<br>COLLECTION</h3>
                      </div>
                    </a>
                  </div>
                </div>
              </div>
            </li>
            <li><a href="#"><?php echo $lang['subqatro']; ?></a></li>
            <li><a href="#"><?php echo $lang['subcin']; ?></a></li>
            <li class="dropdown side-dropdown">
              <a class="dropdown-toggle" data-toggle="dropdown" aria-expanded="true"><?php echo $lang['subseis']; ?> <i class="fa fa-angle-right"></i></a>
              <div class="custom-menu">
                <div class="row">
                  <div class="col-md-4">
                    <ul class="list-links">
                      <li>
                        <h3 class="list-links-title">Categories</h3></li>
                      <li><a href="#">Women’s Clothing</a></li>
                      <li><a href="#">Men’s Clothing</a></li>
                      <li><a href="#">Phones & Accessories</a></li>
                      <li><a href="#">Jewelry & Watches</a></li>
                      <li><a href="#">Bags & Shoes</a></li>
                    </ul>
                    <hr>
                    <ul class="list-links">
                      <li>
                        <h3 class="list-links-title">Categories</h3></li>
                      <li><a href="#">Women’s Clothing</a></li>
                      <li><a href="#">Men’s Clothing</a></li>
                      <li><a href="#">Phones & Accessories</a></li>
                      <li><a href="#">Jewelry & Watches</a></li>
                      <li><a href="#">Bags & Shoes</a></li>
                    </ul>
                    <hr class="hidden-md hidden-lg">
                  </div>
                  <div class="col-md-4">
                    <ul class="list-links">
                      <li>
                        <h3 class="list-links-title">Categories</h3></li>
                      <li><a href="#">Women’s Clothing</a></li>
                      <li><a href="#">Men’s Clothing</a></li>
                      <li><a href="#">Phones & Accessories</a></li>
                      <li><a href="#">Jewelry & Watches</a></li>
                      <li><a href="#">Bags & Shoes</a></li>
                    </ul>
                    <hr>
                    <ul class="list-links">
                      <li>
                        <h3 class="list-links-title">Categories</h3></li>
                      <li><a href="#">Women’s Clothing</a></li>
                      <li><a href="#">Men’s Clothing</a></li>
                      <li><a href="#">Phones & Accessories</a></li>
                      <li><a href="#">Jewelry & Watches</a></li>
                      <li><a href="#">Bags & Shoes</a></li>
                    </ul>
                    <hr class="hidden-md hidden-lg">
                  </div>
                  <div class="col-md-4">
                    <ul class="list-links">
                      <li>
                        <h3 class="list-links-title">Categories</h3></li>
                      <li><a href="#">Women’s Clothing</a></li>
                      <li><a href="#">Men’s Clothing</a></li>
                      <li><a href="#">Phones & Accessories</a></li>
                      <li><a href="#">Jewelry & Watches</a></li>
                      <li><a href="#">Bags & Shoes</a></li>
                    </ul>
                    <hr>
                    <ul class="list-links">
                      <li>
                        <h3 class="list-links-title">Categories</h3></li>
                      <li><a href="#">Women’s Clothing</a></li>
                      <li><a href="#">Men’s Clothing</a></li>
                      <li><a href="#">Phones & Accessories</a></li>
                      <li><a href="#">Jewelry & Watches</a></li>
                      <li><a href="#">Bags & Shoes</a></li>
                    </ul>
                  </div>
                </div>
              </div>
            </li>
            <li><a href="#"><?php echo $lang['subsite']; ?></a></li>
            <li><a href="#"><?php echo $lang['subocto']; ?></a></li>
            <li><a href="#"><?php echo $lang['vall']; ?></a></li>
          </ul>
        </div>
        <!-- /category nav -->

        <!-- menu nav -->
        <div class="menu-nav">
          <span class="menu-header">Menu <i class="fa fa-bars"></i></span>
          <ul class="menu-list">
            <li><a href="./"><?php echo $lang['home']; ?></a></li>
            <li><a href="#"><?php echo $lang['shop']; ?></a></li>
            <li class="dropdown mega-dropdown"><a class="dropdown-toggle" data-toggle="dropdown" aria-expanded="true"><?php echo $lang['women']; ?> <i class="fa fa-caret-down"></i></a>
              <div class="custom-menu">
                <div class="row">
                  <div class="col-md-4">
                    <ul class="list-links">
                      <li>
                        <h3 class="list-links-title"><?php echo $lang['categ']; ?></h3></li>
                      <li><a href="#"><?php echo $lang['catone']; ?></a></li>
                      <li><a href="#"><?php echo $lang['cattwo']; ?></a></li>
                      <li><a href="#"><?php echo $lang['catthree']; ?></a></li>
                      <li><a href="#"><?php echo $lang['catfour']; ?></a></li>
                      <li><a href="#"><?php echo $lang['catfive']; ?></a></li>
                      <li><a href="#"><?php echo $lang['catsix']; ?></a></li>
                    </ul>
                    <hr class="hidden-md hidden-lg">
                  </div>
                  <div class="col-md-4">
                    <ul class="list-links">
                      <li>
                        <h3 class="list-links-title"><?php echo $lang['categ']; ?></h3></li>
                      <li><a href="#"><?php echo $lang['catseven']; ?></a></li>
                      <li><a href="#"><?php echo $lang['cateigth']; ?></a></li>
                      <li><a href="#"><?php echo $lang['catnine']; ?></a></li>
                      <li><a href="#"><?php echo $lang['catteen']; ?></a></li>
                      <li><a href="#"><?php echo $lang['cateleven']; ?></a></li>
                      <li><a href="#"><?php echo $lang['cattwelve']; ?></a></li>
                    </ul>
                    <hr class="hidden-md hidden-lg">
                  </div>
                  <div class="col-md-4">
                    <ul class="list-links">
                      <li>
                        <h3 class="list-links-title">Categories</h3></li>
                      <li><a href="#"><?php echo $lang['catthirteen']; ?></a></li>
                      <li><a href="#"><?php echo $lang['catfourteen']; ?></a></li>
                      <li><a href="#"><?php echo $lang['catfiveteen']; ?></a></li>
                      <li><a href="#"><?php echo $lang['catsixteen']; ?></a></li>
                    </ul>
                  </div>
                </div>
                <div class="row hidden-sm hidden-xs">
                  <div class="col-md-12">
                    <hr>
                    <a class="banner banner-1" href="#">
                      <img src="./img/banner05.jpg" alt="">
                      <div class="banner-caption text-center">
                        <h2 class="white-color">NEW COLLECTION</h2>
                        <h3 class="white-color font-weak">HOT DEAL</h3>
                      </div>
                    </a>
                  </div>
                </div>
              </div>
            </li>
            <li class="dropdown mega-dropdown full-width"><a class="dropdown-toggle" data-toggle="dropdown" aria-expanded="true"><?php echo $lang['men']; ?> <i class="fa fa-caret-down"></i></a>
              <div class="custom-menu">
                <div class="row">
                  <div class="col-md-3">
                    <div class="hidden-sm hidden-xs">
                      <a class="banner banner-1" href="#">
                        <img src="./img/banner06.jpg" alt="">
                        <div class="banner-caption text-center">
                          <h3 class="white-color text-uppercase">Women’s</h3>
                        </div>
                      </a>
                      <hr>
                    </div>
                    <ul class="list-links">
                      <li>
                        <h3 class="list-links-title">Categories</h3></li>
                      <li><a href="#">Women’s Clothing</a></li>
                      <li><a href="#">Men’s Clothing</a></li>
                      <li><a href="#">Phones & Accessories</a></li>
                      <li><a href="#">Jewelry & Watches</a></li>
                      <li><a href="#">Bags & Shoes</a></li>
                    </ul>
                  </div>
                  <div class="col-md-3">
                    <div class="hidden-sm hidden-xs">
                      <a class="banner banner-1" href="#">
                        <img src="./img/banner07.jpg" alt="">
                        <div class="banner-caption text-center">
                          <h3 class="white-color text-uppercase">Men’s</h3>
                        </div>
                      </a>
                    </div>
                    <hr>
                    <ul class="list-links">
                      <li>
                        <h3 class="list-links-title">Categories</h3></li>
                      <li><a href="#">Women’s Clothing</a></li>
                      <li><a href="#">Men’s Clothing</a></li>
                      <li><a href="#">Phones & Accessories</a></li>
                      <li><a href="#">Jewelry & Watches</a></li>
                      <li><a href="#">Bags & Shoes</a></li>
                    </ul>
                  </div>
                  <div class="col-md-3">
                    <div class="hidden-sm hidden-xs">
                      <a class="banner banner-1" href="#">
                        <img src="./img/banner08.jpg" alt="">
                        <div class="banner-caption text-center">
                          <h3 class="white-color text-uppercase">Accessories</h3>
                        </div>
                      </a>
                    </div>
                    <hr>
                    <ul class="list-links">
                      <li>
                        <h3 class="list-links-title">Categories</h3></li>
                      <li><a href="#">Women’s Clothing</a></li>
                      <li><a href="#">Men’s Clothing</a></li>
                      <li><a href="#">Phones & Accessories</a></li>
                      <li><a href="#">Jewelry & Watches</a></li>
                      <li><a href="#">Bags & Shoes</a></li>
                    </ul>
                  </div>
                  <div class="col-md-3">
                    <div class="hidden-sm hidden-xs">
                      <a class="banner banner-1" href="#">
                        <img src="./img/banner09.jpg" alt="">
                        <div class="banner-caption text-center">
                          <h3 class="white-color text-uppercase">Bags</h3>
                        </div>
                      </a>
                    </div>
                    <hr>
                    <ul class="list-links">
                      <li>
                        <h3 class="list-links-title">Categories</h3></li>
                      <li><a href="#">Women’s Clothing</a></li>
                      <li><a href="#">Men’s Clothing</a></li>
                      <li><a href="#">Phones & Accessories</a></li>
                      <li><a href="#">Jewelry & Watches</a></li>
                      <li><a href="#">Bags & Shoes</a></li>
                    </ul>
                  </div>
                </div>
              </div>
            </li>
            <li><a href="#"><?php echo $lang['sale']; ?></a></li>
            <li class="dropdown default-dropdown"><a class="dropdown-toggle" data-toggle="dropdown" aria-expanded="true">Pages <i class="fa fa-caret-down"></i></a>
              <ul class="custom-menu">
                <li><a href="index.html">Home</a></li>
                <li><a href="products.html">Products</a></li>
                <li><a href="product-page.html">Product Details</a></li>
                <li><a href="checkout.html">Checkout</a></li>
              </ul>
            </li>
          </ul>
        </div>
        <!-- menu nav -->
      </div>
    </div>
    <!-- /container -->
  </div>
  <!-- /NAVIGATION -->
  
  <!-- HOME -->
  <div id="home">
    <!-- container -->
    <div class="container">
      <!-- home wrap -->
      <div class="home-wrap">
        <!-- home slick -->
        <div id="home-slick">
          <!-- banner -->
          <div class="banner banner-1">
            <img src="./img/banner01.jpg" alt="">
            <div class="banner-caption text-center">
              <h1><?php echo $lang['sli1']; ?></h1>
              <h3 class="white-color font-weak"><?php echo $lang['ut']; ?> 50% <?php echo $lang['disc']; ?></h3>
              <button class="primary-btn"><?php echo $lang['spnow']; ?></button>
            </div>
          </div>
          <!-- /banner -->

          <!-- banner -->
          <div class="banner banner-1">
            <img src="./img/banner02.jpg" alt="">
            <div class="banner-caption text-center">
              <h1 class="primary-color"><?php echo $lang['sli2']; ?><br><span class="white-color font-weak"><?php echo $lang['ut']; ?> 50% <?php echo $lang['disc']; ?></span></h1>
              <button class="primary-btn"><?php echo $lang['spnow']; ?></button>
            </div>
          </div>
          <!-- /banner -->

          <!-- banner -->
          <div class="banner banner-1">
            <img src="./img/banner03.jpg" alt="">
            <div class="banner-caption text-center">
              <h1 class="white-color"><?php echo $lang['sli3']; ?></h1>
              <button class="primary-btn"><?php echo $lang['spnow']; ?></button>
            </div>
          </div>
          <!-- /banner -->
        </div>
        <!-- /home slick -->
      </div>
      <!-- /home wrap -->
    </div>
    <!-- /container -->
  </div>
  <!-- /HOME -->

  <!-- section -->
  <div class="section">
    <!-- container -->
    <div class="container">
      <!-- row -->
      <div class="row">
        <!-- banner -->
        <div class="col-md-4 col-sm-6">
          <a class="banner banner-1" href="#">
            <img src="./img/banner10.jpg" alt="">
            <div class="banner-caption text-center">
              <h2 class="white-color" style="text-transform: uppercase;"><?php echo $lang['sli3']; ?></h2>
            </div>
          </a>
        </div>
        <!-- /banner -->

        <!-- banner -->
        <div class="col-md-4 col-sm-6">
          <a class="banner banner-1" href="#">
            <img src="./img/banner11.jpg" alt="">
            <div class="banner-caption text-center">
              <h2 class="white-color" style="text-transform: uppercase;"><?php echo $lang['sli3']; ?></h2>
            </div>
          </a>
        </div>
        <!-- /banner -->

        <!-- banner -->
        <div class="col-md-4 col-md-offset-0 col-sm-6 col-sm-offset-3">
          <a class="banner banner-1" href="#">
            <img src="./img/banner12.jpg" alt="">
            <div class="banner-caption text-center">
              <h2 class="white-color" style="text-transform: uppercase;"><?php echo $lang['sli3']; ?></h2>
            </div>
          </a>
        </div>
        <!-- /banner -->

      </div>
      <!-- /row -->
    </div>
    <!-- /container -->
  </div>
  <!-- /section -->

  <!-- section -->
  <div class="section">
    <!-- container -->
    <div class="container">
      <!-- row -->
      <div class="row">
        <!-- section-title -->
        <div class="col-md-12">
          <div class="section-title">
            <h2 class="title"><?php echo $lang['offer']; ?></h2>
            <div class="pull-right">
              <div class="product-slick-dots-1 custom-dots"></div>
            </div>
          </div>
        </div>
        <!-- /section-title -->

        <!-- banner -->
        <div class="col-md-3 col-sm-6 col-xs-6">
          <div class="banner banner-2">
            <img src="./img/banner14.jpg" alt="">
            <div class="banner-caption">
              <h2 class="white-color" style="text-transform: uppercase;"><?php echo $lang['sli3'] ;?></h2>
              <button class="primary-btn"><?php echo $lang['spnow']; ?></button>
            </div>
          </div>
        </div>
        <!-- /banner -->

        <!-- Product Slick -->
        <div class="col-md-9 col-sm-6 col-xs-6">
          <div class="row">
            <div id="product-slick-1" class="product-slick">
              <!-- Product Single -->
              <div class="product product-single">
                <div class="product-thumb">
                  <div class="product-label">
                    <span><?php echo $lang['nw']; ?></span>
                    <span class="sale">-20%</span>
                  </div>
                  <ul class="product-countdown">
                    <li><span>00 H</span></li>
                    <li><span>00 M</span></li>
                    <li><span>00 S</span></li>
                  </ul>
                  <button class="main-btn quick-view"><i class="fa fa-search-plus"></i> Quick view</button>
                  <img src="./img/product01.jpg" alt="">
                </div>
                <div class="product-body">
                  <h3 class="product-price">S/ 32.50 <del class="product-old-price">S/ 45.00</del></h3>
                  <div class="product-rating">
                    <i class="fa fa-star"></i>
                    <i class="fa fa-star"></i>
                    <i class="fa fa-star"></i>
                    <i class="fa fa-star"></i>
                    <i class="fa fa-star-o empty"></i>
                  </div>
                  <h2 class="product-name"><a href="#">Product Name Goes Here</a></h2>
                  <div class="product-btns">
                    <button class="main-btn icon-btn"><i class="fa fa-heart"></i></button>
                    <button class="main-btn icon-btn"><i class="fa fa-exchange"></i></button>
                    <button class="primary-btn add-to-cart"><i class="fa fa-shopping-cart"></i> Add to Cart</button>
                  </div>
                </div>
              </div>
              <!-- /Product Single -->

              <!-- Product Single -->
              <div class="product product-single">
                <div class="product-thumb">
                  <div class="product-label">
                    <span class="sale">-20%</span>
                  </div>
                  <ul class="product-countdown">
                    <li><span>00 H</span></li>
                    <li><span>00 M</span></li>
                    <li><span>00 S</span></li>
                  </ul>
                  <button class="main-btn quick-view"><i class="fa fa-search-plus"></i> Quick view</button>
                  <img src="./img/product07.jpg" alt="">
                </div>
                <div class="product-body">
                  <h3 class="product-price">S/ 32.50 <del class="product-old-price">S/ 45.00</del></h3>
                  <div class="product-rating">
                    <i class="fa fa-star"></i>
                    <i class="fa fa-star"></i>
                    <i class="fa fa-star"></i>
                    <i class="fa fa-star"></i>
                    <i class="fa fa-star-o empty"></i>
                  </div>
                  <h2 class="product-name"><a href="#">Product Name Goes Here</a></h2>
                  <div class="product-btns">
                    <button class="main-btn icon-btn"><i class="fa fa-heart"></i></button>
                    <button class="main-btn icon-btn"><i class="fa fa-exchange"></i></button>
                    <button class="primary-btn add-to-cart"><i class="fa fa-shopping-cart"></i> Add to Cart</button>
                  </div>
                </div>
              </div>
              <!-- /Product Single -->

              <!-- Product Single -->
              <div class="product product-single">
                <div class="product-thumb">
                  <div class="product-label">
                    <span><?php echo $lang['nw']; ?></span>
                    <span class="sale">-20%</span>
                  </div>
                  <ul class="product-countdown">
                    <li><span>00 H</span></li>
                    <li><span>00 M</span></li>
                    <li><span>00 S</span></li>
                  </ul>
                  <button class="main-btn quick-view"><i class="fa fa-search-plus"></i> Quick view</button>
                  <img src="./img/product06.jpg" alt="">
                </div>
                <div class="product-body">
                  <h3 class="product-price">S/ 32.50 <del class="product-old-price">S/ 45.00</del></h3>
                  <div class="product-rating">
                    <i class="fa fa-star"></i>
                    <i class="fa fa-star"></i>
                    <i class="fa fa-star"></i>
                    <i class="fa fa-star"></i>
                    <i class="fa fa-star-o empty"></i>
                  </div>
                  <h2 class="product-name"><a href="#">Product Name Goes Here</a></h2>
                  <div class="product-btns">
                    <button class="main-btn icon-btn"><i class="fa fa-heart"></i></button>
                    <button class="main-btn icon-btn"><i class="fa fa-exchange"></i></button>
                    <button class="primary-btn add-to-cart"><i class="fa fa-shopping-cart"></i> Add to Cart</button>
                  </div>
                </div>
              </div>
              <!-- /Product Single -->

              <!-- Product Single -->
              <div class="product product-single">
                <div class="product-thumb">
                  <div class="product-label">
                    <span><?php echo $lang['nw']; ?></span>
                    <span class="sale">-20%</span>
                  </div>
                  <ul class="product-countdown">
                    <li><span>00 H</span></li>
                    <li><span>00 M</span></li>
                    <li><span>00 S</span></li>
                  </ul>
                  <button class="main-btn quick-view"><i class="fa fa-search-plus"></i> Quick view</button>
                  <img src="./img/product08.jpg" alt="">
                </div>
                <div class="product-body">
                  <h3 class="product-price">S/ 32.50 <del class="product-old-price">S/ 45.00</del></h3>
                  <div class="product-rating">
                    <i class="fa fa-star"></i>
                    <i class="fa fa-star"></i>
                    <i class="fa fa-star"></i>
                    <i class="fa fa-star"></i>
                    <i class="fa fa-star-o empty"></i>
                  </div>
                  <h2 class="product-name"><a href="#">Product Name Goes Here</a></h2>
                  <div class="product-btns">
                    <button class="main-btn icon-btn"><i class="fa fa-heart"></i></button>
                    <button class="main-btn icon-btn"><i class="fa fa-exchange"></i></button>
                    <button class="primary-btn add-to-cart"><i class="fa fa-shopping-cart"></i> Add to Cart</button>
                  </div>
                </div>
              </div>
              <!-- /Product Single -->
            </div>
          </div>
        </div>
        <!-- /Product Slick -->
      </div>
      <!-- /row -->

      <!-- row -->
      <div class="row">
        <!-- section title -->
        <div class="col-md-12">
          <div class="section-title">
            <h2 class="title"><?php echo $lang['offer']; ?></h2>
            <div class="pull-right">
              <div class="product-slick-dots-2 custom-dots">
              </div>
            </div>
          </div>
        </div>
        <!-- section title -->

        <!-- Product Single -->
        <div class="col-md-3 col-sm-6 col-xs-6">
          <div class="product product-single product-hot">
            <div class="product-thumb">
              <div class="product-label">
                <span class="sale">-20%</span>
              </div>
              <ul class="product-countdown">
                <li><span>00 H</span></li>
                <li><span>00 M</span></li>
                <li><span>00 S</span></li>
              </ul>
              <button class="main-btn quick-view"><i class="fa fa-search-plus"></i> Quick view</button>
              <img src="./img/product01.jpg" alt="">
            </div>
            <div class="product-body">
              <h3 class="product-price">S/ 32.50 <del class="product-old-price">S/ 45.00</del></h3>
              <div class="product-rating">
                <i class="fa fa-star"></i>
                <i class="fa fa-star"></i>
                <i class="fa fa-star"></i>
                <i class="fa fa-star"></i>
                <i class="fa fa-star-o empty"></i>
              </div>
              <h2 class="product-name"><a href="#">Product Name Goes Here</a></h2>
              <div class="product-btns">
                <button class="main-btn icon-btn"><i class="fa fa-heart"></i></button>
                <button class="main-btn icon-btn"><i class="fa fa-exchange"></i></button>
                <button class="primary-btn add-to-cart"><i class="fa fa-shopping-cart"></i> Add to Cart</button>
              </div>
            </div>
          </div>
        </div>
        <!-- /Product Single -->

        <!-- Product Slick -->
        <div class="col-md-9 col-sm-6 col-xs-6">
          <div class="row">
            <div id="product-slick-2" class="product-slick">
              <!-- Product Single -->
              <div class="product product-single">
                <div class="product-thumb">
                  <button class="main-btn quick-view"><i class="fa fa-search-plus"></i> Quick view</button>
                  <img src="./img/product06.jpg" alt="">
                </div>
                <div class="product-body">
                  <h3 class="product-price">S/ 32.50</h3>
                  <div class="product-rating">
                    <i class="fa fa-star"></i>
                    <i class="fa fa-star"></i>
                    <i class="fa fa-star"></i>
                    <i class="fa fa-star"></i>
                    <i class="fa fa-star-o empty"></i>
                  </div>
                  <h2 class="product-name"><a href="#">Product Name Goes Here</a></h2>
                  <div class="product-btns">
                    <button class="main-btn icon-btn"><i class="fa fa-heart"></i></button>
                    <button class="main-btn icon-btn"><i class="fa fa-exchange"></i></button>
                    <button class="primary-btn add-to-cart"><i class="fa fa-shopping-cart"></i> Add to Cart</button>
                  </div>
                </div>
              </div>
              <!-- /Product Single -->

              <!-- Product Single -->
              <div class="product product-single">
                <div class="product-thumb">
                  <div class="product-label">
                    <span class="sale">-20%</span>
                  </div>
                  <button class="main-btn quick-view"><i class="fa fa-search-plus"></i> Quick view</button>
                  <img src="./img/product05.jpg" alt="">
                </div>
                <div class="product-body">
                  <h3 class="product-price">S/ 32.50 <del class="product-old-price">S/ 45.00</del></h3>
                  <div class="product-rating">
                    <i class="fa fa-star"></i>
                    <i class="fa fa-star"></i>
                    <i class="fa fa-star"></i>
                    <i class="fa fa-star"></i>
                    <i class="fa fa-star-o empty"></i>
                  </div>
                  <h2 class="product-name"><a href="#">Product Name Goes Here</a></h2>
                  <div class="product-btns">
                    <button class="main-btn icon-btn"><i class="fa fa-heart"></i></button>
                    <button class="main-btn icon-btn"><i class="fa fa-exchange"></i></button>
                    <button class="primary-btn add-to-cart"><i class="fa fa-shopping-cart"></i> Add to Cart</button>
                  </div>
                </div>
              </div>
              <!-- /Product Single -->

              <!-- Product Single -->
              <div class="product product-single">
                <div class="product-thumb">
                  <button class="main-btn quick-view"><i class="fa fa-search-plus"></i> Quick view</button>
                  <img src="./img/product04.jpg" alt="">
                </div>
                <div class="product-body">
                  <h3 class="product-price">S/ 32.50</h3>
                  <div class="product-rating">
                    <i class="fa fa-star"></i>
                    <i class="fa fa-star"></i>
                    <i class="fa fa-star"></i>
                    <i class="fa fa-star"></i>
                    <i class="fa fa-star-o empty"></i>
                  </div>
                  <h2 class="product-name"><a href="#">Product Name Goes Here</a></h2>
                  <div class="product-btns">
                    <button class="main-btn icon-btn"><i class="fa fa-heart"></i></button>
                    <button class="main-btn icon-btn"><i class="fa fa-exchange"></i></button>
                    <button class="primary-btn add-to-cart"><i class="fa fa-shopping-cart"></i> Add to Cart</button>
                  </div>
                </div>
              </div>
              <!-- /Product Single -->

              <!-- Product Single -->
              <div class="product product-single">
                <div class="product-thumb">
                  <div class="product-label">
                    <span>New</span>
                    <span class="sale">-20%</span>
                  </div>
                  <button class="main-btn quick-view"><i class="fa fa-search-plus"></i> Quick view</button>
                  <img src="./img/product03.jpg" alt="">
                </div>
                <div class="product-body">
                  <h3 class="product-price">S/ 32.50 <del class="product-old-price">S/ 45.00</del></h3>
                  <div class="product-rating">
                    <i class="fa fa-star"></i>
                    <i class="fa fa-star"></i>
                    <i class="fa fa-star"></i>
                    <i class="fa fa-star"></i>
                    <i class="fa fa-star-o empty"></i>
                  </div>
                  <h2 class="product-name"><a href="#">Product Name Goes Here</a></h2>
                  <div class="product-btns">
                    <button class="main-btn icon-btn"><i class="fa fa-heart"></i></button>
                    <button class="main-btn icon-btn"><i class="fa fa-exchange"></i></button>
                    <button class="primary-btn add-to-cart"><i class="fa fa-shopping-cart"></i> Add to Cart</button>
                  </div>
                </div>
              </div>
              <!-- /Product Single -->

            </div>
          </div>
        </div>
        <!-- /Product Slick -->
      </div>
      <!-- /row -->
    </div>
    <!-- /container -->
  </div>
  <!-- /section -->

  <!-- section -->
  <div class="section section-grey">
    <!-- container -->
    <div class="container">
      <!-- row -->
      <div class="row">
        <!-- banner -->
        <div class="col-md-8">
          <div class="banner banner-1">
            <img src="./img/banner13.jpg" alt="">
            <div class="banner-caption text-center">
              <h1 class="primary-color">HOT DEAL<br><span class="white-color font-weak">Up to 50% OFF</span></h1>
              <button class="primary-btn">Shop Now</button>
            </div>
          </div>
        </div>
        <!-- /banner -->

        <!-- banner -->
        <div class="col-md-4 col-sm-6">
          <a class="banner banner-1" href="#">
            <img src="./img/banner11.jpg" alt="">
            <div class="banner-caption text-center">
              <h2 class="white-color">NEW COLLECTION</h2>
            </div>
          </a>
        </div>
        <!-- /banner -->

        <!-- banner -->
        <div class="col-md-4 col-sm-6">
          <a class="banner banner-1" href="#">
            <img src="./img/banner12.jpg" alt="">
            <div class="banner-caption text-center">
              <h2 class="white-color">NEW COLLECTION</h2>
            </div>
          </a>
        </div>
        <!-- /banner -->
      </div>
      <!-- /row -->
    </div>
    <!-- /container -->
  </div>
  <!-- /section -->

  <!-- section -->
  <div class="section">
    <!-- container -->
    <div class="container">
      <!-- row -->
      <div class="row">
        <!-- section title -->
        <div class="col-md-12">
          <div class="section-title">
            <h2 class="title"><?php echo $lang['latprod']; ?></h2>
          </div>
        </div>
        <!-- section title -->

        <!-- Product Single -->
        <div class="col-md-3 col-sm-6 col-xs-6">
          <div class="product product-single">
            <div class="product-thumb">
              <button class="main-btn quick-view"><i class="fa fa-search-plus"></i> Quick view</button>
              <img src="./img/product01.jpg" alt="">
            </div>
            <div class="product-body">
              <h3 class="product-price">$32.50</h3>
              <div class="product-rating">
                <i class="fa fa-star"></i>
                <i class="fa fa-star"></i>
                <i class="fa fa-star"></i>
                <i class="fa fa-star"></i>
                <i class="fa fa-star-o empty"></i>
              </div>
              <h2 class="product-name"><a href="#">Product Name Goes Here</a></h2>
              <div class="product-btns">
                <button class="main-btn icon-btn"><i class="fa fa-heart"></i></button>
                <button class="main-btn icon-btn"><i class="fa fa-exchange"></i></button>
                <button class="primary-btn add-to-cart"><i class="fa fa-shopping-cart"></i> Add to Cart</button>
              </div>
            </div>
          </div>
        </div>
        <!-- /Product Single -->

        <!-- Product Single -->
        <div class="col-md-3 col-sm-6 col-xs-6">
          <div class="product product-single">
            <div class="product-thumb">
              <div class="product-label">
                <span>New</span>
                <span class="sale">-20%</span>
              </div>
              <button class="main-btn quick-view"><i class="fa fa-search-plus"></i> Quick view</button>
              <img src="./img/product02.jpg" alt="">
            </div>
            <div class="product-body">
              <h3 class="product-price">$32.50 <del class="product-old-price">$45.00</del></h3>
              <div class="product-rating">
                <i class="fa fa-star"></i>
                <i class="fa fa-star"></i>
                <i class="fa fa-star"></i>
                <i class="fa fa-star"></i>
                <i class="fa fa-star-o empty"></i>
              </div>
              <h2 class="product-name"><a href="#">Product Name Goes Here</a></h2>
              <div class="product-btns">
                <button class="main-btn icon-btn"><i class="fa fa-heart"></i></button>
                <button class="main-btn icon-btn"><i class="fa fa-exchange"></i></button>
                <button class="primary-btn add-to-cart"><i class="fa fa-shopping-cart"></i> Add to Cart</button>
              </div>
            </div>
          </div>
        </div>
        <!-- /Product Single -->

        <!-- Product Single -->
        <div class="col-md-3 col-sm-6 col-xs-6">
          <div class="product product-single">
            <div class="product-thumb">
              <div class="product-label">
                <span>New</span>
                <span class="sale">-20%</span>
              </div>
              <button class="main-btn quick-view"><i class="fa fa-search-plus"></i> Quick view</button>
              <img src="./img/product03.jpg" alt="">
            </div>
            <div class="product-body">
              <h3 class="product-price">$32.50 <del class="product-old-price">$45.00</del></h3>
              <div class="product-rating">
                <i class="fa fa-star"></i>
                <i class="fa fa-star"></i>
                <i class="fa fa-star"></i>
                <i class="fa fa-star"></i>
                <i class="fa fa-star-o empty"></i>
              </div>
              <h2 class="product-name"><a href="#">Product Name Goes Here</a></h2>
              <div class="product-btns">
                <button class="main-btn icon-btn"><i class="fa fa-heart"></i></button>
                <button class="main-btn icon-btn"><i class="fa fa-exchange"></i></button>
                <button class="primary-btn add-to-cart"><i class="fa fa-shopping-cart"></i> Add to Cart</button>
              </div>
            </div>
          </div>
        </div>
        <!-- /Product Single -->

        <!-- Product Single -->
        <div class="col-md-3 col-sm-6 col-xs-6">
          <div class="product product-single">
            <div class="product-thumb">
              <div class="product-label">
                <span>New</span>
              </div>
              <button class="main-btn quick-view"><i class="fa fa-search-plus"></i> Quick view</button>
              <img src="./img/product04.jpg" alt="">
            </div>
            <div class="product-body">
              <h3 class="product-price">$32.50</h3>
              <div class="product-rating">
                <i class="fa fa-star"></i>
                <i class="fa fa-star"></i>
                <i class="fa fa-star"></i>
                <i class="fa fa-star"></i>
                <i class="fa fa-star-o empty"></i>
              </div>
              <h2 class="product-name"><a href="#">Product Name Goes Here</a></h2>
              <div class="product-btns">
                <button class="main-btn icon-btn"><i class="fa fa-heart"></i></button>
                <button class="main-btn icon-btn"><i class="fa fa-exchange"></i></button>
                <button class="primary-btn add-to-cart"><i class="fa fa-shopping-cart"></i> Add to Cart</button>
              </div>
            </div>
          </div>
        </div>
        <!-- /Product Single -->
      </div>
      <!-- /row -->

      <!-- row -->
      <div class="row">
        <!-- banner -->
        <div class="col-md-3 col-sm-6 col-xs-6">
          <div class="banner banner-2">
            <img src="./img/banner15.jpg" alt="">
            <div class="banner-caption">
              <h2 class="white-color">NEW<br>COLLECTION</h2>
              <button class="primary-btn">Shop Now</button>
            </div>
          </div>
        </div>
        <!-- /banner -->

        <!-- Product Single -->
        <div class="col-md-3 col-sm-6 col-xs-6">
          <div class="product product-single">
            <div class="product-thumb">
              <div class="product-label">
                <span>New</span>
                <span class="sale">-20%</span>
              </div>
              <button class="main-btn quick-view"><i class="fa fa-search-plus"></i> Quick view</button>
              <img src="./img/product07.jpg" alt="">
            </div>
            <div class="product-body">
              <h3 class="product-price">$32.50 <del class="product-old-price">$45.00</del></h3>
              <div class="product-rating">
                <i class="fa fa-star"></i>
                <i class="fa fa-star"></i>
                <i class="fa fa-star"></i>
                <i class="fa fa-star"></i>
                <i class="fa fa-star-o empty"></i>
              </div>
              <h2 class="product-name"><a href="#">Product Name Goes Here</a></h2>
              <div class="product-btns">
                <button class="main-btn icon-btn"><i class="fa fa-heart"></i></button>
                <button class="main-btn icon-btn"><i class="fa fa-exchange"></i></button>
                <button class="primary-btn add-to-cart"><i class="fa fa-shopping-cart"></i> Add to Cart</button>
              </div>
            </div>
          </div>
        </div>
        <!-- /Product Single -->

        <!-- Product Single -->
        <div class="col-md-3 col-sm-6 col-xs-6">
          <div class="product product-single">
            <div class="product-thumb">
              <div class="product-label">
                <span>New</span>
                <span class="sale">-20%</span>
              </div>
              <button class="main-btn quick-view"><i class="fa fa-search-plus"></i> Quick view</button>
              <img src="./img/product06.jpg" alt="">
            </div>
            <div class="product-body">
              <h3 class="product-price">$32.50 <del class="product-old-price">$45.00</del></h3>
              <div class="product-rating">
                <i class="fa fa-star"></i>
                <i class="fa fa-star"></i>
                <i class="fa fa-star"></i>
                <i class="fa fa-star"></i>
                <i class="fa fa-star-o empty"></i>
              </div>
              <h2 class="product-name"><a href="#">Product Name Goes Here</a></h2>
              <div class="product-btns">
                <button class="main-btn icon-btn"><i class="fa fa-heart"></i></button>
                <button class="main-btn icon-btn"><i class="fa fa-exchange"></i></button>
                <button class="primary-btn add-to-cart"><i class="fa fa-shopping-cart"></i> Add to Cart</button>
              </div>
            </div>
          </div>
        </div>
        <!-- /Product Single -->

        <!-- Product Single -->
        <div class="col-md-3 col-sm-6 col-xs-6">
          <div class="product product-single">
            <div class="product-thumb">
              <div class="product-label">
                <span>New</span>
                <span class="sale">-20%</span>
              </div>
              <button class="main-btn quick-view"><i class="fa fa-search-plus"></i> Quick view</button>
              <img src="./img/product05.jpg" alt="">
            </div>
            <div class="product-body">
              <h3 class="product-price">$32.50 <del class="product-old-price">$45.00</del></h3>
              <div class="product-rating">
                <i class="fa fa-star"></i>
                <i class="fa fa-star"></i>
                <i class="fa fa-star"></i>
                <i class="fa fa-star"></i>
                <i class="fa fa-star-o empty"></i>
              </div>
              <h2 class="product-name"><a href="#">Product Name Goes Here</a></h2>
              <div class="product-btns">
                <button class="main-btn icon-btn"><i class="fa fa-heart"></i></button>
                <button class="main-btn icon-btn"><i class="fa fa-exchange"></i></button>
                <button class="primary-btn add-to-cart"><i class="fa fa-shopping-cart"></i> Add to Cart</button>
              </div>
            </div>
          </div>
        </div>
        <!-- /Product Single -->
      </div>
      <!-- /row -->

      <!-- row -->
      <div class="row">
        <!-- section title -->
        <div class="col-md-12">
          <div class="section-title">
            <h2 class="title"><?php echo $lang['pick']; ?></h2>
          </div>
        </div>
        <!-- section title -->

        <!-- Product Single -->
        <div class="col-md-3 col-sm-6 col-xs-6">
          <div class="product product-single">
            <div class="product-thumb">
              <button class="main-btn quick-view"><i class="fa fa-search-plus"></i> Quick view</button>
              <img src="./img/product04.jpg" alt="">
            </div>
            <div class="product-body">
              <h3 class="product-price">$32.50</h3>
              <div class="product-rating">
                <i class="fa fa-star"></i>
                <i class="fa fa-star"></i>
                <i class="fa fa-star"></i>
                <i class="fa fa-star"></i>
                <i class="fa fa-star-o empty"></i>
              </div>
              <h2 class="product-name"><a href="#">Product Name Goes Here</a></h2>
              <div class="product-btns">
                <button class="main-btn icon-btn"><i class="fa fa-heart"></i></button>
                <button class="main-btn icon-btn"><i class="fa fa-exchange"></i></button>
                <button class="primary-btn add-to-cart"><i class="fa fa-shopping-cart"></i> Add to Cart</button>
              </div>
            </div>
          </div>
        </div>
        <!-- /Product Single -->

        <!-- Product Single -->
        <div class="col-md-3 col-sm-6 col-xs-6">
          <div class="product product-single">
            <div class="product-thumb">
              <div class="product-label">
                <span>New</span>
              </div>
              <button class="main-btn quick-view"><i class="fa fa-search-plus"></i> Quick view</button>
              <img src="./img/product03.jpg" alt="">
            </div>
            <div class="product-body">
              <h3 class="product-price">$32.50</h3>
              <div class="product-rating">
                <i class="fa fa-star"></i>
                <i class="fa fa-star"></i>
                <i class="fa fa-star"></i>
                <i class="fa fa-star"></i>
                <i class="fa fa-star-o empty"></i>
              </div>
              <h2 class="product-name"><a href="#">Product Name Goes Here</a></h2>
              <div class="product-btns">
                <button class="main-btn icon-btn"><i class="fa fa-heart"></i></button>
                <button class="main-btn icon-btn"><i class="fa fa-exchange"></i></button>
                <button class="primary-btn add-to-cart"><i class="fa fa-shopping-cart"></i> Add to Cart</button>
              </div>
            </div>
          </div>
        </div>
        <!-- /Product Single -->

        <!-- Product Single -->
        <div class="col-md-3 col-sm-6 col-xs-6">
          <div class="product product-single">
            <div class="product-thumb">
              <div class="product-label">
                <span class="sale">-20%</span>
              </div>
              <button class="main-btn quick-view"><i class="fa fa-search-plus"></i> Quick view</button>
              <img src="./img/product02.jpg" alt="">
            </div>
            <div class="product-body">
              <h3 class="product-price">$32.50 <del class="product-old-price">$45.00</del></h3>
              <div class="product-rating">
                <i class="fa fa-star"></i>
                <i class="fa fa-star"></i>
                <i class="fa fa-star"></i>
                <i class="fa fa-star"></i>
                <i class="fa fa-star-o empty"></i>
              </div>
              <h2 class="product-name"><a href="#">Product Name Goes Here</a></h2>
              <div class="product-btns">
                <button class="main-btn icon-btn"><i class="fa fa-heart"></i></button>
                <button class="main-btn icon-btn"><i class="fa fa-exchange"></i></button>
                <button class="primary-btn add-to-cart"><i class="fa fa-shopping-cart"></i> Add to Cart</button>
              </div>
            </div>
          </div>
        </div>
        <!-- /Product Single -->

        <!-- Product Single -->
        <div class="col-md-3 col-sm-6 col-xs-6">
          <div class="product product-single">
            <div class="product-thumb">
              <div class="product-label">
                <span>New</span>
                <span class="sale">-20%</span>
              </div>
              <button class="main-btn quick-view"><i class="fa fa-search-plus"></i> Quick view</button>
              <img src="./img/product01.jpg" alt="">
            </div>
            <div class="product-body">
              <h3 class="product-price">$32.50 <del class="product-old-price">$45.00</del></h3>
              <div class="product-rating">
                <i class="fa fa-star"></i>
                <i class="fa fa-star"></i>
                <i class="fa fa-star"></i>
                <i class="fa fa-star"></i>
                <i class="fa fa-star-o empty"></i>
              </div>
              <h2 class="product-name"><a href="#">Product Name Goes Here</a></h2>
              <div class="product-btns">
                <button class="main-btn icon-btn"><i class="fa fa-heart"></i></button>
                <button class="main-btn icon-btn"><i class="fa fa-exchange"></i></button>
                <button class="primary-btn add-to-cart"><i class="fa fa-shopping-cart"></i> Add to Cart</button>
              </div>
            </div>
          </div>
        </div>
        <!-- /Product Single -->
      </div>
      <!-- /row -->
    </div>
    <!-- /container -->
  </div>
  <!-- /section -->