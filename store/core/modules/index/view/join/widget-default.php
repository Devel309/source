<?php
if (isset($_GET['lang'])){
  $language = $_GET['lang'];
  if(!empty($language)){
    require("lang/".$language.".php");
  }
}else{
  $language = substr($_SERVER['HTTP_ACCEPT_LANGUAGE'],0,2);
  require("lang/".$language.".php");
}
?>
  <!-- NAVIGATION -->
  <div id="navigation">
    <!-- container -->
    <div class="container">
      <div id="responsive-nav">
        <!-- category nav -->
        <div class="category-nav show-on-click">
          <span class="category-header"><?php echo $lang['categ']; ?> <i class="fa fa-list"></i></span>
          <ul class="category-list">
            <li class="dropdown side-dropdown">
              <a class="dropdown-toggle" data-toggle="dropdown" aria-expanded="true"><?php echo $lang['subuno']; ?> <i class="fa fa-angle-right"></i></a>
              <div class="custom-menu">
                <div class="row">
                  <div class="col-md-4">
                    <ul class="list-links">
                      <li>
                        <h3 class="list-links-title">Categories</h3></li>
                      <li><a href="#">Women’s Clothing</a></li>
                      <li><a href="#">Men’s Clothing</a></li>
                      <li><a href="#">Phones & Accessories</a></li>
                      <li><a href="#">Jewelry & Watches</a></li>
                      <li><a href="#">Bags & Shoes</a></li>
                    </ul>
                    <hr class="hidden-md hidden-lg">
                  </div>
                  <div class="col-md-4">
                    <ul class="list-links">
                      <li>
                        <h3 class="list-links-title">Categories</h3></li>
                      <li><a href="#">Women’s Clothing</a></li>
                      <li><a href="#">Men’s Clothing</a></li>
                      <li><a href="#">Phones & Accessories</a></li>
                      <li><a href="#">Jewelry & Watches</a></li>
                      <li><a href="#">Bags & Shoes</a></li>
                    </ul>
                    <hr class="hidden-md hidden-lg">
                  </div>
                  <div class="col-md-4">
                    <ul class="list-links">
                      <li>
                        <h3 class="list-links-title">Categories</h3></li>
                      <li><a href="#">Women’s Clothing</a></li>
                      <li><a href="#">Men’s Clothing</a></li>
                      <li><a href="#">Phones & Accessories</a></li>
                      <li><a href="#">Jewelry & Watches</a></li>
                      <li><a href="#">Bags & Shoes</a></li>
                    </ul>
                  </div>
                </div>
                <div class="row hidden-sm hidden-xs">
                  <div class="col-md-12">
                    <hr>
                    <a class="banner banner-1" href="#">
                      <img src="./img/banner05.jpg" alt="">
                      <div class="banner-caption text-center">
                        <h2 class="white-color">NEW COLLECTION</h2>
                        <h3 class="white-color font-weak">HOT DEAL</h3>
                      </div>
                    </a>
                  </div>
                </div>
              </div>
            </li>
            <li><a href="#"><?php echo $lang['subdos']; ?></a></li>
            <li class="dropdown side-dropdown"><a class="dropdown-toggle" data-toggle="dropdown" aria-expanded="true"><?php echo $lang['subtres']; ?> <i class="fa fa-angle-right"></i></a>
              <div class="custom-menu">
                <div class="row">
                  <div class="col-md-4">
                    <ul class="list-links">
                      <li>
                        <h3 class="list-links-title">Categories</h3></li>
                      <li><a href="#">Women’s Clothing</a></li>
                      <li><a href="#">Men’s Clothing</a></li>
                      <li><a href="#">Phones & Accessories</a></li>
                      <li><a href="#">Jewelry & Watches</a></li>
                      <li><a href="#">Bags & Shoes</a></li>
                    </ul>
                    <hr>
                    <ul class="list-links">
                      <li>
                        <h3 class="list-links-title">Categories</h3></li>
                      <li><a href="#">Women’s Clothing</a></li>
                      <li><a href="#">Men’s Clothing</a></li>
                      <li><a href="#">Phones & Accessories</a></li>
                      <li><a href="#">Jewelry & Watches</a></li>
                      <li><a href="#">Bags & Shoes</a></li>
                    </ul>
                    <hr class="hidden-md hidden-lg">
                  </div>
                  <div class="col-md-4">
                    <ul class="list-links">
                      <li>
                        <h3 class="list-links-title">Categories</h3></li>
                      <li><a href="#">Women’s Clothing</a></li>
                      <li><a href="#">Men’s Clothing</a></li>
                      <li><a href="#">Phones & Accessories</a></li>
                      <li><a href="#">Jewelry & Watches</a></li>
                      <li><a href="#">Bags & Shoes</a></li>
                    </ul>
                    <hr>
                    <ul class="list-links">
                      <li>
                        <h3 class="list-links-title">Categories</h3></li>
                      <li><a href="#">Women’s Clothing</a></li>
                      <li><a href="#">Men’s Clothing</a></li>
                      <li><a href="#">Phones & Accessories</a></li>
                      <li><a href="#">Jewelry & Watches</a></li>
                      <li><a href="#">Bags & Shoes</a></li>
                    </ul>
                  </div>
                  <div class="col-md-4 hidden-sm hidden-xs">
                    <a class="banner banner-2" href="#">
                      <img src="./img/banner04.jpg" alt="">
                      <div class="banner-caption">
                        <h3 class="white-color">NEW<br>COLLECTION</h3>
                      </div>
                    </a>
                  </div>
                </div>
              </div>
            </li>
            <li><a href="#"><?php echo $lang['subqatro']; ?></a></li>
            <li><a href="#"><?php echo $lang['subcin']; ?></a></li>
            <li class="dropdown side-dropdown">
              <a class="dropdown-toggle" data-toggle="dropdown" aria-expanded="true"><?php echo $lang['subseis']; ?> <i class="fa fa-angle-right"></i></a>
              <div class="custom-menu">
                <div class="row">
                  <div class="col-md-4">
                    <ul class="list-links">
                      <li>
                        <h3 class="list-links-title">Categories</h3></li>
                      <li><a href="#">Women’s Clothing</a></li>
                      <li><a href="#">Men’s Clothing</a></li>
                      <li><a href="#">Phones & Accessories</a></li>
                      <li><a href="#">Jewelry & Watches</a></li>
                      <li><a href="#">Bags & Shoes</a></li>
                    </ul>
                    <hr>
                    <ul class="list-links">
                      <li>
                        <h3 class="list-links-title">Categories</h3></li>
                      <li><a href="#">Women’s Clothing</a></li>
                      <li><a href="#">Men’s Clothing</a></li>
                      <li><a href="#">Phones & Accessories</a></li>
                      <li><a href="#">Jewelry & Watches</a></li>
                      <li><a href="#">Bags & Shoes</a></li>
                    </ul>
                    <hr class="hidden-md hidden-lg">
                  </div>
                  <div class="col-md-4">
                    <ul class="list-links">
                      <li>
                        <h3 class="list-links-title">Categories</h3></li>
                      <li><a href="#">Women’s Clothing</a></li>
                      <li><a href="#">Men’s Clothing</a></li>
                      <li><a href="#">Phones & Accessories</a></li>
                      <li><a href="#">Jewelry & Watches</a></li>
                      <li><a href="#">Bags & Shoes</a></li>
                    </ul>
                    <hr>
                    <ul class="list-links">
                      <li>
                        <h3 class="list-links-title">Categories</h3></li>
                      <li><a href="#">Women’s Clothing</a></li>
                      <li><a href="#">Men’s Clothing</a></li>
                      <li><a href="#">Phones & Accessories</a></li>
                      <li><a href="#">Jewelry & Watches</a></li>
                      <li><a href="#">Bags & Shoes</a></li>
                    </ul>
                    <hr class="hidden-md hidden-lg">
                  </div>
                  <div class="col-md-4">
                    <ul class="list-links">
                      <li>
                        <h3 class="list-links-title">Categories</h3></li>
                      <li><a href="#">Women’s Clothing</a></li>
                      <li><a href="#">Men’s Clothing</a></li>
                      <li><a href="#">Phones & Accessories</a></li>
                      <li><a href="#">Jewelry & Watches</a></li>
                      <li><a href="#">Bags & Shoes</a></li>
                    </ul>
                    <hr>
                    <ul class="list-links">
                      <li>
                        <h3 class="list-links-title">Categories</h3></li>
                      <li><a href="#">Women’s Clothing</a></li>
                      <li><a href="#">Men’s Clothing</a></li>
                      <li><a href="#">Phones & Accessories</a></li>
                      <li><a href="#">Jewelry & Watches</a></li>
                      <li><a href="#">Bags & Shoes</a></li>
                    </ul>
                  </div>
                </div>
              </div>
            </li>
            <li><a href="#"><?php echo $lang['subsite']; ?></a></li>
            <li><a href="#"><?php echo $lang['subocto']; ?></a></li>
            <li><a href="#"><?php echo $lang['vall']; ?></a></li>
          </ul>
        </div>
        <!-- /category nav -->

        <!-- menu nav -->
        <div class="menu-nav">
          <span class="menu-header">Menu <i class="fa fa-bars"></i></span>
          <ul class="menu-list">
            <li><a href="./"><?php echo $lang['home']; ?></a></li>
            <li><a href="#"><?php echo $lang['shop']; ?></a></li>
            <li class="dropdown mega-dropdown"><a class="dropdown-toggle" data-toggle="dropdown" aria-expanded="true"><?php echo $lang['women']; ?> <i class="fa fa-caret-down"></i></a>
              <div class="custom-menu">
                <div class="row">
                  <div class="col-md-4">
                    <ul class="list-links">
                      <li>
                        <h3 class="list-links-title">Categories</h3></li>
                      <li><a href="#">Women’s Clothing</a></li>
                      <li><a href="#">Men’s Clothing</a></li>
                      <li><a href="#">Phones & Accessories</a></li>
                      <li><a href="#">Jewelry & Watches</a></li>
                      <li><a href="#">Bags & Shoes</a></li>
                    </ul>
                    <hr class="hidden-md hidden-lg">
                  </div>
                  <div class="col-md-4">
                    <ul class="list-links">
                      <li>
                        <h3 class="list-links-title">Categories</h3></li>
                      <li><a href="#">Women’s Clothing</a></li>
                      <li><a href="#">Men’s Clothing</a></li>
                      <li><a href="#">Phones & Accessories</a></li>
                      <li><a href="#">Jewelry & Watches</a></li>
                      <li><a href="#">Bags & Shoes</a></li>
                    </ul>
                    <hr class="hidden-md hidden-lg">
                  </div>
                  <div class="col-md-4">
                    <ul class="list-links">
                      <li>
                        <h3 class="list-links-title">Categories</h3></li>
                      <li><a href="#">Women’s Clothing</a></li>
                      <li><a href="#">Men’s Clothing</a></li>
                      <li><a href="#">Phones & Accessories</a></li>
                      <li><a href="#">Jewelry & Watches</a></li>
                      <li><a href="#">Bags & Shoes</a></li>
                    </ul>
                  </div>
                </div>
                <div class="row hidden-sm hidden-xs">
                  <div class="col-md-12">
                    <hr>
                    <a class="banner banner-1" href="#">
                      <img src="./img/banner05.jpg" alt="">
                      <div class="banner-caption text-center">
                        <h2 class="white-color">NEW COLLECTION</h2>
                        <h3 class="white-color font-weak">HOT DEAL</h3>
                      </div>
                    </a>
                  </div>
                </div>
              </div>
            </li>
            <li class="dropdown mega-dropdown full-width"><a class="dropdown-toggle" data-toggle="dropdown" aria-expanded="true"><?php echo $lang['men']; ?> <i class="fa fa-caret-down"></i></a>
              <div class="custom-menu">
                <div class="row">
                  <div class="col-md-3">
                    <div class="hidden-sm hidden-xs">
                      <a class="banner banner-1" href="#">
                        <img src="./img/banner06.jpg" alt="">
                        <div class="banner-caption text-center">
                          <h3 class="white-color text-uppercase">Women’s</h3>
                        </div>
                      </a>
                      <hr>
                    </div>
                    <ul class="list-links">
                      <li>
                        <h3 class="list-links-title">Categories</h3></li>
                      <li><a href="#">Women’s Clothing</a></li>
                      <li><a href="#">Men’s Clothing</a></li>
                      <li><a href="#">Phones & Accessories</a></li>
                      <li><a href="#">Jewelry & Watches</a></li>
                      <li><a href="#">Bags & Shoes</a></li>
                    </ul>
                  </div>
                  <div class="col-md-3">
                    <div class="hidden-sm hidden-xs">
                      <a class="banner banner-1" href="#">
                        <img src="./img/banner07.jpg" alt="">
                        <div class="banner-caption text-center">
                          <h3 class="white-color text-uppercase">Men’s</h3>
                        </div>
                      </a>
                    </div>
                    <hr>
                    <ul class="list-links">
                      <li>
                        <h3 class="list-links-title">Categories</h3></li>
                      <li><a href="#">Women’s Clothing</a></li>
                      <li><a href="#">Men’s Clothing</a></li>
                      <li><a href="#">Phones & Accessories</a></li>
                      <li><a href="#">Jewelry & Watches</a></li>
                      <li><a href="#">Bags & Shoes</a></li>
                    </ul>
                  </div>
                  <div class="col-md-3">
                    <div class="hidden-sm hidden-xs">
                      <a class="banner banner-1" href="#">
                        <img src="./img/banner08.jpg" alt="">
                        <div class="banner-caption text-center">
                          <h3 class="white-color text-uppercase">Accessories</h3>
                        </div>
                      </a>
                    </div>
                    <hr>
                    <ul class="list-links">
                      <li>
                        <h3 class="list-links-title">Categories</h3></li>
                      <li><a href="#">Women’s Clothing</a></li>
                      <li><a href="#">Men’s Clothing</a></li>
                      <li><a href="#">Phones & Accessories</a></li>
                      <li><a href="#">Jewelry & Watches</a></li>
                      <li><a href="#">Bags & Shoes</a></li>
                    </ul>
                  </div>
                  <div class="col-md-3">
                    <div class="hidden-sm hidden-xs">
                      <a class="banner banner-1" href="#">
                        <img src="./img/banner09.jpg" alt="">
                        <div class="banner-caption text-center">
                          <h3 class="white-color text-uppercase">Bags</h3>
                        </div>
                      </a>
                    </div>
                    <hr>
                    <ul class="list-links">
                      <li>
                        <h3 class="list-links-title">Categories</h3></li>
                      <li><a href="#">Women’s Clothing</a></li>
                      <li><a href="#">Men’s Clothing</a></li>
                      <li><a href="#">Phones & Accessories</a></li>
                      <li><a href="#">Jewelry & Watches</a></li>
                      <li><a href="#">Bags & Shoes</a></li>
                    </ul>
                  </div>
                </div>
              </div>
            </li>
            <li><a href="#"><?php echo $lang['sale']; ?></a></li>
            <li class="dropdown default-dropdown"><a class="dropdown-toggle" data-toggle="dropdown" aria-expanded="true">Pages <i class="fa fa-caret-down"></i></a>
              <ul class="custom-menu">
                <li><a href="index.html">Home</a></li>
                <li><a href="products.html">Products</a></li>
                <li><a href="product-page.html">Product Details</a></li>
                <li><a href="checkout.html">Checkout</a></li>
              </ul>
            </li>
          </ul>
        </div>
        <!-- menu nav -->
      </div>
    </div>
    <!-- /container -->
  </div>
  <!-- /NAVIGATION -->

  <!-- BREADCRUMB -->
  <div id="breadcrumb">
    <div class="container">
      <ul class="breadcrumb">
        <li><a href="#"><?php echo $lang['home']; ?></a></li>
        <li class="active"><?php echo $lang['reg']; ?></li>
      </ul>
    </div>
  </div>
  <!-- /BREADCRUMB -->

  <!-- section -->
  <div class="section">
    <!-- container -->
    <div class="container">
      <!-- row -->
      <div class="row">
        <div class="center">
          <div class="col-xs-12 col-sm-4">
            <div class="form-group">
              <label for="inputName"><?php echo $lang['name']; ?>*</label>
              <input type="text" ng-model="name" class="form-control">
              <span ng-if="name.length == 0" class="alert-danger"><?php echo $lang['msjname']; ?></span>
            </div>
          </div>
          <div class="col-xs-12 col-sm-4">
            <div class="form-group">
              <label for="inputName"><?php echo $lang['lname']; ?>*</label>
              <input type="text" ng-model="lastname" class="form-control">
              <span ng-if="lastname.length == 0" class="alert-danger"><?php echo $lang['msjlname']; ?></span>
            </div>
          </div>
          <div class="col-xs-12 col-sm-4">
            <div class="form-group">
              <label for="inputName"><?php echo $lang['email']; ?>*</label>
              <input type="email" ng-model="email" class="form-control">
              <span ng-if="email.length == 0" class="alert-danger"><?php echo $lang['msjemail']; ?></span>
            </div>
          </div>
          <div class="col-xs-12 col-sm-4">
            <div class="form-group">
              <label for="inputName"><?php echo $lang['phone']; ?>*</label>
              <input type="text" class="form-control" ng-model="phone" ng-change="phonev()">
              <span ng-show="isphone == 0 || isphone>10" class="alert-danger"><?php echo $lang['msjphone']; ?></span>
            </div>
          </div>
          <div class="col-xs-12 col-sm-4">
            <div class="form-group">
              <label for="inputName"><?php echo $lang['address']; ?>*</label>
              <input type="text" ng-model="address" class="form-control">
              <span ng-if="address.length == 0" class="alert-danger"><?php echo $lang['msjadres']; ?></span>
            </div>
          </div>
          <div class="col-xs-12 col-sm-4">
            <div class="form-group">
              <label for="inputName"><?php echo $lang['pasw']; ?>*</label>
              <input type="password" ng-model="pasw" class="form-control" ng-change="match()">
              <span ng-if="pasw.length == 0" class="alert-danger"><?php echo $lang['msjpasw']; ?></span>
            </div>
          </div>
          <div class="col-xs-12 col-sm-4">
            <div class="form-group">
              <label for="inputName"><?php echo $lang['rpasw']; ?>*</label>
              <input type="password" ng-model="rpasw" class="form-control" ng-change="match()">
              <span ng-show="ismatch && rpasw.length > 0" class="alert-success"><?php echo $lang['msjrpas2']; ?></span>
              <span ng-hide="ismatch || rpasw==null" class="alert-danger"><?php echo $lang['msjrpas1']; ?></span>
            </div>
          </div>
        </div>
      </div>
      <button type="button" class="primary-btn" ng-click="save()"><?php echo $lang['reg']; ?></button>&nbsp;&nbsp;&nbsp;&nbsp;
      <a href="./" class="second-btn" ng-click="save()"><?php echo $lang['cancel']; ?></a>
      <!-- /row -->
    </div>
    <!-- /container -->
  </div>
  <!-- /section -->