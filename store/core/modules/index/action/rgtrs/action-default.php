<?php
$data = json_decode(file_get_contents("php://input"));

$reg =  new ClientData();

if ($data->action==="add") {
	$reg->name = $data->name;
	$reg->lastname = $data->lastname;
	$reg->email = $data->email;
	$reg->phone = $data->phone;
	$reg->address = $data->address;
	$reg->rpasw = sha1(md5($data->rpasw));
	$exists = $reg->getVerify($reg->email);
	if (count($exists)===0) {
		$reg->add();
		$new = $reg->getVerify($reg->email);
		//session_start();
		$_SESSION["client_id"]=$new->id;
	}
}

?>