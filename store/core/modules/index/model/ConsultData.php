<?php
class ConsultData {
	public static $tablename = "consult";
	public static $vlr="";

	public function ConsultData(){
		$this->created_at = "NOW()";
	}

	public function add(){
		$sql = "call add_consult(\"$this->msm\",\"$this->cid\",\"$this->conid\")";
		Executor::doit($sql);
	}

	public function payment(){
		$sql = "call payment(\"$this->sec\",\"$this->fec\",\"$this->trans\",\"$this->caj\",\"$this->ofic\",\"$this->monto\",\"$this->idcc\",\"$this->fini\",\"$this->ffin\",\"$this->idclt\")";
		Executor::doit($sql);
	}

	public static function delById($id){
		$sql = "delete from ".self::$tablename." where id=$id";
		Executor::doit($sql);
	}
	public function del(){
		$sql = "delete from ".self::$tablename." where id=$this->id";
		Executor::doit($sql);
	}

	public function update(){
		$sql = "update ".self::$tablename." set name=\"$this->name\",lastname=\"$this->lastname\",email=\"$this->email\",phone=\"$this->phone\",address=\"$this->address\",is_active=$this->is_active,Dni=\"$this->dni\" where id=$this->id";
		Executor::doit($sql);
	}

	public static function getById($id){
		$sql = "select * from ".self::$tablename." where id=$id";
		$query = Executor::doit($sql);
		return Model::one($query[0],new ConsultData());
	}
	
	public static function getByClient($id){
		$sql = "select * from ".self::$tablename." where client_id=$id";
		$query = Executor::doit($sql);
		return Model::many($query[0],new ConsultData());
	}
	
	public static function getAll(){
		$sql = "select *,date(created_at) datec from ".self::$tablename." order by created_at";
		$query = Executor::doit($sql);
		return Model::many($query[0],new ConsultData());
	}
	
	public static function getDates($con_id){
		$sql = "select * from consult_date where client_consult_id='$con_id' order by fecha desc";
		$query = Executor::doit($sql);
		return Model::many($query[0],new ConsultData());
	}
	
	public static function getEmp(){
		$sql = "select * from employe";
		$query = Executor::doit($sql);
		return Model::many($query[0],new ConsultData());
	}
	
	public static function getConvs($con_id){
		$sql = "select id,conver,client_id,consult_date_id cdi from ".self::$tablename." where client_consult_id='$con_id' order by created_at desc";
		$query = Executor::doit($sql);
		return Model::many($query[0],new ConsultData());
	}

	public static function fechas(){
		$sql = "select concat(year(curdate()),mid(fecini,5,8)) fini,concat(year(curdate()),mid(fecfin,5,8)) ffin from months where curdate() between fecini and fecfin";
		$query = Executor::doit($sql);
		return Model::many($query[0],new ConsultData());
	}
}

?>