<?php
class GroupClientData {
	public static $tablename = "convclients";
	public static $vlr="";

	public function GroupClientData(){
		$this->name = "";
		$this->lastname = "";
		$this->email = "";
		$this->password = "";
		$this->created_at = "NOW()";
	}

	public function add(){
		$sql = "insert into ".self::$tablename." values (default,\"$this->msj\",now(),\"$this->cid\")";
		Executor::doit($sql);
	}

	public static function delById($id){
		$sql = "delete from ".self::$tablename." where id=$id";
		Executor::doit($sql);
	}
	public function del(){
		$sql = "delete from ".self::$tablename." where id=$this->id";
		Executor::doit($sql);
	}

	public function update(){
		$sql = "update ".self::$tablename." set name=\"$this->name\",lastname=\"$this->lastname\",email=\"$this->email\",phone=\"$this->phone\",address=\"$this->address\",is_active=$this->is_active,Dni=\"$this->dni\" where id=$this->id";
		Executor::doit($sql);
	}

	public static function getById($id){
		$sql = "select * from ".self::$tablename." where id=$id";
		$query = Executor::doit($sql);
		return Model::one($query[0],new GroupClientData());
	}
	
	public static function getByClient($id){
		$sql = "select * from ".self::$tablename." where client_id=$id";
		$query = Executor::doit($sql);
		return Model::many($query[0],new GroupClientData());
	}
	
	public static function getAll(){
		$sql = "select *,date(create_at) datec from ".self::$tablename." order by create_at";
		$query = Executor::doit($sql);
		return Model::many($query[0],new GroupClientData());
	}
	
	public static function getConvs(){
		$sql = "select *,date_format(created_at, '%H:%I') time from ".self::$tablename." order by id desc limit 5";
		$query = Executor::doit($sql);
		return Model::many($query[0],new GroupClientData());
	}
}

?>