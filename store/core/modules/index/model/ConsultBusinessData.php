<?php
class ConsultBusinessData {
	public static $tablename = "consult_company";
	public static $vlr="";

	public function ConsultBusinessData(){
		$this->created_at = "NOW()";
	}

	public static function VerifyClientId($id){
		$sql = "select count(*) cant from pay p inner join service s on p.service_id=s.id inner join ".self::$tablename." c on p.t_service_id=c.id and (curdate() between fec_ini and fec_fin) and c.consultant_id='$id';";
		$query = Executor::doit($sql);
		return Model::one($query[0],new ConsultBusinessData());
	}

	public static function getMyConsults($id){
		$sql = "select * from ".self::$tablename." where consultant_id='$id'";
		$query = Executor::doit($sql);
		return Model::many($query[0],new ConsultBusinessData());
	}

	public static function getClients($cid){
		$sql = "select cc.id,c.id client_id,concat(c.lastname,' ',c.name) fullname, c.phone, c.nameb, c.profile from client_consult_company cc inner join client c on cc.client_id=c.id and cc.consult_company_id='$cid'";
		$query = Executor::doit($sql);
		return Model::many($query[0],new ConsultData());
	}

	public static function getAll(){
		$sql = "select *,date(created_at) datec from ".self::$tablename." order by created_at";
		$query = Executor::doit($sql);
		return Model::many($query[0],new ConsultBusinessData());
	}
	
	public static function getById($id){
		$sql = "select count(*) cant from ".self::$tablename." where consultant_id='$id'";
		$query = Executor::doit($sql);
		return Model::one($query[0],new ConsultBusinessData());
	}
	
}

?>