<script src="js/sweetalert-dev.js"></script>
<link rel="stylesheet" href="js/sweetalert.css">
<?php

// print_r($_POST);
$reg =  new ClientData();
$reg->id=$_SESSION['client_id'];

if(isset($_POST['save_email'])){
	$reg->email = $_POST['email'];
	$reg->update_email();
	echo '<body><script>swal({
		title: "Success Message",
		text: "Actualizado Correctamente!",
		type: "success",
		showCancelButton: false,
		confirmButtonColor: "#92D6E8",
		confirmButtonText: "Ok",
		closeOnConfirm: false
	},
	function(){
		location.href ="index.php?view=edp";
	});</script></body>';
}else{
	if(isset($_POST['save_info'])){
	$reg->lastname = $_POST['lname'];
	$reg->firstname = $_POST['fname'];
	$reg->gender = $_POST['gender'];
	$reg->phone = $_POST['phone'];
	$reg->update_info();
	echo '<body><script>swal({
		title: "Success Message",
		text: "Actualizado Correctamente!",
		type: "success",
		showCancelButton: false,
		confirmButtonColor: "#92D6E8",
		confirmButtonText: "Ok",
		closeOnConfirm: false
	},
	function(){
		location.href ="index.php?view=edp";
	});</script></body>';
	}else{
		if(isset($_POST['save_psw'])){
			$reg->psw = sha1(md5($_POST['psw']));
			$reg->rpsw = sha1(md5($_POST['rpsw']));
			if($reg->psw===$reg->rpsw){
				$reg->update_passwd();
				echo '<body><script>swal({
				title: "Success Message",
				text: "Actualizado Correctamente!",
				type: "success",
				showCancelButton: false,
				confirmButtonColor: "#92D6E8",
				confirmButtonText: "Ok",
				closeOnConfirm: false
				},
				function(){
					location.href ="index.php?view=edp";
				});</script></body>';
			}else{
				echo '<body><script>swal({
				title: "Error Message",
				text: "El password no coincide",
				type: "error",
				showCancelButton: false,
				confirmButtonColor: "#DD6B55",
				confirmButtonText: "Ok",
				closeOnConfirm: false
				},
				function(){
					location.href ="index.php?view=edp";
				});</script></body>';
			}
		}else{
			if(isset($_POST['save_photo'])){
				$handle = new Upload($_FILES['image']);
				if ($handle->uploaded) {
				$url="img/clts/";
				$handle->Process($url);
			    $reg->image = $handle->file_dst_name;
			    $reg->update_image();
				echo '<body><script>swal({
				title: "Success Message",
				text: "Actualizado Correctamente!",
				type: "success",
				showCancelButton: false,
				confirmButtonColor: "#92D6E8",
				confirmButtonText: "Ok",
				closeOnConfirm: false
				},
				function(){
					location.href ="index.php?view=edp";
				});</script></body>';
				}
			}else{
				if(isset($_POST['save_bb'])){
					$reg->nameb = $_POST['cname'];
					$reg->eid = $_POST['eid'];
					$reg->update_b();
					echo '<body><script>swal({
					title: "Success Message",
					text: "Actualizado Correctamente!",
					type: "success",
					showCancelButton: false,
					confirmButtonColor: "#92D6E8",
					confirmButtonText: "Ok",
					closeOnConfirm: false
					},
					function(){
						location.href ="index.php?view=edp";
					});</script></body>';
				}
			}
		}
	}
}
?>