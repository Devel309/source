<?php

$form_data = json_decode(file_get_contents("php://input"));

$con = new ListConsultData();

if (empty($form_data->action)) {
	echo json_encode($con->getAll());
}
else{
	if($form_data->action==="empresas"){
		echo json_encode($con->getEmpresas($form_data->idconsult));
	}else{
		$con->ccid = $form_data->ccid;
		$con->cid = $_SESSION['client_id'];
		$verify = $con->verify($con->ccid,$con->cid);
		if ($verify->cant==='0') {
			$con->adquirir();
		}
	}
}

?>

