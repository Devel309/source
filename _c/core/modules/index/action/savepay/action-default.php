<?php

$form_data = json_decode(file_get_contents("php://input"));

$con = new ConsultData();

if (empty($form_data->action)) {
	echo json_encode($con->fechas());
}else{
	if ($form_data->action==="pagar") {
		$con->sec=$form_data->secuencia;
		$con->fec=$form_data->fecha;
		$con->trans=$form_data->transaction;
		$con->caj=$form_data->cajero;
		$con->ofic=$form_data->oficina;
		$con->monto=$form_data->monto;
		$con->idcc=$form_data->idcc;
		$con->fini=$form_data->fecini;
		$con->ffin=$form_data->fecfin;
		$con->idclt=$_SESSION['client_id'];
		$con->payment();
	}
}

?>

