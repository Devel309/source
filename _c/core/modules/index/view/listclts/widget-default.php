<?php
if (isset($_GET['lang'])){
  $language = $_GET['lang'];
  if(!empty($language)){
    require("../lang/".$language.".php");
  }
}else{
  $language = substr($_SERVER['HTTP_ACCEPT_LANGUAGE'],0,2);
  require("../lang/".$language.".php");
}
?>
<div class="pull-left breadcrumb_admin clear_both">
        <div class="pull-left page_title theme_color">
          <h1><?php echo $lang['listc']; ?></h1>
          <h2 class=""><?php echo $lang['consult']; ?></h2>
        </div>
        <div class="pull-right">
          <ol class="breadcrumb">
            <li><a href="#"><?php echo $lang['listc']; ?></a></li>
            <li class="active"><?php echo $lang['consult']; ?></li>
          </ol>
        </div>
      </div>
      <div class="container clear_both padding_fix">
        <!--\\\\\\\ container  start \\\\\\-->
        
        <div class="row" ng-init="listar()" ng-show="visible">
          <div class="col-lg-6">
            <section class="panel default blue_title h2">
              <div class="panel-heading"><?php echo $lang['listc']; ?></div>
              <div class="panel-body">
                <ul class="list-group" ng-repeat="lc in listaconsult">
                  <li ng-click="clients(lc.id)" class="list-group-item">{{lc.name_company}}</li>
                </ul>
              </div>
            </section>
          </div>

          <div class="col-lg-6" ng-show="visclient">
            <section class="panel default blue_title h2">
              <div class="panel-heading"><?php echo $lang['client']; ?></div>
              <div class="panel-body">
                <div class="contact_people" ng-repeat="c in clientes">
                  <a ng-href="#"><img height="120px" width="120px" src="../img/clts/{{c.profile}}" /></a>
                  <div class="contact_people_body">
                    <h5>{{c.fullname}}</h5>
                      <span><i class="fa fa-phone"></i>{{c.phone}}</span>
                      <span><i class="fa fa-briefcase"></i>{{c.nameb}}</span>
                      <a ng-href="?view=clt&_i={{c.id}}"><?php echo $lang['consult']; ?> <i class="fa fa-arrow-right"></i></a>
                  </div>
                </div>
              </div>
            </section>
          </div>
      </div>

<script>
var app = angular.module('consultoria', []);
app.controller('controller', function($scope, $http, $location){

    $scope.visible = false;
    $scope.visclient = false;

  $scope.listar = function(){
    $http.get('?action=listsclts').success(function(data){
      $scope.listaconsult = data;
        if ($scope.listaconsult.length>0) {
          $scope.visible = true;
        }else{
          $scope.visible = false;
        }
    });
  };

  $scope.clients = function(id){
    $http({
      method:"POST",
      url:"?action=listsclts",
      data:{
        'id':id,
        'action':'client'
      }
    }).success(function(data){
        $scope.clientes = data;
        if ($scope.clientes.length>0) {
          $scope.visclient = true;
        }else{
          $scope.visclient = false;
        }
    });
  };

});

</script>