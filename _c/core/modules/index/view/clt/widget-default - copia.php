<?php
if (isset($_GET['lang'])){
  $language = $_GET['lang'];
  if(!empty($language)){
    require("../lang/".$language.".php");
  }
}else{
  $language = substr($_SERVER['HTTP_ACCEPT_LANGUAGE'],0,2);
  require("../lang/".$language.".php");
}
?>
    <!-- Content Header (Page header) -->
    <section class="content-header">
      <h1>
        <?php echo $lang['consult']; ?>
        <small>Online</small>
      </h1>
      <ol class="breadcrumb">
        <li><a href="index.php?view=chti"><i class="fa fa-dashboard"></i> <?php echo $lang['dash']; ?></a></li>
        <li><a href="#"><?php echo $lang['consult']; ?></a></li>
      </ol>
    </section>
    <?php $clt = ConsultData::getDates(); ?>
    <!-- Main content -->
    <section class="content">
    <form class="form-horizontal" method="post" action="index.php?action=clt" role="form">
      <!-- row -->
      <div class="row">
        <div class="col-md-12">
          <!-- The time line -->
          <ul class="timeline">
            <!-- timeline time label -->
            <?php foreach($clt as $c): ?>
            <li class="time-label">
                  <span class="bg-yellow">
                    <?php echo $c->datec; ?>
                  </span>
                  <?php $conv = ConsultData::getConvs($c->datec); ?>
                  <?php foreach($conv as $cv): ?>
	            <!-- timeline item -->
    	        <li>
                <?php if($cv->color==='0'){?>
        	      <i class="fa fa-user bg-aqua"></i>
                <?php } else { ?>
                  <i class="fa fa-user bg-blue"></i>
                <?php } ?>
	              <div class="timeline-item">
    	            <span class="time"><i class="fa fa-comment-o"></i></span>
	                <div class="timeline-body">
    	              <?php echo $cv->conver; ?>
	                </div>
                    <?php if($cv->color==='0'){?>
        	      		<div class="timeline-footer">
    	        	      <a class="btn btn-danger btn-xs">Delete</a>
        	        	</div>
	                <?php } ?>
	              </div>
    	        </li>
        	    <!-- END timeline item -->
                <?php endforeach; ?>
            </li>
            <?php endforeach; ?>
            <!-- /.timeline-label -->
            <!-- timeline item -->
            <li>
              <i class="fa fa-comments bg-yellow"></i>
              <div class="timeline-item">                
                <div class="box-footer">
              	  <div class="input-group">
	                <input class="form-control" name="msm" placeholder="<?php echo $lang['typmsj']; ?>..." required>
                    <div class="input-group-btn"><input style="font-family: FontAwesome;" type="submit" class="btn btn-warning" onclick="myFunction()" value="&#xf1d8;"></div>
		                <!--div class="input-group-btn">
		                  <button type="button" class="btn btn-warning"><i class="fa fa-send bg-yellow"></i></button>
        		        </div-->
	              </div>
            	</div>
              </div>
            </li>
            <!-- END timeline item -->
            <li>
              <i class="fa fa-clock-o bg-gray"></i>
            </li>
          </ul>
        </div>
        <!-- /.col -->
      </div>
      <!-- /.row -->
      </form>
    </section>
    <!-- /.content -->