<?php
if (isset($_GET['lang'])){
  $language = $_GET['lang'];
  if(!empty($language)){
    require("../lang/".$language.".php");
  }
}else{
  $language = substr($_SERVER['HTTP_ACCEPT_LANGUAGE'],0,2);
  require("../lang/".$language.".php");
}
?>
<div class="pull-left breadcrumb_admin clear_both">
        <div class="pull-left page_title theme_color">
          <h1><?php echo $lang['listc']; ?></h1>
          <h2 class=""><?php echo $lang['consult']; ?></h2>
        </div>
        <div class="pull-right">
          <ol class="breadcrumb">
            <li><a href="#"><?php echo $lang['listc']; ?></a></li>
            <li class="active"><?php echo $lang['consult']; ?></li>
          </ol>
        </div>
      </div>
      <div class="container clear_both padding_fix">
        <!--\\\\\\\ container  start \\\\\\-->
        
        <div class="row" ng-init="listar()">
          <div class="col-lg-6">
            <section class="panel default blue_title h2">
              <div class="panel-heading"><?php echo $lang['listc']; ?></div>
              <div class="panel-body">
                <ul class="list-group" ng-repeat="lc in listaconsult">
                  <li class="list-group-item" ng-click="empresas(lc.id)">{{lc.name_consult}}</li>
                </ul>
              </div>
            </section>
          </div>
          <div class="col-lg-6" ng-show="visible">
            <section class="panel default blue_title h2">
              <div class="panel-heading"><?php echo $lang['listbus']; ?></div>
              <div class="panel-body">
                <div class="list-group" ng-repeat="aud in auditores">
                  <a class="list-group-item" ng-click="add(aud.id)" ng-href="?view=list">
                    <span class="badge">Adquirir</span>
                    <h4 class="list-group-item-heading">{{aud.name_company}}</h4>
                    <p class="list-group-item-text">{{aud.description}}</p>
                    <h5 class="list-group-item-heading">{{aud.price}}$</h5>
                  </a>
                  </div>
              </div>
            </section>
          </div>
        </div>
      </div>

<script>
var app = angular.module('consultoria', []);
app.controller('controller', function($scope, $http, $location){

  $scope.visible = false;

  $scope.listar = function(){
    $http.get('?action=listc').success(function(data){
      $scope.listaconsult = data;
    });
  };

  $scope.empresas = function(id_consult){
    $http({
      method:"POST",
      url:"?action=listc",
      data:{
        'idconsult':id_consult,
        'action':'empresas'
      }
    }).success(function(data){
        $scope.auditores = data;
        if ($scope.auditores.length>0) {
          $scope.visible = true;
        }else{
          $scope.visible = false;
        }
    });
  };

  $scope.add = function(ccid){
    $http({
      method:"POST",
      url:"?action=listc",
      data:{
        'ccid':ccid,
        'action':'adquirir'
      }
    }).success(function(data){
    });
  };

});

</script>