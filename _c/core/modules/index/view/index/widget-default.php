<?php
$language = "";
if (isset($_GET['lang'])){
  $language = $_GET['lang'];
  if(!empty($language)){
    require("../lang/".$language.".php");
  }
}else{
  $language = substr($_SERVER['HTTP_ACCEPT_LANGUAGE'],0,2);
  require("../lang/".$language.".php");
}
?>
<style>
.datepicker{z-index:1151 !important;}
</style>
<div class="pull-left breadcrumb_admin clear_both">
        <div class="pull-left page_title theme_color">
          <h1><?php echo $lang['dash']; ?></h1>
          <h2 class=""><?php echo $lang['cpanel']; ?></h2>
        </div>
        <div class="pull-right">
          <ol class="breadcrumb">
            <li><a href="#"><?php echo $lang['dash']; ?></a></li>
            <li class="active"><?php echo $lang['cpanel']; ?></li>
          </ol>
        </div>
      </div>
      <div class="container clear_both padding_fix">
        <?php $consultant = ConsultBusinessData::getById($_SESSION['client_id']);
        $verpay = ConsultBusinessData::VerifyClientId($_SESSION['client_id']); ?>
        <div class="row" ng-init="services()">
          <div class="col-sm-3 col-sm-6" <?php if($consultant->cant>'0'): if ($verpay->cant>'0'): ?>onclick="window.location='?view=listclts';"<?php endif; else: ?>onclick="window.location='?view=listc';"<?php endif; ?> ng-repeat="sv in service">
            <div class="information green_info">   
              <div class="information_inner">
                <div class="info green_symbols"><i class="fa fa-laptop icon"></i></div>
                <span><b style="text-transform: uppercase;">{{sv.name}}</b><br>PREMIUM<br>
                <?php if($consultant->cant>'0'):
                  if ($verpay->cant==='0') { ?>
                    S/. {{sv.price}} mensual
                <?php } endif; ?></span>
                <div class="infoprogress_green">
                  <div class="greenprogress"></div>
                </div>                
                <?php if($consultant->cant>'0'):
                  if ($verpay->cant==='0') {?><h3 ng-click="openuno(sv.id,sv.name,sv.price)"><i class="fa fa-credit-card"> Pagar</i></h3><?php } endif; ?>
              </div>
            </div>
          </div>
          <div class="col-sm-3 col-sm-6">
            <a href="../store" target="_new">
            <div class="information blue_info">
              <div class="information_inner">
              <div class="info blue_symbols"><i class="fa fa-shopping-cart icon"></i></div>
                <span><b><?php echo strtoupper($lang['sales']); ?></b><br>PREMIUM </span>
                <div class="infoprogress_blue">
                  <div class="blueprogress"></div>
                </div>
                <h3 class="bolded">S/. 80 mensual </h3>
              </div>
            </div>
            </a>
          </div>
          <div class="col-sm-3 col-sm-6">
            <div class="information red_info">
              <div class="information_inner">
              <div class="info red_symbols"><i class="fa fa-bar-chart-o icon"></i></div>
                <span><b>LOGÍSTCA</b><br>PREMIUM </span>
                <div class="infoprogress_red">
                  <div class="redprogress"></div>
                </div>
                <h3 class="bolded">S/. 120 mensual </h3>
              </div>
            </div>
          </div>
          <div class="col-sm-3 col-sm-6">
          <a href="../foods" target="_new">
           <div class="information gray_info">
              <div class="information_inner">
              <div class="info blue_symbols"><i class="fa fa-cutlery icon"></i></div>
                <span><b>RESTAURANTES</b><br>PREMIUM </span>
                <div class="infoprogress_gray">
                  <div class="grayprogress"></div>
                </div>
                <h3 class="bolded">S/. 50 mensual </h3>
              </div>
            </div>
          </div>
          </a>
        </div>
         
      </div>

<div class="modal fade" id="modaluno" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">
  <div class="modal-dialog">
    <div class="modal-content">
      <div class="modal-header">
        <button type="button" class="close" data-dismiss="modal" aria-hidden="true">&times;</button>
        <h4 class="modal-title" id="myModalLabel"><?php echo $lang['pay']; ?></h4>
      </div>
      <div class="modal-body">
        <div class="invoice_body">
            <div  class="pull-left">
              <div class="invoice_logo"><img src="../img/logtop.png" /></div>
              <address>
              <strong>MULTISERVICIOS YAHUA, SRL.</strong><br>
              RUC: 20604760888<br>
              JR. LOS KISWARES SN BAR. SHANCAYAN<br>
              INDEPENDENCIA - HUARAZ - ANCASH - PERÚ<br>
              <abbr title="Phone">P:</abbr> <a href="tel:+51925459244">(+51) 925-459-244</a>
              </address>
            </div>
            <div class="pull-right">
              <h1><?php echo $lang['fact']; ?></h1>
            </div>
            <div class="clearfix"></div>
            <br/>
            <br/>
            <br/>
            <br/>
            <br/>
            <br/>
            <div class="row alert alert-info">
              <div class="col-sm-9">
                <address>
                <strong>Depositar el monto a la siguiente cuenta</strong><br>
                CTA: 0000 0000 0000 0000<br>
                CCI: <br>
                <abbr title="Phone">NOMBRE:</abbr> NOMBRE_EMPRESA
                </address>
              </div>
              <div class="col-sm-3">
                <div> <strong><?php echo $lang['fact'].' '.$lang['num']; ?> :</strong> <span class="pull-right"> #AA-454-4113-00 </span> </div>
                <br/>
                <div class=""> <strong><?php echo $lang['fact'].' '.$lang['date']; ?> :</strong> <span class="pull-right"><i class="fa fa-calendar"></i>16/4/2014</span><br/>
                </div>
                <br/>
                <div class="well  red">
                  <div class="pull-left"> Total Debe : </div>
                  <div class="pull-right"> {{priceserv}}.00 Soles </div>
                  <div class="clearfix"></div>
                </div>
              </div>
            </div>
            <table class="table table-hover">
              <thead>
                <tr>
                  <th class="text-center">CANTIDAD</th>
                  <th>DESCRIPCION</th>
                  <th colspan="2" class="text-center">FECHA</th>
                  <th>PRECIO</th>
                  <th>SUBTOTAL</th>
                </tr>
              </thead>
              <tbody ng-repeat="fc in fechas">
                <tr class="tr_border">
                  <td class="text-center"><strong>1</strong></td>
                  <td>{{nameserv}}</td>
                  <td>{{fc.fini}}</td>
                  <td>{{fc.ffin}}</td>
                  <td>S/.{{priceserv}}.00</td>
                  <td>S/.{{1*priceserv}}.00</td>
                </tr>
                <tr>
                  <td colspan="4">Total</td>
                  <td><strong>S/.{{1*priceserv}}.00</strong></td>
                </tr>
                
              </tbody>
            </table>
            
            <div class="invoice_footer">
            <div class="row" ng-show="ocultar">
              <div class="col-sm-7">
                <div class="payment-methods">
                  <h5>Payment Methods</h5>
                  <img src="images/ame.png" /> <img src="images/mas.png" /> <img src="images/visa.png" /> </div>
              </div>
              <div class="col-sm-5">
                <div class="invoice_total pull-right" ng-repeat="fc in fechas">
                  <h3><strong>Total: <span class="well_text">S/.{{1*priceserv}}.00</span></strong></h3>
                </div>
              </div>
            </div>
            <div class="row alert alert-warning" ng-show="visible">
              <h5 class="text-center">Ingrese los datos del voucher</h5>
              <table class="table table-hover">
              <thead>
                <tr>
                  <th>SECUENCIA</th>
                  <th>FECHA PAGO</th>
                  <th>TRANSACCIÓN</th>
                  <th>CAJERO</th>
                  <th>OFICINA</th>
                </tr>
              </thead>
              <tbody>
                <tr class="tr_border">
                  <td class="text-center"><input type="text" id="secuencia" ng-model="secuencia" placeholder="XXXXXX" class="form-control" maxlength="6" autofocus></td>
                  <td><input type="text" size="18" class="form-control" datepicker data-ng-model="date" readonly dp-format="yyyy/m/d"></td>
                  <td><input type="text" ng-model="trans" placeholder="XXXX" class="form-control" maxlength="4"></td>
                  <td><input type="text" ng-model="cajero" placeholder="XXXX" class="form-control" maxlength="4"></td>
                  <td><input type="text" ng-model="oficina" placeholder="XXXX" class="form-control" maxlength="4"></td>
                </tr>
                
              </tbody>
            </table>
            </div>
          </div>
      </div>
      <div class="modal-footer">
        <button type="button" class="btn btn-danger" data-dismiss="modal">Close</button>
        <button ng-show="btnpay" type="button" class="btn btn-success" ng-click="enabled()">pay</button>
        <button ng-show="btnsave" type="button" class="btn btn-success" ng-click="pagar()"><?php echo $lang['save']; ?></button>
      </div>
    </div>
  </div>
</div>
</div>

<script>
  var app = angular.module('consultoria', []);

  app.controller('controller', function($scope, $http) {

  $scope.btnpay = true;
  $scope.btnsave = false;
  $scope.visible = false;
  $scope.ocultar = true;

  $scope.openuno = function(id,name,price){
    $scope.idserv = id;
    $scope.nameserv = name;
    $scope.priceserv = price;
    $("#modaluno").modal("show");
    $scope.inicio();
    //$('.mask').inputmask();
    //$('.datepicker').datepicker();
  };

  $scope.inicio = function(){
    $http({
      method:"POST",
      url:"?action=savepay"
    }).success(function(data){
      $scope.fechas = data;
      $scope.dateini = $scope.fechas[0].fini;
      $scope.datefin = $scope.fechas[0].ffin;
    });
  };

  $scope.enabled = function(){
    $scope.ocultar = false;
    $scope.visible = true;
    $scope.btnpay = false;
    $scope.btnsave = true;
  };

  $scope.services = function(){
    $http({
      method:"POST",
      url:"?action=service"
    }).success(function(data){
      $scope.service = data;
    });
  }

  $scope.pagar = function(){
    $http({
      method:"POST",
      url:"?action=savepay",
      data:{
        action:"pagar",
        secuencia:$scope.secuencia,
        fecha:$scope.date,
        transaction:$scope.trans,
        cajero:$scope.cajero,
        oficina:$scope.oficina,
        monto:$scope.priceserv,
        idcc:$scope.idserv,
        fecini:$scope.dateini,
        fecfin:$scope.datefin
      }
    }).success(function(data){
      $scope.btnpay = true;
      $scope.btnsave = false;
      $scope.secuencia = '';
      $scope.date = '';
      $scope.trans = '';
      $scope.cajero = '';
      $scope.oficina = '';
      $("#modaluno").modal("hide");
      location.reload();
    });
  };


  }).directive('datepicker', function() {
         return {
            restrict: 'A',
            require: 'ngModel',
            compile: function() {
               return {
                  pre: function(scope, element, attrs, ngModelCtrl) {
                     var format, dateObj;
                     format = (!attrs.dpFormat) ? 'yyyy/m/d' : attrs.dpFormat;
                     if (!attrs.initDate && !attrs.dpFormat) {
                        // If there is no initDate attribute than we will get todays date as the default
                        dateObj = new Date();
                        scope[attrs.ngModel] = dateObj.getFullYear() + '/' + (dateObj.getMonth() + 1) + '/' + dateObj.getDate();
                     } else if (!attrs.initDate) {
                        // Otherwise set as the init date
                        scope[attrs.ngModel] = attrs.initDate;
                     }
                     // Initialize the date-picker
                     $(element).datepicker({
                        format: format,
                     }).on('changeDate', function(ev) {
                        // To me this looks cleaner than adding $apply(); after everything.
                        scope.$apply(function () {
                          dateObj = new Date(ev.date);
                        scope[attrs.ngModel] = dateObj.getFullYear() + '/' + (dateObj.getMonth() + 1) + '/' + dateObj.getDate();
                        });
                     });
                  }
               }
            }
         }
      });
  
</script>