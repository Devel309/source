<?php
if (isset($_GET['lang'])){
  $language = $_GET['lang'];
  if(!empty($language)){
    require("../lang/".$language.".php");
  }
}else{
  $language = substr($_SERVER['HTTP_ACCEPT_LANGUAGE'],0,2);
  require("../lang/".$language.".php");
}
?>
<div class="pull-left breadcrumb_admin clear_both">
        <div class="pull-left page_title theme_color">
          <h1><?php echo $lang['listc']; ?></h1>
          <h2 class=""><?php echo $lang['consult']; ?></h2>
        </div>
        <div class="pull-right">
          <ol class="breadcrumb">
            <li><a href="#"><?php echo $lang['listc']; ?></a></li>
            <li class="active"><?php echo $lang['consult']; ?></li>
          </ol>
        </div>
      </div>
      <div class="container clear_both padding_fix">
        <!--\\\\\\\ container  start \\\\\\-->
        
        <div class="row" ng-init="listar()" ng-show="visible">
          <div class="col-lg-6">
            <section class="panel default blue_title h2">
              <div class="panel-heading"><?php echo $lang['listc']; ?></div>
              <div class="panel-body">
                <ul class="list-group" ng-repeat="lc in listaconsult">
                  <a ng-href="?view=clt&_i={{lc.id}}"><li class="list-group-item">{{lc.name_company}}</li></a>
                </ul>
              </div>
            </section>
          </div>
      </div>

<script>
var app = angular.module('consultoria', []);
app.controller('controller', function($scope, $http, $location){

    $scope.visible = false;

  $scope.listar = function(){
    $http({
      method:"POST",
      url:"?action=list"
    }).success(function(data){
        $scope.listaconsult = data;
        if ($scope.listaconsult.length>0) {
          $scope.visible = true;
        }else{
          $scope.visible = false;
        }
    });
  };

});

</script>