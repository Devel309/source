<?php
class ListConsultData {
	public static $tablename = "list_consult";
	public static $vlr="";

	public function ListConsultData(){
		$this->created_at = "NOW()";
	}

	public function adquirir(){
		$sql = "insert into client_consult_company (consult_company_id,client_id) values (\"$this->ccid\",\"$this->cid\")";
		Executor::doit($sql);
	}

	public static function delById($id){
		$sql = "delete from ".self::$tablename." where id=$id";
		Executor::doit($sql);
	}
	public function del(){
		$sql = "delete from ".self::$tablename." where id=$this->id";
		Executor::doit($sql);
	}

	public function update(){
		$sql = "update ".self::$tablename." set name=\"$this->name\",lastname=\"$this->lastname\",email=\"$this->email\",phone=\"$this->phone\",address=\"$this->address\",is_active=$this->is_active,Dni=\"$this->dni\" where id=$this->id";
		Executor::doit($sql);
	}

	public static function getById($id){
		$sql = "select * from ".self::$tablename." where id=$id";
		$query = Executor::doit($sql);
		return Model::one($query[0],new ListConsultData());
	}
	
	public static function getByClient($id){
		$sql = "select * from ".self::$tablename." where client_id=$id";
		$query = Executor::doit($sql);
		return Model::many($query[0],new ListConsultData());
	}

	public static function verify($ccid,$cid){
		$sql = "select count(*) cant from client_consult_company where consult_company_id='$ccid' and client_id=$cid";
		$query = Executor::doit($sql);
		return Model::one($query[0],new ListConsultData());
	}
	
	public static function getAll(){
		$sql = "select * from ".self::$tablename." order by name_consult";
		$query = Executor::doit($sql);
		return Model::many($query[0],new ListConsultData());
	}

	public static function getmyconsult($cli_id){
		$sql = "select cc.id,c.name_company from consult_company c inner join client_consult_company cc on c.id=cc.consult_company_id where cc.client_id='$cli_id'";
		$query = Executor::doit($sql);
		return Model::many($query[0],new ListConsultData());
	}
	
	public static function getEmpresas($consult_id){
		$sql = "select * from consult_company where list_consult_id='".$consult_id."'";
		$query = Executor::doit($sql);
		return Model::many($query[0],new ListConsultData());
	}
}

?>